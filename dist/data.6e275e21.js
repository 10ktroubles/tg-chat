// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"src/scheme.json":[function(require,module,exports) {
module.exports = [{
  "id": "1",
  "textid": "start",
  "name": "Приветственное сообщение",
  "message": "Привет! Я Зарешай-бот, я помогаю решать проблемы.\r\n\r\nНапиши, как тебя зовут, чтобы начать пробный кусок игры.\r\n\r\nВ случае чего посмотри хелп https:\/\/telegra.ph\/Help-Zareshaj-bota-10-04\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "startmarathon",
    "linked_to_id": "797"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "76",
  "textid": "TestAnton",
  "name": "Тестовое от Антона ",
  "message": "Привет, так я исследую мир и обучаюсь",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "start",
    "linked_to_id": "1"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "94",
  "textid": "GameQuestion",
  "name": "Вопрос по содержанию",
  "message": "Пиши, я позову кого-то живого. ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "102",
  "textid": "homework",
  "name": "Сдай задание",
  "message": "Я тебя читаю. Пиши. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "homeworkResponse",
    "linked_to_id": "212"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "105",
  "textid": "problem",
  "name": "Проблема",
  "message": "<i>В основе игры - уникальная способность, которой я обладаю – связь с будущим. В разработке меня принимали участие футурологи, физики, психологи и социологи. И в результате я получил возможность периодически связываться с информационной системой будущего. Я не могу отправить в будущее послание, и не могу получить информацию оттуда в обычном смысле этого слова. Но я могу замерить так называемый размер эффекта: какие последствия были у твоих действий, выполненных в настоящем. И подсказать, как лучше действовать сегодня, чтобы последствия были полезными для тебя и других. Мы с тобой будем общаться и договариваться о каких-то действиях здесь и сейчас в надежде на лучшее будущее :) <\/i>\r\n\r\nНам надо определиться, с чем именно мы будем работать в течение игры. \r\n#задание #проблема \r\nНакидай 3-5 вариантов, которые приходят в голову первыми. Посмотри на них внимательно и выбери одну главную проблему, которая соответствует следующим критериям:\r\n\r\n1. Эта ситуация\/проблема вызывает у тебя беспокойство, недовольство или другие неприятные эмоции\r\n2. Тебе действительно важно, чтобы произошли изменения, то есть ты хочешь ради этого прикладывать усилия и тратить время \r\n3. Эти изменения (или хотя бы значительную их часть) реалистично ожидать в течение месяца\r\n\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "probsol",
      "value": 1
    }],
    "text": "Так, и что делать?",
    "trigger": "",
    "linked_to": "problem1",
    "linked_to_id": "779"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "106",
  "textid": "goal",
  "name": "Проблема в цель",
  "message": "У тебя уже есть формулировка проблемы — то, что тебя не устраивает и что хотелось бы изменить. \r\n\r\n#задание #цель\r\nЭто наш отправной пункт. Но нам нужен и пункт прибытия — то, что ты хочешь получить в конце пути, твой желаемый результат и цель.\r\n\r\n<b>Подсказки<\/b>\r\n\r\nЧтобы превратить проблему в цель, задай себе несколько вопросов:\r\n\r\n1. Что я буду считать хорошим результатом?\r\n\r\n2. Что изменится во мне и в моей жизни, когда это произойдет?\r\n\r\n3. Как я пойму, что достиг(ла) этого результата? Постарайся описать конкретные признаки изменений, которые произойдут, когда ты получишь то, что хочешь. Будет здорово, если эти признаки сможешь непосредственно наблюдать не только ты, но и твое окружение.\r\n\r\nЗапиши цель на бумажку или сюда и жми \"Следующее задание\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующее задание",
    "trigger": "",
    "linked_to": "checkGoal",
    "linked_to_id": "270"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "107",
  "textid": "BasePlan",
  "name": "Планирование",
  "message": "У тебя есть цель по итогам предыдущего задания, что ж, пора сделать План. \r\nНезависимо от типа цели и стратегии достижения, мы двигаемся по такому циклу: ориентирование на местности -> постановка цели -> планирование -> исполнение плана -> оценка результата и новая итерация. \r\n\r\nНо в зависимости от типа цели и стратегии ее достижения будет варьироваться длина этого цикла, шаги и техники внутри и так далее. Посмотри, на что больше всего похожа твоя цель: \r\n\r\n1. Хочу принять сложное решение.\r\n2. Хочу сделать конкретное дело (проект), выполнить серию действий, но оно сложное. \r\n3. Хочу избавиться от прокрастинации.\r\n4. Хочу научиться выстраивать личные границы — защищать свое пространство, комфорт; говорить \"нет\". \r\n5. Хочу меньше зависеть от мнения окружающих, быть увереннее, иметь устойчивую самооценку. \r\n6. Хочу лучше понимать свои потребности, свои ценности.\r\n7. Хочу сформировать привычку.\r\n8. Ничего из этого, или что-то сложное и другое.\r\n ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "1. Сложное решение",
    "trigger": "",
    "linked_to": "externalStrategy",
    "linked_to_id": "119"
  }, {
    "on_click": [],
    "text": "2. Конкретное дело",
    "trigger": "",
    "linked_to": "external",
    "linked_to_id": "113"
  }, {
    "on_click": [],
    "text": "3. Прокрастинация",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }, {
    "on_click": [],
    "text": "4. Границы",
    "trigger": "",
    "linked_to": "boundaries",
    "linked_to_id": "233"
  }, {
    "on_click": [],
    "text": "5. Самооценка",
    "trigger": "",
    "linked_to": "selfEsteem",
    "linked_to_id": "239"
  }, {
    "on_click": [],
    "text": "6. Потребности",
    "trigger": "",
    "linked_to": "needs",
    "linked_to_id": "258"
  }, {
    "on_click": [],
    "text": "7. Сформировать привычку",
    "trigger": "",
    "linked_to": "habit",
    "linked_to_id": "402"
  }, {
    "on_click": [],
    "text": "8. Другое",
    "trigger": "",
    "linked_to": "strange",
    "linked_to_id": "345"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "113",
  "textid": "external",
  "name": "Внешние цели, план",
  "message": "Ты знаешь, как это сделать?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }, {
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "externalHelp",
    "linked_to_id": "122"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "BasePlan",
    "linked_to_id": "107"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "115",
  "textid": "SweetMotivation",
  "name": "Подбодрить",
  "message": "Давай, человек! У тебя все получится! Я в тебя верю!(как могу)",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/large.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/16387106_10211696738403952_725622403330789314_n-768x768.jpeg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/котикдоброта.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/i.gifer.com\/4Pi4.gif",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/котик не страдай херней.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/{2694a36b-118f-4126-ab18-97c580424c2d}clipboard.png",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/{79e89fa3-4cbd-4aaf-98c1-82a740ddba28}SC.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/{c891b099-2c06-4c55-b96c-b882d985e4ff}Подбадривание.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/{917cedfb-afbc-4755-9258-cc9c3a04329d}You+can+do+it_1c0d9c_5909836.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/{e2177e01-e089-493f-84ba-6a103b96320d}optimistic-opossum-new-zealand-33.png",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/{ee4a831b-debb-480f-b186-80fb0deccdad}2761281_0.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/далек.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/ковбои.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/не страдать херней.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/панда.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/Пион ты можешь.png",
    "format": "image"
  }],
  "send_random_attachment": true,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "119",
  "textid": "externalStrategy",
  "name": "Внешняя цель: представляю",
  "message": "#задание #план\r\n\r\nПоставь таймер на пять минут и выпиши все способы достижения цели, которые придут тебе в голову. Если ничего не приходит, попробуй спросить в чате или погуглить. Если все равно нет — пиши, что-то придумаем!\r\n\r\nСделай так, чтобы осталось 3-5 способов, безжалостно выкидывай сложные и вычурные планы. Чем план проще, тем он лучше. План должен состоять из конкретных физических действий. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сделано",
    "trigger": "",
    "linked_to": "alternatives",
    "linked_to_id": "124"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "122",
  "textid": "externalHelp",
  "name": "Внешняя цель: помогите",
  "message": "#задание #план\r\n\r\nОкей, попробуй вспомнить, удавалось ли кому-то подобное и в чем разница. Погугли ближайшее к твоей цели. \r\n\r\nПоштурми способы достижения цели пять минут по таймеру. \r\n\r\nНапиши в чат, там есть люди, они могут помочь. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сделано",
    "trigger": "",
    "linked_to": "alternatives",
    "linked_to_id": "124"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "external",
    "linked_to_id": "113"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "124",
  "textid": "alternatives",
  "name": "Выбрать из альтернатив",
  "message": "Теперь нужно выбрать из вариантов. Для этого нужно понять, как выбирать: что для тебя важно при выборе?\r\n\r\n#задание #план\r\nВыпиши свои критерии выбора.\r\n\r\nСкажем, если ты хочешь выучить английский, то тебе нужно определиться, что для тебя важно, например: чтобы нужно было как можно меньше выходить из дому, подтянуться к уровню какого-то экзамена, начать общаться с иностранцами и так далее. \r\n\r\nВот эти важные штуки — критерии оценки альтернатив (альтернативы были на прошлом шаге). \r\n\r\nТеперь давай проверим. Придумай любое решение, которое минимально удовлетворяет критериям, но которое тебе не понравилось бы (как во всяких злых сказках делает джинн). Добавь недостающие критерии. \r\n\r\nПодсказка: \r\nОбычно людям важны: скорость, дешевизна, легкость выполнения, простота плана, качество полученного результата, приятные\/неприятные взаимодействия в процессе и т.д.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "У меня все критерии",
    "trigger": "",
    "linked_to": "planning",
    "linked_to_id": "127"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "externalStrategy",
    "linked_to_id": "119"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "127",
  "textid": "planning",
  "name": "Выбрать из альтернатив; план",
  "message": "#задание #план\r\n\r\nДавай сделаем таблицу: строчки это альтернативы, столбики это критерии. \r\nДай каждому критерию вес от 1 до 10 (напиши рядом с названием критерия). \r\n\r\nТеперь оцени каждую альтернативу по каждому критерию и умножь на вес критерия. \r\nВуаля, у тебя табличка с выбором. Считай сколько в сумме у каждой альтернативы — это твой вариант. Нравится?\r\n\r\nЕсли не нравится, то вернись назад и проверь, все ли у тебя альтернативы, все ли у тебя критерии.  ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }, {
    "on_click": [],
    "text": "Нет :( ",
    "trigger": "",
    "linked_to": "externalStrategy",
    "linked_to_id": "119"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "alternatives",
    "linked_to_id": "124"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "130",
  "textid": "premortem",
  "name": "Премортем",
  "message": "#задание #план #премортем \r\n\r\nУ тебя есть способ, которым ты будешь достигать цели. Давай его немного детализируем и поколдуем. Напиши 6±2 пункта плана, можно с подпунктами в каждом. \r\n\r\nУбедись, что там везде понятный результат и проставлены сроки. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "План есть, сроки есть",
    "trigger": "",
    "linked_to": "premortem1",
    "linked_to_id": "328"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "136",
  "textid": "everyday",
  "name": "Мягкий пинок",
  "message": "#дневник\r\n\r\nМягкий ботячий пиночек. Как дела? Что было вчера и что будет сегодня? ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "137",
  "textid": "retro",
  "name": "Ретроспектива",
  "message": "Давай отрефлексируем произошедшее. \r\n\r\n#задание #ретро\r\n\r\nСейчас опять тебя расспрошу, уже о том, как прошел этот месяц для тебя. \r\nРетроспектива помогает оценивать свои стратегии и не повторять ошибок — ну ты наверняка это знаешь. \r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Хорошо!",
    "trigger": "",
    "linked_to": "finalquest1",
    "linked_to_id": "1465"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/панда обниманда.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "184",
  "textid": "procrastination",
  "name": "Начало прокрастинация",
  "message": "#прокрастинация\r\n\r\nПрокрастинация — это комплексная штука, она может проявляться по-разному и в ее основе могут быть разные причины. Давай выясним, как это устроено у тебя. При необходимости ты можешь пройти обе линии. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Мне трудно начать дело, но потом я втягиваюсь и нормально работаю",
    "trigger": "",
    "linked_to": "beginning",
    "linked_to_id": "286"
  }, {
    "on_click": [],
    "text": "Я начинаю дела, но мне трудно довести их до конца",
    "trigger": "",
    "linked_to": "ending",
    "linked_to_id": "291"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "186",
  "textid": "rest",
  "name": "Отдых",
  "message": "Давай попробуем  наладить баланс работы и отдыха в твоей жизни. \r\n\r\n#задание #отдых #прокрастинация\r\nНапиши список штук, которые тебе приятно делать и которые не связаны с работой. Попробуй делать их каждый день хотя бы пару дней. О результатах напиши.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "doable",
    "linked_to_id": "189"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "ending",
    "linked_to_id": "291"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "189",
  "textid": "doable",
  "name": "Выполнимо ли?",
  "message": "Отлично, давай дальше:\r\nТо, что ты ставишь себе как цели на день, удается выполнять хоть кому-то? Если оценить время выполнения каждой задачи и сложить, оно укладывается в рабочий день?\r\n\r\n#задание #прокрастинация #дела\r\nПопробуй пару дней оценивать каждую задачу, которую делаешь, по времени. Дальше, если ты планируешь на день задач больше, чем на 6 часов, то попробуй ставить более реалистичные цели.\r\n\r\nО результатах напиши. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "В начало задания про прокрастинацию",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "195",
  "textid": "conflictYES",
  "name": "Есть конфликт",
  "message": "#конфликт\r\n\r\nДля того, чтобы разрешить внутренний конфликт, можно поработать с метафорой частей личности.\r\n\r\nДеление личности на части не предполагает, что у нас и правда есть некие выделенные и автономные фрагменты. Мы обозначаем их условно, для того чтобы через это деление взглянуть на ситуацию с двух разных ракурсов и выстроить более цельное представление о себе, своих интересах и, далее, на основе более сложной модели, найти новые стратегии и подходы.\r\n\r\nВнутренний конфликт - это о том, что у нас есть две потребности, которые, по какой-то причине, мы не умеем удовлетворять в конкретной ситуации.\r\n\r\n<b>Примеры:<\/b>\r\n\"Я хочу больше общаться с людьми, это полезно для работы и развития (потребность в развитии), но мне страшно, что меня не поймут, не примут (потребность в принятии)\".\r\n\"Я бы хотела проявлять больше эмпатии в отношении мужа (потребность заботиться, потребность выражать чувства через заботу), но иногда у меня у самой просто нет на это сил (потребность в поддержке, участии, заботе, хорошем самочувствии, безопасности)\".\r\n\"Всякий раз, когда я берусь за новое дело я полон воодушевления и кажется, что готов свернуть горы (потребность в развитии, в осуществлении своего вклада в мир). Однако, сталкиваясь со сложностями я сдуваюсь. Это бывает в моменты, когда я не совсем понимаю, что делать и неопределённость меня пугает (потребность в определённости, ясности, предсказуемости).\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Понятно, дальше",
    "trigger": "",
    "linked_to": "conflict1",
    "linked_to_id": "418"
  }, {
    "on_click": [],
    "text": "К началу задания по прокрастинацию",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }, {
    "on_click": [],
    "text": "К заданию про привычки",
    "trigger": "",
    "linked_to": "habit1",
    "linked_to_id": "403"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "199",
  "textid": "triggerYES",
  "name": "Нет триггера",
  "message": "Даже собаки и голуби могут это! (могут сформировать привычку, если дать им понятный триггер, устроить так, чтобы действие было простым, и быстро выдать подкрепление)\r\n\r\nИтак, что нужно сделать:\r\nНапример, я хочу начать отжиматься, и я говорю себе, что когда я вытру лицо полотенцем после умывания утром, и посмотрю на себя в зеркало, я приму упор лежа и сделаю 5 отжиманий. После этого я улыбнусь себе в зеркале и похвалю себя. \r\n\r\nЯ действую по схеме: триггер-действие-подкрепление. \r\nТриггер — я смотрю на себя в зеркало, только что умывшись. Это простая штука, которая точно случится.\r\nДействие — упаду и 5 раз отожмусь. Небольшое, но даже в таком виде ценное!\r\nПодкрепление — улыбнусь себе и похвалю себя. Мне должно понравиться :) \r\n\r\n#задание #триггер #привычка\r\n\r\nПридумай себе такой триггер для дел, которые тебе нужно делать:\r\n\r\nПодсказки:\r\n1. Не пытайся внедрить сразу миллион привычек. Оптимально 1-3 привычки внедряются единовременно. \r\n2. Подроби дело, к которому не получается приступить, на маленькие кусочки, и делай триггеры на кусочки. \r\nНапиши, что получилось",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "beginning",
    "linked_to_id": "286"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "205",
  "textid": "depression",
  "name": "Депрессия",
  "message": "Окей. Если просто нет энергии ни на что, то апатия, спишь 10 часов в сутки, медленно двигаешься и говоришь, тревожишься, нет интереса, то пройди, пожалуйста, PHQ-9: https:\/\/www.b17.ru\/tests\/phq-9\/\r\n\r\nРасскажи что получилось, как теперь дела с прокрастинацией?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать задание",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "210",
  "textid": "bonusTask",
  "name": "Самоподдержка",
  "message": "Двигаться по плану, даже очень хорошо проработанному, бывает не такой уж простой штукой. Легко переходить от одного пункта к другому, если мы бодры, полны сил и можем сфокусироваться только на одной задаче. Но жизнь есть жизнь, и гораздо чаще мы делаем стопятьсот дел одновременно, устаем, реагируем на внезапно свалившиеся срочные задачи, переключаясь с уже запланированных и на каком-то этапе чувствуем себя обессиленными потерявшими изначальный задор. В этих ситуациях очень важно иметь наготове способы поддержать себя. Вот несколько советов, которые помогут это сделать:\r\n\r\n1. Прими тот факт, что ты - человек, сила воли и вообще силы не бесконечны и это нормально. Хотя это звучит парадоксально, но именно мягкое и доброжелательное отношение к своим спадам и срывам (вместо критики и самообвинения) дает гораздо больше сил вернуться обратно в нужное русло.\r\nИсследования показали, что люди, способные к самосостраданию, не только чувствуют себя лучше, но и работают более продуктивно.\r\n2. Останавливай внутреннего “критика”. Всегда лучше самостоятельно выбирать, поддерживать его или игнорировать.\r\n3. Вовремя отдыхай. Любая батарейка требует своевременной подзарядки, и твоя тоже. Не ожидай, что сможешь фигачить без остановки, еды, сна и прочих ненужных роботам, но необходимых людям вещей. \r\n\r\nА вот и #задание:\r\n1. Напиши слова поддержки самому себе в будущем, когда ты столкнешься с падением мотивации и усталостью.\r\n2. Напиши слова поддержки для игроков Зарешая в такой же ситуации и скинь их в чат.\r\n\r\nМожно написать текстом, можно голосом - как тебе больше нравится.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "К заданиям про прокрастинацию",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "212",
  "textid": "homeworkResponse",
  "name": "Спасибо, принято",
  "message": "Спасибо, принято",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "219",
  "textid": "bonusTaskGroup",
  "name": "Групповое задание",
  "message": "Итак, в разгаре стадия реализации. \r\n\r\nЭто самая скучная стадия, но именно \r\nумение выдерживать её и обеспечивает успех. Бродский призывал \"полюбить \r\nскуку\". Мы попробуем привнести в неё немного разнообразия.\r\n\r\n<b>Задание<\/b>\r\nПроведите сеанс синхронной работы с другими участниками Зарешая. \r\nМинимум на 1 час.\r\n\r\n<b>Подсказки<\/b>\r\nСамоорганизуйтесь в общем чате. Для выбора подходящей даты\/времени, \r\nможно использовать doodle: https:\/\/doodle.com\/ \r\nМожете использовать zoom https:\/\/zoom.us\/ или специальный инструмент для \r\nонлайн коворкинга https:\/\/complice.co\/rooms – Алиса рекомендует!\r\nДоговоритесь заранее о длительности рабочей сессии и перерыва.\r\nФигачьте! В перерывы знакомьтесь больше друг с другом.\r\n\r\n<b>Проверка<\/b>\r\nПришлите боту скриншот вашей сессии или фото встречи, а также описание, \r\nчто удалось сделать.\r\n\r\n#задание #работа #дневник",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "execute",
    "linked_to_id": "377"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "233",
  "textid": "boundaries",
  "name": "Границы",
  "message": "Цель звучит как:\r\n- хочу научиться выстраивать границы с окружающими\r\n- хочу научиться отказывать\/высказывать свое мнение открыто\/говорить неприятные вещи.\r\n\r\n#границы #теория\r\n1. Усвой мысль, что у тебя, как и у каждого человека на земле, есть ПРАВО НА ОТКАЗ. Оно такое же несомненное и неотъемлемое, как право дышать, двигаться и жить. Ты им имеешь полное право пользоваться по своему усмотрению, в любой момент времени. \r\n\r\n2. Если ты не будешь пользоваться правом на отказ, а будешь соглашаться, ты все равно не защитишь себя от дальнейших сложностей. Соглашательство — не гарантия любви и принятия окружающих. Или гарантия \"любви\" только тех людей, которые предпочитают видеть рядом с собой податливых и робких, а не самостоятельных и зрелых личностей. Подумай, интересно ли тебе быть связанной(ым) именно с такими людьми или лучше быть с теми, кто уважает твою автономию и право на собственное мнение. Отказ\/несогласие может быть для тебя фильтром на адекватность человека и способом формировать более приятное и зрелое окружение вокруг себя.\r\n\r\n3. Люди не умирают от отказов. Им может быть неприятно, они могут быть фрустрированы, некоторые даже могут обижаться, но см. пункт 2 — зрелые люди уважают право другого иметь свое мнение, и это их не травмирует необратимо. А подпорченное настроение рано или поздно проходит, это же часть жизни :) По этим трем пунктам #задание - напиши, что ты про это думаешь? Хотел(а) бы ты \"присвоить\" эти мысли и сделать их частью внутренних убеждений? Если нет, объясни, что тебе в них не близко. Если да, напишу тебе алгоритм, как отказывать вежливо и с достоинством :)",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, расскажи как отказывать",
    "trigger": "",
    "linked_to": "sayNO",
    "linked_to_id": "236"
  }, {
    "on_click": [],
    "text": "Не хочу отказывать",
    "trigger": "",
    "linked_to": "dontSayNO",
    "linked_to_id": "235"
  }, {
    "on_click": [],
    "text": "Я умею отказывать",
    "trigger": "",
    "linked_to": "DESC",
    "linked_to_id": "238"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "BasePlan",
    "linked_to_id": "107"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "235",
  "textid": "dontSayNO",
  "name": "Не хочу отказывать",
  "message": "Расскажи, почему. Что ты ощущаешь, что происходит?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "boundaries",
    "linked_to_id": "233"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "236",
  "textid": "sayNO",
  "name": "Научиться отказывать",
  "message": "#задание #границы #техника\r\n\r\n1. Внимательно выслушай, что говорит собеседник\r\n2. Дай знать, что ты понимаешь его потребность, уважаешь его желание, признаешь право на это\r\n3. Скажи, что несмотря на это не можешь выполнить его просьбу\/ожидание (чем четче, лаконичнее и понятнее скажете, тем лучше)\r\n4. Обоснуй отказ, если хочешь. Но помни, что ты имеешь полное право отказать, не вдаваясь в подробности!\r\n5. Опять же, по желанию, можешь сказать человеку что-то хорошее — пожелать удачи, предложить другой вариант или ресурс для решения его задачи\r\n\r\nПопробуй отказываться от неприятного в течении пары дней, а потом приходи рассказать. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "boundaries",
    "linked_to_id": "233"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "238",
  "textid": "DESC",
  "name": "DESC",
  "message": "#задание #границы #техника\r\n\r\nОтстаивание границ, техника DESC\r\n\r\n1. D (Describe) — детально опиши ситуацию и поведение собеседника, как ты его видишь. Постарайся быть максимально объективной(ым), не примешивай свои эмоции и интерпретации, этому будет отведено специальное место.\r\n\r\n2. E (express) — вырази свое отношение к ситуации, которую ты описал(а). Расскажи о своих эмоциях, несогласии. Сделай это от первого лица с помощью Я-сообщения (не надо обвинять человека и описывать, какой он плохой).\r\n\r\n3. S (specify) — опиши очень конкретно, как ты видишь поведение собеседника, которое тебе больше бы понравилось. Скажи \"я бы хотел_а..., чтобы вы вели себя...\", \"мне было бы комфортнее, если бы в такой ситуации вы...\". Пусть твое описание будет максимально нейтральным, спокойным и касаться поведения человека, а не его личности, черт характера и пр.\r\n\r\n4. C (consequences) — обозначь последствия, хорошие и плохие, при двух вариантах развития событий. Опиши, что станет лучше, в том числе для него самого, если поведение человека изменится. И что будет, если его поведение останется прежним. Важно озвучивать только те последствия, которые ты будешь готов(а) воплотить в жизнь — это ключевой момент!\r\n\r\nОбрати внимание на то, что чем более лаконичными, спокойными и уважительными будут твои реплики, тем лучше. Твоя цель — не задавить собеседника, даже если ты сам(а) чувствуешь давление с его стороны, а обозначить границы, за которые ты не собираешься его пускать.\r\n\r\nПотренируйся пару дней и расскажи как прошло.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "boundaries",
    "linked_to_id": "233"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "239",
  "textid": "selfEsteem",
  "name": "Самооценка и независимость",
  "message": "Цель звучит как:\r\n- хочу быть источником нормы и могу жить, действовать, выбирать, как считаю сам_а, не оглядываясь вокруг постоянно в ожидании сверки с внешней нормой\r\n- хочу быть более независимой\/ым от мнения окружающих\r\n\r\n#задание #автономность #техника\r\n\r\nНапиши список своих качеств\/убеждений о себе, в которых ты настолько уверен_а, что даже если бы 5-7 авторитетных для тебя человек сказали бы, что это ерунда и ты на самом деле не такая\/ой, ты бы не поверил_а и не изменил_а своего мнения.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "threat",
    "linked_to_id": "253"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "BasePlan",
    "linked_to_id": "107"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "241",
  "textid": "bonusAversion",
  "name": "Избегания",
  "message": "Чего ты избегаешь? Смотри на дела, которые ты считаешь нужным делать и вообще твоя жизнь была бы лучше, если бы ты их делал(а), но чет не выходит. \r\n\r\n#задание #избегание \r\nЗапиши, что это за занятие",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "aversionPros",
    "linked_to_id": "243"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "243",
  "textid": "aversionPros",
  "name": "Избегание — плюсы",
  "message": "Запиши все причины, почему это стоит делать: визуализируй как будет классно, какой ты получишь хороший результат, как ты будешь себя чувствовать. \r\n\r\n#задание #избегание",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "aversionCons",
    "linked_to_id": "245"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "245",
  "textid": "aversionCons",
  "name": "Избегание — минусы",
  "message": "Теперь запиши, почему ты не хочешь это делать: что-то кажется тебе страшным, неприятным? Что-то вызывает конкретные плохие ощущения? \r\n\r\n#задание #избегание",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "aversionElem",
    "linked_to_id": "247"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "247",
  "textid": "aversionElem",
  "name": "Избегание — справиться",
  "message": "Теперь — сюрприз — мы не будем их сравнивать, мы просто избавимся от минусов. Придумай, как убрать каждый минус, или спроси коллег в общем чате. \r\n\r\n#задание #избегание",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "К ветке про прокрастинацию",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }, {
    "on_click": [],
    "text": "К решению внутреннего конфликта",
    "trigger": "",
    "linked_to": "conflictYES",
    "linked_to_id": "195"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "253",
  "textid": "threat",
  "name": "Угроза самооценке",
  "message": "Ты хорошо справляешься! Теперь скажи, как ты думаешь, какие убеждения о себе становятся под угрозу, когда ты боишься внешних оценок, мнения других людей?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "myFacts",
    "linked_to_id": "255"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "selfEsteem",
    "linked_to_id": "239"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "255",
  "textid": "myFacts",
  "name": "Факты обо мне",
  "message": "Отлично, двигаемся дальше! \r\n\r\n#задание #автономность\r\nПодбери по 3-5 значимых для тебя фактов, которые подтверждают те качества, которые ты выделил_а? На чем основывается твоя уверенность, что ты именно такая\/ой? Что ты можешь внутренне \"предъявить\" в ответ на неблагоприятную реакцию окружающих?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать задание",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "threat",
    "linked_to_id": "253"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "258",
  "textid": "needs",
  "name": "Лучше понимать потребности",
  "message": "Твоя цель похожа на это?\r\n- хочу лучше понимать свои #потребности\r\n- хочу научиться замечать свои успехи и поощрять себя\r\n\r\nДумаю, мы с тобой справимся :) Развитие внимательности к своим потребностям и успехам бывает довольно долгим путем, который не ограничивается месяцем игры. Но за месяц можно освоить несколько техник, потренировать их применение в жизни и наметить план дальнейших действий. Ты готов_а?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да!",
    "trigger": "",
    "linked_to": "awareness",
    "linked_to_id": "261"
  }, {
    "on_click": [],
    "text": "Не знаю...",
    "trigger": "",
    "linked_to": "needsDunno",
    "linked_to_id": "260"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "BasePlan",
    "linked_to_id": "107"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "260",
  "textid": "needsDunno",
  "name": "Не уверен про потребности",
  "message": "Расскажи, в чем твои сомнения? ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "261",
  "textid": "awareness",
  "name": "Минутка осознанности",
  "message": "Тогда поехали!\r\nВот первое #задание:\r\n\"Минутка осознанности\" - настрой 5-6 напоминалок на телефоне в течение дня и по звонку отметь (письменно), в каком состоянии ты находишься, какие #эмоции испытываешь в этот момент?\r\nЗапиши их. Попробуй рядом с каждой эмоцией обозначить, какая твоя потребность в этот момент удовлетворена (если эмоция положительная) или нет (если эмоция отрицательная).\r\nНапример, ты можешь быть напряженным_ой или злым_ой, грустным_ой, если чувствуешь, что какие-то твои успехи остались незамеченными (потребность в признании)\r\nИли можешь радоваться, если получилось сделать что-то, чего не умел_а раньше (потребность чувствовать себя компетентным_ой)\r\n#потребности",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать задние",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "needs",
    "linked_to_id": "258"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "263",
  "textid": "diary",
  "name": "Дневник успеха",
  "message": "Ты молодец! А вот и следующее #задание:\r\nВ течение 3 дней веди Дневник успеха: постарайся замечать свой опыт успешного решения задач - в счет идут даже те успехи, которые ты считаешь не очень значительными. Если тебе трудно найти такие, спроси у друзей, коллег, что они считают твоими маленькими достижениями и что у тебя хорошо получается.\r\nНапиши, что удалось, а что было трудным.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать задание",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "270",
  "textid": "checkGoal",
  "name": "Проверить цель",
  "message": "Теперь давай проверим цель.\r\n\r\n<b>1. Позитивная формулировка.<\/b> Легче достигать целей, которые описывают то, что ты хочешь получить, а не то, от чего хочешь избавиться. Например, можно сказать \"я хочу перестать прокрастинировать\" — это будет негативной формулировкой, и она не дает представления о том, что же я хочу делать вместо этого. А можно сказать \"я хочу научиться занимать свое время чем-то интересным и\/или полезным\" или \"я хочу научиться снимать стресс другим способом\".\r\n\r\nЕсли твоя цель не соответствует этому критерию, переформулируй ее. Отправь мне.\r\nЕсли соответствует, то напиши \"Соответствует\" ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "checkGoal1",
    "linked_to_id": "272"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "272",
  "textid": "checkGoal1",
  "name": "Проверить цель1",
  "message": "<b>2. Конкретность и понятность.<\/b> Звучит ли твоя цель так, чтобы не составило труда проверить, действительно ли ты получил(а) то, что хотела? Если цель - \"быть счастливым\", \"улучшить свою жизнь\", \"изменить себя\", — она не обладает конкретностью. Если ты указываешь, какое именно поведение, мысли, эмоции и пр. ты хочешь получить на выходе, это достаточно конкретная цель. Также в формулировке цели старайся использовать слова, которые будут понятны любому человеку, даже ребенку лет 7-9. Чем проще описана цель, тем легче проверить, достиг(ла) ли ты ее.\r\n\r\nЕсли формулировку нужно изменить, сделай это. Напиши мне. \r\nЕсли соответствует, то напиши \"Соответствует\" ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "checkGoal2",
    "linked_to_id": "274"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "274",
  "textid": "checkGoal2",
  "name": "Проверить цель 2",
  "message": "<b>3.  Достижимость.<\/b>Позже мы еще вернемся к оценке твоих ресурсов для достижения цели. А пока ответь себе на вопрос — включает ли моя цель изменения, которые должны произойти с другими людьми или обстоятельствами, которые от меня не зависят или почти не зависят? Скажем, если ты хочешь, чтобы другие люди стали лучше тебя понимать или больше любить и уважать, это не будет хорошей формулировкой цели, потому что ее выполнение часто не зависит от тебя. Твоя формулировка должна включать только то, что связано лично с тобой, с твоими решениями, поведением и состоянием.\r\n\r\nКогда уверишься, что этот критерий тоже соблюдается, пришли мне финальную формулировку цели. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "didgoal",
    "linked_to_id": "364"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "276",
  "textid": "homeworkGoal",
  "name": "Сдать цель",
  "message": "Теперь мне надо подумать над этим и посовещаться с людьми. Максимум, через 48 часов я вернусь и скажу, что делать дальше. А пока на, почитай: \r\n\r\nhttps:\/\/theoryandpractice.ru\/posts\/8956-stavit-tsel\r\n\r\nА еще посмотри хелп, как со мной обращаться https:\/\/telegra.ph\/Help-Zareshaj-bota-10-04",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "286",
  "textid": "beginning",
  "name": "Трудно начинать дела",
  "message": "Понимаю, такое бывает. Как ты думаешь, какая причина откладывания начала дел больше всего подходит тебе?\r\n\r\n1. Я боюсь что у меня все равно ничего не выйдет\r\n2. У меня есть внутренний конфликт — и хочу это делать и нет\r\n3. Мне трудно начать, потому что у меня почти всегда нет четкого плана когда и как начинать\r\n4. Мне трудно начать делать дело, потому что я не могу организовать подходящие условия для этого\r\n5. Я вроде хочу это делать, но почему-то не начинаю — кажется, у меня избегание",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Ничего не выйдет",
    "trigger": "",
    "linked_to": "catastrophe",
    "linked_to_id": "288"
  }, {
    "on_click": [],
    "text": "Внутренний конфликт",
    "trigger": "",
    "linked_to": "conflictYES",
    "linked_to_id": "195"
  }, {
    "on_click": [],
    "text": "Нет плана",
    "trigger": "",
    "linked_to": "triggerYES",
    "linked_to_id": "199"
  }, {
    "on_click": [],
    "text": "Нет условий",
    "trigger": "",
    "linked_to": "environment",
    "linked_to_id": "289"
  }, {
    "on_click": [],
    "text": "Избегание",
    "trigger": "",
    "linked_to": "bonusAversion",
    "linked_to_id": "241"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "288",
  "textid": "catastrophe",
  "name": "Техника декатастрофизации",
  "message": "#задание #прокрастинация\r\n\r\nОпиши, что конкретно случится в физическом мире? Нужны очень четкие сценарии. \r\nПосмотри на эти сценарии еще раз и рассортируй — реалистичные в одну сторону, нереалистичные в другую.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Ок, отпустило. А что делать с реалистичными?",
    "trigger": "",
    "linked_to": "didcatastrophe",
    "linked_to_id": "391"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "beginning",
    "linked_to_id": "286"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "289",
  "textid": "environment",
  "name": "Среда для работы",
  "message": "Был ли у тебя опыт, когда тебе удавалось создать условия для легкого включения в работу?\r\n\r\n#задание #работа\r\nОпиши, как выглядело рабочее место, что происходило в жизни, какие звуки, тактильные ощущения, запахи — любая сенсорная информация пригодится. \r\n\r\nЕсли не было такого опыта, то напиши об этом тоже. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "beginning",
    "linked_to_id": "286"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "291",
  "textid": "ending",
  "name": "Трудно доводить до конца",
  "message": "Понимаю, такое бывает. Как ты думаешь, какая причина откладывания завершения дел больше всего подходит тебе?\r\n\r\n1. Я не умею мотивировать себя, по ходу выполнения дела теряю к нему интерес\r\n2. Я постоянно заставляю себя что-то делать или хотя бы думать о делах, в итоге мало и плохо отдыхаю\r\n3. Мне трудно последовательно делать дела, потому что у меня почти всегда нет четкого плана для их выполнения",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я не умею мотивировать себя",
    "trigger": "",
    "linked_to": "bonusTask",
    "linked_to_id": "210"
  }, {
    "on_click": [],
    "text": "Плохо отдыхаю",
    "trigger": "",
    "linked_to": "rest",
    "linked_to_id": "186"
  }, {
    "on_click": [],
    "text": "Нет плана",
    "trigger": "",
    "linked_to": "externalStrategy",
    "linked_to_id": "119"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "procrastination",
    "linked_to_id": "184"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "328",
  "textid": "premortem1",
  "name": "Премортем 1",
  "message": "Теперь представь, что из будущего пришла весточка: у тебя ничего не вышло!\r\n\r\n#премортем",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Невозможно!",
    "trigger": "",
    "linked_to": "overConf",
    "linked_to_id": "330"
  }, {
    "on_click": [],
    "text": "Странно, но могу поверить",
    "trigger": "",
    "linked_to": "premortem2",
    "linked_to_id": "331"
  }, {
    "on_click": [],
    "text": "Неудивительно",
    "trigger": "",
    "linked_to": "premortemNew",
    "linked_to_id": "336"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "330",
  "textid": "overConf",
  "name": "Сверхуверенность",
  "message": "Возможно, у тебя сверхуверенность. https:\/\/ru.wikipedia.org\/wiki\/Эффект_сверхуверенности \r\n\r\nДругой вариант — у тебя очень хороший простой план с высокой надежностью. Поздравляю! \r\n\r\nТогда просто опиши его. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "331",
  "textid": "premortem2",
  "name": "Премортем2",
  "message": "Расскажи историю о том, почему ничего не вышло. #премортем\r\n\r\nЗапиши все истории, которые приходят в голову, если надо — спроси товарищей. Они всегда готовы рассказать, что пойдет не так :) \r\n\r\nВспомни про эмпатический разрыв (ты сейчас вряд ли понимаешь, как будешь себя чувствовать через пару недель) и предвзятость подтверждения (люди больше любят факты, которые подтверждают их точку зрения, чем наоборот). \r\n\r\nОбязательно оцени, сколько времени у тебя займет реализация и что может растянуть сроки. После этого накинь 25% и посмотри, есть ли у тебя вообще такой бюджет времени.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово, дальше",
    "trigger": "",
    "linked_to": "premortem3",
    "linked_to_id": "333"
  }, {
    "on_click": [],
    "text": "Сначала",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "333",
  "textid": "premortem3",
  "name": "Премортем3",
  "message": "Возьми наиболее вероятные и угрожающие сценарии и придумай (может быть, с помощью чата), что можно сделать, чтобы в них не попасть или минимизировать риск. \r\n\r\nРасскажи финальный план, который получился (или напиши, если ничего не получилось)\r\n\r\n#задание #премортем",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сдать",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Сначала",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "336",
  "textid": "premortemNew",
  "name": "Плохой план, переделывай",
  "message": "Если тебя не удивляет, что твой план провалился, то, может быть, стоит выкинуть его целиком и сделать новый. Попробуй спросить совета в чате, а затем начни заново.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сначала",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "345",
  "textid": "strange",
  "name": "Странная цель",
  "message": "Сейчас позову людей, они разберутся",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "361",
  "textid": "17august19",
  "name": "17 августа ",
  "message": "Отлично, ты играешь!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Получить первое задание",
    "trigger": "",
    "linked_to": "problem",
    "linked_to_id": "105"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "364",
  "textid": "didgoal",
  "name": "Сделал Цель",
  "message": "Прекрасно! Пройден второй этап. \r\n\r\nФормулирование цели на 30% увеличило вероятность того, что ты сообщишь об успешном достижении к концу игры. Твоя удовлетворенность жизнью несколько увеличилась на ближайшее время. Так же, как твоя вера в собственные силы и возможность влиять на свою жизнь. Благотворное воздействие от вложенных усилий наблюдается не только в рамках поставленной цели, но и в других областях жизни. \r\n\r\nЗатухающие волны позитивных эффектов распространяются от сегодняшнего дня далеко в будущее.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "homeworkGoal",
    "linked_to_id": "276"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "377",
  "textid": "execute",
  "name": "Выполнение!",
  "message": "Поздравляю, ты приступаешь или вот-вот приступишь к выполнению плана. Есть несколько полезностей, которые стоит учесть при выполнении любого дела, чтобы не упороться и дойти до финиша живым и довольным человеком)\r\n\r\n#теория #план\r\n\r\n1. Внутри любого плана есть главные задачи и второстепенные. Мы частенько кидаемся решать не очень главные, но понятные задачи, откладывая сложные, но важные. Чекай себя время от времени на этот предмет. \r\n2. Определи свои сильные стороны и опирайся на них. То, в чем ты пока недостаточно компетентен, попробуй делегировать или хотя бы привлекай помощь - это нормально и продвинет тебя гораздо больше, чем стремление \"через не могу\" зарешать все самостоятельно.\r\n3. Планируй выполнение сложных частей плана на пиковые периоды продуктивности. Отметь, в какие периоды времени ты наиболее эффективно работаешь.\r\n4. По возможности организуй комфортную среду для работы - чтобы никто и ничто не отвлекало тебя\r\n5. Так же тщательно, как и работу, спланируй отдых и время на восстановление. Способы выбери те, которые подходят и приятны лично тебе - прогулки, чил на газоне в парке, физическая активность, общение с приятными людьми, медитация, массаж, прослушивание музыки и пр.\r\n6. Развивай свою способность реалистично оценивать возможности. Раз в неделю оглядывайся назад и подводи итог - сколько было запланировано и сколько сделано. Через пару месяцев у тебя накопится достаточно материала, чтобы понять реальные объемы работы, которые ты успеваешь выполнить в единицу времени. Скорректируй свои планы с учетом этого, не старайся выкроить двенадцать шапочек из одной шкурки :) Не бери на себя больше, чем можешь, без риска эмоционального и физического истощения.\r\n\r\n(текст подготовлен с использованием материалов из блога @geek.psychology)\r\n\r\nВот еще три задания, которые нужно сделать в любой момент в процессе исполнения: ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Пятиминутки",
    "trigger": "",
    "linked_to": "fiveminutes",
    "linked_to_id": "380"
  }, {
    "on_click": [],
    "text": "Особый резон",
    "trigger": "",
    "linked_to": "extrawork",
    "linked_to_id": "381"
  }, {
    "on_click": [],
    "text": "Групповая работа",
    "trigger": "",
    "linked_to": "bonusTaskGroup",
    "linked_to_id": "219"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "380",
  "textid": "fiveminutes",
  "name": "Пятиминутки",
  "message": "Делать сложно. Но делать полезно. Будем разноображивать процесс деланья. \r\n\r\n#задание #дневник #работа\r\n\r\nПрименить технику \"пятиминутки\" для достижения твоей цели.\r\n\r\nЧто именно делать\r\n\r\nЗаведи таймер на 5 минут. И попробуй решить проблему целиком. Не просто составить план, а решить.\r\nЕсли за 5 минут не получилось, заведи таймер ещё на 5 минут. За это время составь список идей для следующих пятиминуток.\r\nВыбери наиболее интересную идею из описанных и выполни за следующие 5 минут.\r\nПродолжай пока достигнешь результата. Или пока не устанешь.\r\nЗавершение\r\n\r\nРасскажи боту, что удалось сделать за это задание и какие у тебя впечатления? Можешь написать об этом в общий чат, бот приветствует.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "execute",
    "linked_to_id": "377"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "381",
  "textid": "extrawork",
  "name": "Необычные условия для работы",
  "message": "https:\/\/telegra.ph\/Dopolnitelnoe-zadanie-dlya-ehtapa-realizacii--osobyj-rezon-07-26 ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "execute",
    "linked_to_id": "377"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "386",
  "textid": "didplan",
  "name": "Сделал план",
  "message": "План создан. Ты молодчина!\r\nТеперь поисковая выдача в 2050 находит 12 дополнительных статей при запросе твоего имени. Количество таких запросов в течение года выросло на 25-45 штук. Удовлетворенность жизнью согласно самоотчёту в 2050 году увеличилась на 0,023%. Ощущение управления своей жизнью переживается тобой на 3% чаще в течение ближайшего года. Это помогает тебе чаще действовать с учётом собственных интересов. На 0,06 процентных пунктов смещается баланс «влияние обстоятельств\/авторская позиция» в сторону «авторской позиции». \r\nГотов(а) двигаться дальше?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Приступить к выполнению!",
    "trigger": "",
    "linked_to": "execute",
    "linked_to_id": "377"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "391",
  "textid": "didcatastrophe",
  "name": "Сделал декатастрофизацию",
  "message": "Поздравляем!\r\n\r\nТеперь у тебя на 0,12% меньше искажения прогнозов.\r\n\r\nПовтори это еще 100 раз, и будет 12% :)\r\n\r\nЛюди в будущем избавились от искажений мышления, которые портили прогнозы в сторону катастрофизации. \r\n\r\nСделай премортем для реалистичных сценариева, чтобы защитить свой план",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Премортем",
    "trigger": "",
    "linked_to": "premortem",
    "linked_to_id": "130"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "catastrophe",
    "linked_to_id": "288"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "400",
  "textid": "didboundaries",
  "name": "Сделал границы",
  "message": "Прекрасно! \r\nВ данный момент, человеческое общество проходит трансформацию. Увеличивающаяся ценность человеческой жизни приводит к тому, что личные границы и их соблюдение становятся базовыми навыками просвещенного человека. Ты в авангарде! Да, к сожалению, больше нагрузки ложится именно на тех, кто больше может. Такова жизнь.\r\n\r\nВ будущем, образование и просвещение делают гораздо больше для развития психологических компетенций. Поэтому люди с детского возраста осознанно относятся к личностным границам и уважают индивидуальные потребности других людей. В основе такого  волшебного мира лежат и твои сегодняшние усилия. \r\n\r\nЧудесный мир будущего стал ближе на несколько дней.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Практиковать привычку дальше",
    "trigger": "",
    "linked_to": "habit",
    "linked_to_id": "402"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "402",
  "textid": "habit",
  "name": "Сформировать привычку",
  "message": "Первое и самое главное: если вместо установки привычки того же результата можно добиться автоматизацией\/делегированием\/изменением среды - то подумай, не будет ли это проще.\r\n\r\nНо если ты здесь и хочешь формировать привычку, то жми \"дальше\" :) \r\n\r\n#привычка #теория",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "habit1",
    "linked_to_id": "403"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "403",
  "textid": "habit1",
  "name": "Привычка1",
  "message": "Напиши, какое действие ты хочешь ввести в привычку. #привычка\r\n\r\n<b>Подсказки: <\/b>\r\n1. Действие должно быть минимальным и легко исполнимым. Типа \"надеть кроссовки\" для привычки бегать по утрам, а не \"одеваться, выходить на улицу и полчаса бегать\". Маленькое действие вызывает у мозга меньше сопротивления, а там уже и втянешься. \r\n2. Само действие не должно быть вредным или разрушительным. \r\n3. У тебя должна быть уверенность, что ты хочешь его установить, и не имеешь внутренних конфликтов по этому поводу. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Все соответствует",
    "trigger": "",
    "linked_to": "habit2",
    "linked_to_id": "404"
  }, {
    "on_click": [],
    "text": "Есть внутренний конфликт",
    "trigger": "",
    "linked_to": "conflictYES",
    "linked_to_id": "195"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "habit",
    "linked_to_id": "402"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "404",
  "textid": "habit2",
  "name": "Привычка2",
  "message": "Теперь давай придумаем, какой может быть триггер. Триггер — это такое событие, которое точно произойдет независимо от тебя, и которое будет сигналом, что пора выполнять действие. Например \"услышать звонок будильника\", или \"встать с кровати\". Иногда бывает хорошо сделать триггер типа \"увидеть стакан воды у постели\", но только если ты вообще никогда не забываешь его туда ставить. \r\nИтак, выбирай понятный и точно срабатывающий триггер. \r\n#привычка",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "habit3",
    "linked_to_id": "405"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "habit2",
    "linked_to_id": "404"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "405",
  "textid": "habit3",
  "name": "Привычка3",
  "message": "У тебя есть связка из триггера и действия, хорошо бы еще изобрести подкрепление за следование привычке. Это может быть что угодно маленькое и приятное тебе. Еще это может быть отложенное подкрепление, например, за хорошее исполнение схемы триггер-действие несколько раз. \r\n\r\nЕсли не получается придумать подкрепление, то спроси в чате!\r\n\r\n#привычка",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "habit4",
    "linked_to_id": "406"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "habit3",
    "linked_to_id": "405"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "406",
  "textid": "habit4",
  "name": "Привычка4",
  "message": "Итак, есть полный набор, триггер-действие-подкрепление. \r\n\r\nИногда целевая ситуация повторяется слишком редко, и нужно предпринять специальные усилия, чтобы сделать ее привычной и более частой. Для этого можно спроектировать и сделать десяток повторений одного триггера, чтобы сделать реакцией на него действие, чтобы привычка закрепилась быстрее. \r\nНапример, если ты хочешь вставать по будильнику, то ты можешь поставить будильник на каждые пять минут, раздеться, залезть в постель, а когда будильник зазвонит — сразу встать. Это облегчит следование схеме в условиях, когда тебе будет мешать сонливость — мозг уже привыкнет. \r\n\r\nОсталось практиковать! #задание #привычка\r\nКак только попробуешь дважды, напиши, что получилось, а что нет. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Понятно, буду практиковать!",
    "trigger": "",
    "linked_to": "perfect",
    "linked_to_id": "415"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "415",
  "textid": "perfect",
  "name": "Отлично",
  "message": "Отлично, удачи!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "418",
  "textid": "conflict1",
  "name": "Конфликт1",
  "message": "1. Определи с какими позициями будешь работать. Назови эти части как-нибудь необидно. Не надо \"разумная часть\" и \"идиот\":) Можно \"часть за зарабатывание денег\" и \"часть за качественный отдых\"\r\n\r\n#конфликт",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово, дальше",
    "trigger": "",
    "linked_to": "conflict2",
    "linked_to_id": "419"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "conflictYES",
    "linked_to_id": "195"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "419",
  "textid": "conflict2",
  "name": "Конфликт2",
  "message": "2. Выпиши аргументы обеих позиций в виде прямой речи, и определи потребность. \r\n\r\nНапример:\r\n\r\nЧасть 1: Нужно браться за новые и вдохновляющие дела! Это очень здорово и ты свернёшь горы, вперёд! \/ Потребности: развитие, вклад в мир.\r\nЧасть 2: Слушай, ну вот ты начнёшь опять это всё делать, а потом сдуешься. Мы уже делали дела и оказалось супер сложно. Давай-ка лучше завяжем с этим, ты попробовал, не вышло - не беда, найдёшь что-нибудь ещё. \/ Потребности: эмоциональная безопасность, ясность, определённость, предсказуемость.\r\n\r\nБазовая идея этой практики в том, чтобы начать видеть, что обе части так или иначе \"за тебя\", они обе о том, чтобы \"как лучше\". И что хоть они и говорят часто прямо противоположные штуки, их объединяет одно: они обе заботься о тебе. Именно это общее и позволяет на третьем шаге помочь частям \"договориться\".\r\n\r\n#задание #техника #конфликт",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сделано, дальше",
    "trigger": "",
    "linked_to": "conflict3",
    "linked_to_id": "420"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "conflict1",
    "linked_to_id": "418"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "420",
  "textid": "conflict3",
  "name": "Конфликт3",
  "message": "3. Создай вин-вин стратегию. \r\n\r\nПродолжая пример, решение может быть такое:\r\n\"Я могу продолжать делать большие дела, однако я буду уделять больше внимание сбору информации относительно дела и продумывать подробный план с учётом подводных камней. В случае, если я столкнусь с чем-то неизвестным - я буду выносить это в отдельную задачу. Я могу искать все необходимые мне данные сам(а), могу развивать навыки, которые мне понадобятся и обращаться за помощью к компетентным специалистам. Теперь это часть общего плана.\" \/ Итог - все потребности учтены: дело не прерывается, а где нет определённости - они добирается.\r\n\r\nВажно внимательно отнестись к каждой части. Часто во внутренних конфликтах у нас есть желание больше сопереживать какой-то одной из сторон. К примеру, мы можем сопереживать той части, которая хочет работать, потому что денег нужно больше, чем есть, а той части, которая боится неопределённости, мы не сочувствуем. Мы можем даже корить себя за \"малодушее, трусость, слабость\" и так далее.\r\n\r\nИтогом работы с внутренним конфликтом должна быть договорённость по принципу win-win - когда обе части полностью удовлетворены. Варианты с компромиссными решениями будут неэффективны.\r\n\r\n#задание #конфликт #техника\r\n\r\nНапиши мне решение, которое будешь исполнять, и жми \"готово\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово",
    "trigger": "",
    "linked_to": "perfect",
    "linked_to_id": "415"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "conflict2",
    "linked_to_id": "419"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "423",
  "textid": "didrest",
  "name": "Сделал отдых",
  "message": "Очень хорошо! \r\n\r\nОтдыхать полезно всем. Даже боту! Раньше основная нагрузка на человеков была связана с физическими усилиями. Когда важнее стали когнитивные возможности, то оказалось, что этот ресурс ограничен и им стоит пользоваться бережно. Твой навык осознанности немного вырос, что позволит тебе эффективнее распоряжаться своим интеллектуальным богатством.\r\n\r\nВ будущем, когнитивные стимуляторы и нейрогаджеты заберут подобного рода сложности на себя, но навык осознанности к своим ментальным состояниям и бережного отношения к себе ещё долго будет полезен.\r\n\r\nСоветуем установить привычку к отдыху.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Установить!",
    "trigger": "",
    "linked_to": "habit",
    "linked_to_id": "402"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "427",
  "textid": "reminder",
  "name": "Напоминание",
  "message": "Пинг! Как успехи?",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "http:\/\/memesmix.net\/media\/created\/ij6f87.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "http:\/\/www.netlore.ru\/upload\/files\/7\/p17i91lt52u0vjts1b7p1clcp7j1.jpg",
    "format": "image"
  }, {
    "type": "url",
    "id": "https:\/\/cdn.fishki.net\/upload\/post\/2017\/06\/12\/2312953\/naval-1.jpg",
    "format": "image"
  }],
  "send_random_attachment": true,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "428",
  "textid": "middle",
  "name": "Анкета обратной связи в середине игры",
  "message": "https:\/\/forms.gle\/EFaoczYzvXfE4RWV7\r\n\r\nА также, если у тебя уже есть понимание, что это понадобится, ссылка на продолжение zaresh.ai\/snovazareshat",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "429",
  "textid": "everydayhello",
  "name": "Что вчера и что сегодня?",
  "message": "Привет! Что получилось сделать по плану вчера и что планируешь делать сегодня?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "435",
  "textid": "snova",
  "name": "Снова зарешать",
  "message": "Отлично, ты играешь!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Получить задание",
    "trigger": "",
    "linked_to": "nextGoal",
    "linked_to_id": "436"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "436",
  "textid": "nextGoal",
  "name": "Следующая цель",
  "message": "Мы тебе рады, заходи, будь как дома. Что будешь делать в ближайший месяц? \r\nТебя ведь уже не нужно учить формулировать цель? ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Проверить цель",
    "trigger": "",
    "linked_to": "checkGoal",
    "linked_to_id": "270"
  }, {
    "on_click": [],
    "text": "Все нормально с целью!",
    "trigger": "",
    "linked_to": "BasePlan",
    "linked_to_id": "107"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "438",
  "textid": "help",
  "name": "Хелп",
  "message": "<b>Со мной говорят люди или роботы?<\/b>\r\nКогда есть кнопки и сообщения мгновенны - бот, в ином случае люди.\r\n\r\n<b>Есть ли дедлайны для заданий?<\/b>\r\nЕсли есть тег #задание, то скорее всего нужно выполнить его за сутки. Другое указывается явно в тексте задания.  \r\n\r\n<b>Как начать новое задание?<\/b>\r\nСдать предыдущее, подождать проверки, после чего новое придёт само\r\n\r\n<b>Можно ли пройти задание снова?<\/b>\r\nМожно. Если нет кнопки \"назад\", то пиши боту и мы перезапустим.\r\n\r\n<b>Можно ли писать без команды \"сдать\"?<\/b>\r\nГде такая команда есть - нужно её использовать, но все твои сообщения дойдут до нас в любом случае\r\n\r\n<b>Можно ли редактировать сообщения? Присылать несколько?<\/b>\r\nМожно\r\n\r\n<b>Я застряла на одном из заданий, что делать?<\/b>\r\nНаписать об этом в свой чат, мы придём на помощь\r\n\r\n<b>Что можно делать в общем чате?<\/b>\r\n-Рассказать о себе, добавив #интро\r\n-Задавать вопросы\r\n-Помогать другим разобраться в их вопросах\r\n-Создавать команды для совместной работы\r\n-Делиться успехами\r\n\r\n<b>Могу ли я переформулировать\/изменить свою цель?<\/b>\r\nМожно, но не позже 10 дней с начала игры\r\n\r\n<b>Пропали кнопки, что делать, мне страшно?<\/b>\r\n\r\nПосмотри, не свёрнуты ли кнопки в интерфейсе ТГ (значок с кнопками справа от поля для ввода текста, там же, где смайлы и самолетик). Если нет, и тебе все еще страшно, пиши боту :) \r\n\r\nЕще ты можешь нажимать на кнопку \"подбодрить\" каждый раз, когда чувствуешь провал в мотивации.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Подбодрить!",
    "trigger": "",
    "linked_to": "SweetMotivation",
    "linked_to_id": "115"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "start",
    "linked_to_id": "1"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "445",
  "textid": "14september19",
  "name": "Старт 14 сентября",
  "message": "Отлично, я тебя вижу. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я играю в первый раз",
    "trigger": "",
    "linked_to": "problem",
    "linked_to_id": "105"
  }, {
    "on_click": [],
    "text": "Я уже играл(а)",
    "trigger": "",
    "linked_to": "nextGoal",
    "linked_to_id": "436"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "446",
  "textid": "goOff",
  "name": "Уходи",
  "message": "",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "next",
    "linked_to": "goOff",
    "linked_to_id": "446"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "506",
  "textid": "SkillboxStartToUpload",
  "name": "Вводное сообщение",
  "message": "Привет! Часть записавшихся на курс так и не приступили непосредственно к обучению. Обучение, а тем более, самообучение – одна из самых когнитивно-сложных задач, поэтому такая ситуация не очень удивительна. Например, случаи тяжелой, хронической прокрастинации встречаются среди студентов в 50% случаев, а среди просто взрослых – всего лишь в 20%. \r\nК счастью, существует несколько эффективных инструментов для того, чтобы преодолеть прокрастинацию и всё-таки погрузиться в процесс.  Что можно сделать? \r\n* Уточнить цель и мотивацию. \r\n* Составить детальный план действий.\r\n* Поработать с избеганиями.\r\nКак ты думаешь, что стоит попробовать в начале?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Цель",
    "trigger": "",
    "linked_to": "SkillboxGoalStart",
    "linked_to_id": "507"
  }, {
    "on_click": [],
    "text": "План",
    "trigger": "",
    "linked_to": "SkillboxPlanStart",
    "linked_to_id": "509"
  }, {
    "on_click": [],
    "text": "Избегания",
    "trigger": "",
    "linked_to": "SkillboxAversionStart",
    "linked_to_id": "510"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "507",
  "textid": "SkillboxGoalStart",
  "name": "Работа с целью",
  "message": "Привет! Ты начинаешь учиться чему-то новому, и это здорово!\r\n\r\nОбучение бывает более эффективным, если мы хорошо понимаем, зачем оно нам и какие важные задачи позволяет решить. Поэтому давай ответим на несколько вопросов, которые помогут тебе удерживать стрелку компаса в нужном направлении на протяжении всего пути.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Давай!",
    "trigger": "",
    "linked_to": "SkillboxGoal01",
    "linked_to_id": "517"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxStartToUpload",
    "linked_to_id": "506"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "509",
  "textid": "SkillboxPlanStart",
  "name": "Работа с планом",
  "message": "Ты приступаешь к обучению, и будет полезным составить план. Создание плана помогает заранее осознать нехватку ресурсов и риски непредвиденных обстоятельств. Помимо учебы, в твоей жизни наверняка есть много других активностей, планирование поможет тебе совместить обучение с остальными частями жизни. \r\n\r\nЕсть несколько полезностей, которые стоит учесть при выполнении любого дела, чтобы не выгореть по дороге и дойти до финиша живым и довольным человеком.\r\n\r\nДавай обсудим каждый из них по очереди",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Начнём!",
    "trigger": "",
    "linked_to": "SkillboxPlan01",
    "linked_to_id": "555"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxStartToUpload",
    "linked_to_id": "506"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "510",
  "textid": "SkillboxAversionStart",
  "name": "Работа с избеганиями",
  "message": "Если тебе вроде хочется заниматься обучением, но каждый раз что-то останавливает, то, возможно, на тебя напало Избегание? \r\n\r\nМожно поговорить с этим умным ботом, чтобы справиться с ним. Или напиши помощнику по обучению @GalinaTurchak, она поддержит.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": " Бот, помоги!",
    "trigger": "",
    "linked_to": "SkillboxAversion01",
    "linked_to_id": "539"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxStartToUpload",
    "linked_to_id": "506"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "517",
  "textid": "SkillboxGoal01",
  "name": "Работа с целью. Первый вопрос",
  "message": "Итак, расскажи мне: что ты будешь считать хорошим результатом обучение?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий вопрос",
    "trigger": "",
    "linked_to": "SkillboxGoal02",
    "linked_to_id": "522"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxGoalStart",
    "linked_to_id": "507"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "522",
  "textid": "SkillboxGoal02",
  "name": "Работа с целью. Второй вопрос",
  "message": "Что изменится в тебе и в твоей жизни, когда ты закончишь обучение? Как именно ты сможешь использовать результаты обучения?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий вопрос",
    "trigger": "",
    "linked_to": "SkillboxGoal03",
    "linked_to_id": "525"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxGoal01",
    "linked_to_id": "517"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "525",
  "textid": "SkillboxGoal03",
  "name": "Работа с целью. Третий вопрос",
  "message": "Как ты увидишь, что изменения начали происходить? Как ты поменяешься со стороны? Что заметят окружающие?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий вопрос",
    "trigger": "",
    "linked_to": "SkillboxGoal04",
    "linked_to_id": "530"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxGoal02",
    "linked_to_id": "522"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "530",
  "textid": "SkillboxGoal04",
  "name": "Работа с целью. Четвертый вопрос",
  "message": "С какими твоими жизненными целями связана идея обучения? Почему ты решил_а записаться на курс?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий вопрос",
    "trigger": "",
    "linked_to": "SkillboxGoal05",
    "linked_to_id": "532"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxGoal03",
    "linked_to_id": "525"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "532",
  "textid": "SkillboxGoal05",
  "name": "Работа с целью. Пятый вопрос",
  "message": "Как ты себя поддержишь во время учебы? Как себя похвалишь? Есть ли кто рядом, кто тобой гордится и чувствует как для тебя это важно?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Завершить упражнение",
    "trigger": "",
    "linked_to": "SkillboxGoalFinish",
    "linked_to_id": "534"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxGoal04",
    "linked_to_id": "530"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "534",
  "textid": "SkillboxGoalFinish",
  "name": "Работа с целью. Завершение",
  "message": "Сохрани эти ответы и держи их перед глазами до конца курса.\r\n\r\nКак ты ощущаешь себя сейчас? Помогло ли упражнение? \r\n\r\nМожет быть, хочешь попробовать планирование или работу с избеганиями?\r\n\r\nТы так же можешь написать помощнику по обучению @GalinaTurchak с любым запросом",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "План",
    "trigger": "",
    "linked_to": "SkillboxPlanStart",
    "linked_to_id": "509"
  }, {
    "on_click": [],
    "text": "Избегания",
    "trigger": "",
    "linked_to": "SkillboxAversionStart",
    "linked_to_id": "510"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxGoal05",
    "linked_to_id": "532"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "539",
  "textid": "SkillboxAversion01",
  "name": "Работа с избеганиями. Первое задание",
  "message": "Запиши все причины, почему тебе стоит обучаться.\r\nЧтобы помочь себе вспомнить причины:\r\n* Представь конечный результат\r\n* Попробуй почувствовать свои эмоции после защиты диплома\r\n\r\nОтправь это мне.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "SkillboxAversion02",
    "linked_to_id": "540"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxAversionStart",
    "linked_to_id": "510"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "540",
  "textid": "SkillboxAversion02",
  "name": "Работа с избеганиями. Второе задание",
  "message": "Теперь запиши, почему ты не хочешь обучаться.\r\n\r\nПодсказки:\r\n* Что-то кажется тебе страшным, неприятным? \r\n* Что-то вызывает конкретные плохие ощущения?\r\n* Попробуй представить, как ты приступаешь — что ты чувствуешь, что тебя останавливает, что не так?\r\n\r\nОтправь это мне.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "SkillboxAversion03",
    "linked_to_id": "542"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxAversion01",
    "linked_to_id": "539"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "542",
  "textid": "SkillboxAversion03",
  "name": "Работа с избеганиями. Третье задание",
  "message": "Теперь — сюрприз — мы не будем их сравнивать, мы просто избавимся от минусов! Давай обработаем каждый пункт из минусов отдельно. \r\n* Он связан с внешними обстоятельствами (шумная комната, нет времени в расписании) или с внутренними (ощущение усталости, стыд что до сих пор не сделал, какой-то страх или тревога?)",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "SkillboxAversion04",
    "linked_to_id": "545"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxAversion02",
    "linked_to_id": "540"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "545",
  "textid": "SkillboxAversion04",
  "name": "Работа с избеганиями. Четвертое задание",
  "message": "Придумай, как от этого избавиться? Запланируй время в дне, когда ты будешь заниматься. Составь понятный план и организуй сигнал, по которому ты будешь начинать выполнение плана.\r\nКто может тебя поддержать?\r\n\r\nВозможно, тебе стоит запланировать отдых отдельно? \r\n\r\nНа все сложные вопросы поможет ответить наставник по обучению @GalinaTurchak, и, конечно, мы тебе всегда рады!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Завершить упражнение",
    "trigger": "",
    "linked_to": "SkillboxAversionFinish",
    "linked_to_id": "549"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxAversion03",
    "linked_to_id": "542"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "549",
  "textid": "SkillboxAversionFinish",
  "name": "Работа с избеганиями. Завершение",
  "message": "После упражнения попробуй сесть за занятия. Лучше? \r\n\r\nЕсли недостаточно, то можно попробовать целеполагание или планирование?\r\n\r\nТы так же можешь написать помощнику по обучению @GalinaTurchak с любым запросом",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Цель",
    "trigger": "",
    "linked_to": "SkillboxGoalStart",
    "linked_to_id": "507"
  }, {
    "on_click": [],
    "text": "План",
    "trigger": "",
    "linked_to": "SkillboxPlanStart",
    "linked_to_id": "509"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxAversion04",
    "linked_to_id": "545"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "555",
  "textid": "SkillboxPlan01",
  "name": "Работа с планом. Первая полезность",
  "message": "Внутри любого плана есть главные задачи и второстепенные. \r\n\r\nМожешь привести примеры главных и второстепенных задач для твоего плана?\r\n\r\nМы частенько кидаемся решать не очень главные, но понятные задачи, откладывая сложные, но важные. Чекай себя время от времени на этот предмет.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan02",
    "linked_to_id": "556"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlanStart",
    "linked_to_id": "509"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "556",
  "textid": "SkillboxPlan02",
  "name": "Работа с планом. Вторая полезность",
  "message": "Определи свои сильные стороны и опирайся на них. То, в чем ты пока недостаточно компетентен(-на), попробуй делегировать или хотя бы привлекай помощь - это нормально и продвинет тебя гораздо больше, чем стремление \"через не могу\" зарешать все самостоятельно.\r\n\r\nПоявились идеи, что из твоих сильных черт могло бы пригодится? Напиши мне.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan03",
    "linked_to_id": "557"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan01",
    "linked_to_id": "555"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "557",
  "textid": "SkillboxPlan03",
  "name": "Работа с планом. Третья полезность",
  "message": "Планируй выполнение сложных частей плана на пиковые периоды продуктивности. \r\n\r\nНапиши, какие задачи кажутся тебе самыми сложными?\r\n\r\nВ какие периоды времени ты наиболее эффективно работаешь?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan04",
    "linked_to_id": "558"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan02",
    "linked_to_id": "556"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "558",
  "textid": "SkillboxPlan04",
  "name": "Работа с планом. Четвертая полезность",
  "message": "По возможности организуй комфортную среду для работы - чтобы никто и ничто не отвлекало тебя.\r\n\r\nЧто для тебя является важным условием комфортной работы? \r\n\r\n\/\/Если захочешь более подробно разобраться с этим вопросом, то я могу рассказать тебе про алгоритм создания чудесной таблицы",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan05",
    "linked_to_id": "564"
  }, {
    "on_click": [],
    "text": "Чудо-таблица",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort01",
    "linked_to_id": "559"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan03",
    "linked_to_id": "557"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "559",
  "textid": "SkillboxPlan04Comfort01",
  "name": "бота с планом. Четвертая полезность. Первая подробность",
  "message": "Итак, подумай и выпиши все важные условия, которые тебе необходимы, чтобы обучение было комфортным и эффективным. \r\nНапример, это может быть определенная обстановка (домашняя или рабочая), время дня (утро-день-вечер), помощь других людей (кому-то легче учиться за компанию, и тогда можно организовать коворкинг, а кому-то - в одиночку) и пр.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort02",
    "linked_to_id": "560"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan04",
    "linked_to_id": "558"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "560",
  "textid": "SkillboxPlan04Comfort02",
  "name": "Четвертая полезность. Вторая подробность",
  "message": "Теперь давай сделаем таблицу: строчки это дни недели, столбики это условия, которые ты написал(а) раньше.\r\n\r\nГотово?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort03",
    "linked_to_id": "561"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort01",
    "linked_to_id": "559"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "561",
  "textid": "SkillboxPlan04Comfort03",
  "name": "Четвертая полезность. Третья подробность",
  "message": "Назначь каждому условию вес от 1 до 10 (напиши рядом с названием условия) - то есть если ты считаешь это условие суперважным, ставь 10, если таким, от которого можно и отказаться - ставь 1.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort04",
    "linked_to_id": "562"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort02",
    "linked_to_id": "560"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "562",
  "textid": "SkillboxPlan04Comfort04",
  "name": "Четвертая полезность. Четвертая подробность",
  "message": "Теперь оцени каждый день по каждому условию и умножь на вес критерия.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort05",
    "linked_to_id": "563"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort03",
    "linked_to_id": "561"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "563",
  "textid": "SkillboxPlan04Comfort05",
  "name": "Четвертая полезность. Пятая подробность",
  "message": "Вуаля, у тебя табличка с выбором. Считай, сколько в сумме у каждого дня и выбирай оптимальный, когда ты можешь заниматься в самых подходящих условиях.\r\n\r\nКакой день у тебя получился?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan05",
    "linked_to_id": "564"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan04Comfort04",
    "linked_to_id": "562"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "564",
  "textid": "SkillboxPlan05",
  "name": "Работа с планом. Пятая полезность",
  "message": "Так же тщательно, как и работу, спланируй отдых и время на восстановление. Способы выбери те, которые подходят и приятны лично тебе - прогулки, чил на газоне в парке, физическая активность, общение с приятными людьми, медитация, массаж, прослушивание музыки и пр.\r\n\r\nЧто помогает тебе отдохнуть и восстановиться?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan06",
    "linked_to_id": "565"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan04",
    "linked_to_id": "558"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "565",
  "textid": "SkillboxPlan06",
  "name": "Работа с планом. Шестая полезность",
  "message": "Развивай свою способность реалистично оценивать возможности. Раз в неделю оглядывайся назад и подводи итог - сколько было запланировано и сколько сделано. \r\n\r\nЧерез пару месяцев у тебя накопится достаточно материала, чтобы понять реальные объемы работы, которые ты успеваешь выполнить в единицу времени. Скорректируй свои планы с учетом этого, не старайся выкроить двенадцать шапочек из одной шкурки :) Не бери на себя больше, чем можешь, без риска эмоционального и физического истощения.\r\n\r\nВ среднем, считается разумным умножать сроки первоначального плана на 2,5-3, чтобы получить оценки близкие к реальным. \r\nНасколько ты оцениваешь свою оптимистичность?\r\n\r\nТеперь мы можем перейти к завершающему шагу или поработать с рисками. Выбирай.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Завершить упражнение",
    "trigger": "",
    "linked_to": "SkillboxPlanFinish",
    "linked_to_id": "569"
  }, {
    "on_click": [],
    "text": "Риски",
    "trigger": "",
    "linked_to": "SkillboxPlan06Premortem01",
    "linked_to_id": "566"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan05",
    "linked_to_id": "564"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "566",
  "textid": "SkillboxPlan06Premortem01",
  "name": "Работа с планом. Премортем. Первый вопрос",
  "message": "Представь, что ты начал_а следовать своему расписанию, и из будущего пришла весточка: у тебя ничего не вышло.\r\n\r\nНасколько сильно тебя удивит такая новость?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan06Premortem02",
    "linked_to_id": "567"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan06",
    "linked_to_id": "565"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "567",
  "textid": "SkillboxPlan06Premortem02",
  "name": "Работа с планом. Премортем. Второй вопрос",
  "message": "Запиши все истории почему ничего не вышло. Если надо — спроси друзей. Они всегда готовы рассказать, что пойдет не так :)\r\n\r\nКакие риски удалось заметить?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующий шаг",
    "trigger": "",
    "linked_to": "SkillboxPlan06Premortem03",
    "linked_to_id": "568"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan06Premortem01",
    "linked_to_id": "566"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "568",
  "textid": "SkillboxPlan06Premortem03",
  "name": "Работа с планом. Премортем. Третий вопрос",
  "message": "Возьми наиболее вероятные и угрожающие сценарии и придумай, что можно сделать, чтобы в них не попасть или минимизировать риск.\r\n\r\nНапиши, что изменилось в твоём плане?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Завершить упражнение",
    "trigger": "",
    "linked_to": "SkillboxPlanFinish",
    "linked_to_id": "569"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan06Premortem02",
    "linked_to_id": "567"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "569",
  "textid": "SkillboxPlanFinish",
  "name": "Работа с планом. Завершение",
  "message": "Расскажи финальный план, который получился (или напиши, если ничего не получилось)\r\n\r\nЧувствуешь ли ты большую готовность приступить к обучению? Если недостаточно, то можно попробовать целеполагание или работу с избеганиями?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Цель",
    "trigger": "",
    "linked_to": "SkillboxGoalStart",
    "linked_to_id": "507"
  }, {
    "on_click": [],
    "text": "Избегания",
    "trigger": "",
    "linked_to": "SkillboxAversionStart",
    "linked_to_id": "510"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "SkillboxPlan06",
    "linked_to_id": "565"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "583",
  "textid": "reminderYana",
  "name": "Reminder Yana",
  "message": "На сколько ты поставишь будильник про текст завтра?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "632",
  "textid": "promo1",
  "name": "Промо1",
  "message": "Привет!\r\nЯ Зарешай-бот, и я помогаю разбираться со сложностью жизни. Люди приходят ко мне, чтобы решить проблемы.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А какие проблемы можно решать?",
    "trigger": "",
    "linked_to": "promo2",
    "linked_to_id": "633"
  }, {
    "on_click": [],
    "text": "Что такое сложность жизни?",
    "trigger": "",
    "linked_to": "promo2complexity",
    "linked_to_id": "664"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "633",
  "textid": "promo2",
  "name": "А какие проблемы можно решать? ",
  "message": "Как правило, такие, с которыми сталкивается большинство людей - определиться, что делать в жизни, куда двигаться дальше, начать или завершить важное дело, лучше понять свои потребности и ориентиры, научиться выстраивать отношения с людьми, стать более сфокусированным и организованным",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "И как же ты можешь в этом помочь?",
    "trigger": "",
    "linked_to": "promo2howhelp",
    "linked_to_id": "661"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo1",
    "linked_to_id": "632"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "635",
  "textid": "zareshatExpert",
  "name": "ЗарешатьЭксперт",
  "message": "<b>Что включено:<\/b>\r\n\r\nПрофессиональный человек не реже двух раз в день\r\nВсе задания от бота\r\nГрупповой чат, активности в нем\r\n\r\n<b>С чем заходить:<\/b>\r\nпринять сложное решение\r\nпобороть непонятную прокрастинацию\r\nразрешить внутренний конфликт\r\nпонять, куда двигаться по жизни\r\nнаучиться ставить цели\r\nстать более организованным\r\nпрокачать коммуникативные навыки\r\nнаучиться отстаивать свои границы\r\nстать более стрессоустойчивым\r\n\r\n<b>Билет за 5000 руб.<\/b> (цена повысится до 7000  <b>7 октября<\/b>) t.me\/ZareshaiBot?start=zareshat_expert\r\n\r\nПереходи с телефона\r\n\r\nЕсли при нажатии ничего не происходит, обнови телеграм или просто переходи за билетом на сайт https:\/\/zaresh.ai\/#tickets\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "636",
  "textid": "zareshatBot",
  "name": "ЗарешатьБот",
  "message": "<b>Что включено:<\/b>\r\nНежные ботячьи пиночки, эксперт следит, чтобы вы шли по правильной цепочке заданий\r\nВсе задания от бота\r\nГрупповой чат, активности в нем\r\n\r\n<b>С чем заходить:<\/b>\r\nпринять внешнее решение: выбрать покупку, решить, продавать ли квартиру, выбрать страну для переезда\r\nустановить привычку (спорт, сон, утренние и вечерние дела)\r\nучебный или рабочий проект, которые надо брать и делать\r\nнаучиться отдыхать\r\nвыучить язык\r\nпобороть прокрастинацию, если она случается от отсутствия плана\r\nразгрузить голову и организовать дела\r\n\r\n<b>Билет за 3000 руб.<\/b> t.me\/ZareshaiBot?start=zareshat_bot\r\n\r\nЕсли при нажатии ничего не происходит, обнови телеграм или просто переходи за билетом на сайт https:\/\/zaresh.ai\/#tickets\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "637",
  "textid": "zareshatVIP",
  "name": "Зарешать Вип",
  "message": "<b>Что включено:<\/b>\r\nбилет для тебя на два месяца\r\nдополнительный билет для Золушки — мир будет лучше :)\r\n\r\n<b>Купить за 8000 руб.<\/b> t.me\/ZareshaiBot?start=zareshat_VIP\r\n\r\nПереходи с телефона\r\n\r\nЕсли при нажатии ничего не происходит, обнови телеграм или просто переходи за билетом на сайт https:\/\/zaresh.ai\/#tickets\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/котик теплота.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "648",
  "textid": "promofeedback",
  "name": "ПромоОтзывы",
  "message": "<b>Виктор Казаков (играет в Зарешай второй раз подряд): <\/b>\r\n\r\nПоначалу мне казалось, что очень-очень долго все запрягается, возможно, это из-за большого количества неопределённости, с которой я пришел, но потом все превзошло все мои ожидания. 10 зарешаев из 10!\r\n\r\n<b>Клавдия Китова: <\/b>\r\n\r\nБольше всего мне понравилась постановка целей, последовательные задания от чат-бота, сама идея онлайн-коворкинга — и вообще ощущение тщательно построенной специально для меня зоны ближайшего развития.\r\n\r\n<b>Настасья:<\/b>\r\n\r\nПолезный инструмент для освещения своих вопросов и неудовлетворенностей, который дает несколько вариантов путей для решения ситуации. Понимаешь, что именно мешает тебе достичь твоей цели\/желания, благодаря наводящим вопросам, обсуждению. Например, я не думала, что моя расфокусировка может быть целью на ближайшие полгода. Сейчас есть более горящий сарай, но её я не замечала очень долго. Вылезла практически спонтанно, чем меня очень порадовала, т.к. я редко сводила по ней факты в своей голове, мол, вроде бы мелочь, потом как-нибудь. А оказалось нет. Хотела бы другой поток пройти с этой темой.\r\n\r\nБольше отзывов на сайте\r\nhttps:\/\/zaresh.ai",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "649",
  "textid": "promoteam",
  "name": "ПромоЭксперты",
  "message": "Вот кто щекочет меня с другой стороны (эксперты, которые тебя консультируют): \r\n\r\n<b>Пион Медведева<\/b> @pionmedvedeva @ontologics\r\nУмеет решать проблемы с помощью рациональности. Умеет точно формулировать и понятно объяснять, а также слать подбадривающие картиночки. Область экспертизы: прикладная рациональность, онтологика, принятие решений.\r\n\r\n<b>Наталья Кисельникова<\/b> @natalykis\r\nЗнает, как оказать поддержку и найти индивидуальный подход к каждому игроку. Область экспертизы: психология, психотерапия, решение комплексных проблем\r\n\r\n<b>Илья Вознесенский<\/b> затащит прикладную рациональность.\r\n\r\n<b>Илья Сойфер<\/b>\r\n Помогает наладить отношения с собой, понимать свои потребности и уживаться с чувствами. Область экспертизы: ненасильственное общение, коммуникация с собой, креативный поиск стратегий.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "650",
  "textid": "promoquestion",
  "name": "Промо вопрос",
  "message": "Напиши мне свой запрос, эксперты быстро на него ответят и пояснят, нужно ли тебе в Зарешай :) \r\n\r\nИ еще вот тебе подборка постов из канала Пион https:\/\/t.me\/ontologics\/251",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }, {
    "on_click": [],
    "text": "Билет!",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "656",
  "textid": "spasibobot",
  "name": "Спасибо билет бот",
  "message": "Ты играешь со мной! В день старта я кину тебе ссылку на чат и первое задание. До встречи!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "657",
  "textid": "spasiboexpert",
  "name": "Спасибо билет эксперт",
  "message": "Спасибо! Ты играешь со мной и экспертом. В день старта я скину тебе ссылку на чат и первое задание. До встречи!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "658",
  "textid": "spasibotebe",
  "name": "Спасибо билет вип",
  "message": "Спасибо! Мы возьмем еще Золушку. Тебя добавим в чат в день старта и скинем первое задание. До встречи!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "661",
  "textid": "promo2howhelp",
  "name": "И как же ты можешь в этом помочь?",
  "message": "В создании меня принимали участие психологи, коучи, рационалисты и даже футуролог. Поэтому я знаю и умею <i>реально<\/i> много всего классного.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Что именно ты умеешь?",
    "trigger": "",
    "linked_to": "promo2skills",
    "linked_to_id": "662"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2",
    "linked_to_id": "633"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "662",
  "textid": "promo2skills",
  "name": "Что именно ты умеешь?",
  "message": "Я умею вести диалог, понимать, как устроена твоя проблема и подбирать оптимальные инструменты для ее решения. В моем арсенале техники прикладной рациональности для принятия решений в условиях неопределенности, совершения выбора, повышения осознанности; инструменты проектного управления - для планирования и выполнения задуманного; психологические знания и практики - для помощи в самоопределении, лучшем понимании себя и других.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А это точно работает?",
    "trigger": "",
    "linked_to": "promo2guarantee",
    "linked_to_id": "663"
  }, {
    "on_click": [],
    "text": "Как выглядит процесс?",
    "trigger": "",
    "linked_to": "promo2process",
    "linked_to_id": "686"
  }, {
    "on_click": [],
    "text": "Хочу билет",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2howhelp",
    "linked_to_id": "661"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "663",
  "textid": "promo2guarantee",
  "name": "А это точно работает?",
  "message": "Я могу гарантировать обоснованность и эффективность предлагаемых инструментов. Это лучшее, что есть на сегодняшний день в человеческом опыте. Но даже самый надежный инструмент не гарантирует успеха на 100%. В помогающих практиках существует термин \"разделенная ответственность\". Это значит, что решение проблемы зависит от инструмента, контекста и тебя самого.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Какие ваши доказательства?",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2skills",
    "linked_to_id": "662"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "664",
  "textid": "promo2complexity",
  "name": "Что такое сложность жизни?",
  "message": "Если очень просто, то тебе нужно принимать много важных решений, а времени не хватает, данных недостаточно и одновременно с тобой другие люди тоже что-то решают. А, кстати, не принимать решение - это всё равно принимать. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "И как ты помогаешь с этим справиться?",
    "trigger": "",
    "linked_to": "promo2howcope",
    "linked_to_id": "665"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo1",
    "linked_to_id": "632"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/cs5.pikabu.ru\/images\/previews_comm\/2015-10_5\/1445505175144935170.jpg ",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "665",
  "textid": "promo2howcope",
  "name": "И как же ты помогаешь с этим справиться?",
  "message": "Я умею \"раскладывать\" сложность на простые понятные задачи. Миллионы людей до тебя сталкивались с такими же задачами, которые стоят перед тобой. Некоторые из этих людей находили лучшие инструменты для их решения. Мои создатели использовали опыт этих поколений и взяли лучшие практики и научно-обоснованные подходы. Так что во мне нет никакой магии, только экспертность и алгоритмы решения проблем.\r\nМы с тобой будем вести диалог, и я смогу предложить те инструменты, которые подойдут именно тебе и именно сейчас.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "И как это решает проблему сложности жизни?",
    "trigger": "",
    "linked_to": "promo2simplicity",
    "linked_to_id": "666"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2complexity",
    "linked_to_id": "664"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "666",
  "textid": "promo2simplicity",
  "name": "И как это решает проблему сложности жизни?",
  "message": "Я знаю, из каких частей состоит любая проблема, и этим убираю запутанность картинки. Ты сможешь выделить главное и не тратить время на второстепенное. Я умею наводить фокус с трудностей на цель и ресурсы. Ты научишься прогнозировать развитие проблемы и оценивать влияние других людей на нее. Я буду поддерживать тебя, дам инструменты самоорганизации и помогу не сходить с пути.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Хочу примеров!",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }, {
    "on_click": [],
    "text": "Как выглядит путь?",
    "trigger": "",
    "linked_to": "promo2process",
    "linked_to_id": "686"
  }, {
    "on_click": [],
    "text": "Хочу билет!",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2howcope",
    "linked_to_id": "665"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "674",
  "textid": "test",
  "name": "Test",
  "message": "test",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "test",
      "value": 1
    }, {
      "type": "update_user_progress",
      "target": "test2",
      "value": -1
    }, {
      "type": "add_user_group",
      "target": "testqq"
    }, {
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "test answer"
    }],
    "text": "q",
    "trigger": "",
    "linked_to": "start",
    "linked_to_id": "1"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "686",
  "textid": "promo2process",
  "name": "Промо2процесс",
  "message": "Примерно так и выглядит",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад к тому что ты умеешь",
    "trigger": "",
    "linked_to": "promo2skills",
    "linked_to_id": "662"
  }, {
    "on_click": [],
    "text": "Назад к сложности",
    "trigger": "",
    "linked_to": "promo2complexity",
    "linked_to_id": "664"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/2019-10-04 01.00.53.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "692",
  "textid": "promo2proof",
  "name": "Какие ваши док-ва",
  "message": "У меня есть примеры историй наших игроков, отзывы после участия и описания экспертов. Выбирай!\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Кейсы",
    "trigger": "",
    "linked_to": "promo2cases",
    "linked_to_id": "695"
  }, {
    "on_click": [],
    "text": "Отзывы",
    "trigger": "",
    "linked_to": "promofeedback",
    "linked_to_id": "648"
  }, {
    "on_click": [],
    "text": "Эксперты",
    "trigger": "",
    "linked_to": "promoteam",
    "linked_to_id": "649"
  }, {
    "on_click": [],
    "text": "У меня уникальный случай!",
    "trigger": "",
    "linked_to": "promoquestion",
    "linked_to_id": "650"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }, {
    "on_click": [],
    "text": "В начало",
    "trigger": "",
    "linked_to": "promo1",
    "linked_to_id": "632"
  }, {
    "on_click": [],
    "text": "Давайте билеты",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/opAAAgA_muA-1920-e1562270347950.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "695",
  "textid": "promo2cases",
  "name": "Кейсы",
  "message": "https:\/\/vk.com\/@zareshai-uchastniki-proshlyh-zapuskov?ref=group_block",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "698",
  "textid": "promo2tickets",
  "name": "Купить билет",
  "message": "У нас есть три типа билетов:\r\n\r\nЗа 3к - мы с тобой будем общаться месяц.\r\nЗа 7к (в телеграме за 5к!) - мы будем общаться месяц с тобой и экспертом\r\nЗа 8к  - билет Крестной (с экспертом и добавить бесплатный билет для Золушки)\r\n\r\nБилеты доступны по кнопкам ниже или, если ты с компьютера, то https:\/\/zaresh.ai\/#tickets",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Билет за 3к",
    "trigger": "",
    "linked_to": "zareshatBot",
    "linked_to_id": "636"
  }, {
    "on_click": [],
    "text": "Билет за 7к",
    "trigger": "",
    "linked_to": "zareshatExpert",
    "linked_to_id": "635"
  }, {
    "on_click": [],
    "text": "Билет Крестной",
    "trigger": "",
    "linked_to": "zareshatVIP",
    "linked_to_id": "637"
  }, {
    "on_click": [],
    "text": "Я Золушка, у меня нет денег, но есть принципы ",
    "trigger": "",
    "linked_to": "cinderella",
    "linked_to_id": "702"
  }, {
    "on_click": [],
    "text": "Еще доказательства",
    "trigger": "",
    "linked_to": "promo2proof",
    "linked_to_id": "692"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "702",
  "textid": "cinderella",
  "name": "Золушка",
  "message": "Напиши мне, почему нам стоит взять тебя в игру, и жди Крестную!\r\n\r\nПосле окончания игры мы просим всех Золушек сделать пост в своих социальных сетях. В посте описать свой опыт участия и что получилось сделать с помощью бота.\r\n\r\n1. Какие у тебя цели в жизни? \r\n2. Почему нам нужно взять в игру именно тебя?\r\n3. Как ты будешь применять навыки, полученные в игре?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "promo2tickets",
    "linked_to_id": "698"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "739",
  "textid": "promo11",
  "name": "Промо для loony",
  "message": "Привет!\r\nЯ Зарешай-бот, и я помогаю разбираться со сложностью жизни. Люди приходят ко мне, чтобы решить проблемы.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А какие проблемы можно решать?",
    "trigger": "",
    "linked_to": "promo2",
    "linked_to_id": "633"
  }, {
    "on_click": [],
    "text": "Что такое сложность жизни?",
    "trigger": "",
    "linked_to": "promo2complexity",
    "linked_to_id": "664"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "743",
  "textid": "promo12",
  "name": "Промо12",
  "message": "Привет!\r\nЯ Зарешай-бот, и я помогаю разбираться со сложностью жизни. Люди приходят ко мне, чтобы решить проблемы.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А какие А какие проблемы можно решать? ",
    "trigger": "",
    "linked_to": "promo2",
    "linked_to_id": "633"
  }, {
    "on_click": [],
    "text": "Что такое сложность жизни?",
    "trigger": "",
    "linked_to": "promo2complexity",
    "linked_to_id": "664"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "744",
  "textid": "promo13",
  "name": "Промо13",
  "message": "Привет!\r\nЯ Зарешай-бот, и я помогаю разбираться со сложностью жизни. Люди приходят ко мне, чтобы решить проблемы.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А какие проблемы можно решать?",
    "trigger": "",
    "linked_to": "promo2",
    "linked_to_id": "633"
  }, {
    "on_click": [],
    "text": "Что такое сложность жизни?",
    "trigger": "",
    "linked_to": "promo2complexity",
    "linked_to_id": "664"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "752",
  "textid": "ButtonsOnly",
  "name": "Только кнопки",
  "message": "Человек, общайся со мной с помощью кнопок, пожалуйста!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "772",
  "textid": "stop",
  "name": "Остановить",
  "message": "Если я тебя достал и ты не хочешь получать материалы и объявления, то нажми на меня и выбери \"остановить бота\". Но я буду ждать тебя назад.",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/e6aa44ab4a8a01869009ad9f836c3709.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "776",
  "textid": "12october19",
  "name": "Старт 12.10.19",
  "message": "Отлично, я тебя вижу!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я уже играл(а)",
    "trigger": "",
    "linked_to": "snova",
    "linked_to_id": "435"
  }, {
    "on_click": [],
    "text": "Я играю в первый раз",
    "trigger": "",
    "linked_to": "problem",
    "linked_to_id": "105"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "779",
  "textid": "problem1",
  "name": "Подсказки про проблему",
  "message": "<b>Подсказки<\/b>\r\n\r\n1. Если ты не можешь выбрать из нескольких вариантов, оставь минимальное количество и кратко опиши, почему каждая из этих проблем настолько важна. Возможно, какие-то аргументы покажутся тебе более весомыми, чем другие.\r\n2. Представь, что какая-то проблема выбрана случайным образом. Что ты при этом чувствуешь и думаешь? Есть ли осадок, ощущается что-то не то?\r\n3. Если ты считаешь, что несколько проблем действительно обладают равной важностью и срочностью для решения, выбери любую из них. Сейчас надо сосредоточиться только на одной. \r\n\r\n<b>Запиши и отправь мне: <\/b>\r\n\r\n1. Ситуация\/проблема, которая тебя беспокоит\r\n2. Почему для тебя важно, чтобы произошли изменения? Что станет лучше?\r\n3. Что ты хочешь потратить на эти изменения? (время, деньги, твои убеждения — расстаться с некоторыми из своих текущих убеждений, чтобы достичь цели\", например с убеждением \"режим сна держат только зануды\" для цели \"меньше уставать\", ощущение своей правоты и др.)\r\n4. Что ты уже предпринимал(а), чтобы решить проблему, и какие были результаты?\r\n\r\nПосле этого жми \"Следующее задание\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Следующее задание",
    "trigger": "",
    "linked_to": "goal",
    "linked_to_id": "106"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "797",
  "textid": "startmarathon",
  "name": "Старт Марафона",
  "message": "Отлично, ты в игре. К делу!\r\n\r\nМногие люди затрудняются сформулировать, какая именно проблема в их жизни должна быть решена первой. Эта стартовая цепочка заданий нужна, чтобы лучше понять себя и наметить план действий по улучшению своей жизни.\r\n\r\nЭто, согласно исследованиями, на 30% повышает вероятность того, что ты расскажешь мне позже об успешном их достижении. \r\n\r\nУ меня для тебя есть 10 заданий, одно задание в день, которые помогут тебе лучше понять свою цель. \r\n\r\nЖми на кнопку!",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "MARATHON_CHAT"
    }],
    "text": "Я в деле!",
    "trigger": "",
    "linked_to": "marathonquestionariebgn1",
    "linked_to_id": "824"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/зарешайка.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "798",
  "textid": "startmarathon1",
  "name": "Старт Марафон 1",
  "message": "Отлично! Ты в игре. \r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Начать",
    "trigger": "",
    "linked_to": "marathonquestionariebgn1",
    "linked_to_id": "824"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/Ты в игре.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "802",
  "textid": "startmarathonA",
  "name": "Старт Марафона для старых",
  "message": "Привет! \r\nЯ это я, Зарешай-бот. Как ты знаешь, я помогаю решать проблемы. Но чтобы решить какие-то проблемы в своей жизни, сначала нужно определить, какую проблему ты будешь решать в первую очередь. Я предлагаю тебе заняться этим с понедельника — у меня для тебя есть 10 заданий, одно задание в день, чтобы определиться, чем сейчас важнее всего заняться для твоего развития. \r\n\r\nЦель марафона — сделать твои цели более ясными. Это, согласно исследованиями, на 30% повышает вероятность того, что ты расскажешь мне позже об успешном их достижении. \r\n\r\nЖми на кнопку ниже, чтобы зайти в игру! Это бесплатно :) ",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "MARATHON_CHAT"
    }],
    "text": "Играть!",
    "trigger": "",
    "linked_to": "startmarathon1",
    "linked_to_id": "798"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/Зарешай-марафон.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "805",
  "textid": "startmarathonfb",
  "name": "Старт марафона ФБ",
  "message": "Привет!\r\nЯ это я, Зарешай-бот. Как ты знаешь, я помогаю решать проблемы. Но чтобы решить какие-то проблемы в своей жизни, сначала нужно определить, какую проблему ты будешь решать в первую очередь. Я предлагаю тебе заняться этим с понедельника — у меня для тебя есть 10 заданий, одно задание в день, чтобы определиться, чем сейчас важнее всего заняться для твоего развития.\r\n\r\nЦель марафона — сделать твои цели более ясными. Это, согласно исследованиями, на 30% повышает вероятность того, что ты расскажешь мне позже об успешном их достижении.\r\n\r\nЖми на кнопку ниже, чтобы зайти в игру! Это бесплатно :)",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "MARATHON_CHAT"
    }],
    "text": "Играть!",
    "trigger": "",
    "linked_to": "startmarathon1",
    "linked_to_id": "798"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/Зарешай-марафон.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "822",
  "textid": "startmarathonvk",
  "name": "Старт марафона ВК",
  "message": "Привет!\r\nЯ это я, Зарешай-бот. Как ты знаешь, я помогаю решать проблемы. Но чтобы решить какие-то проблемы в своей жизни, сначала нужно определить, какую проблему ты будешь решать в первую очередь. Я предлагаю тебе заняться этим с понедельника — у меня для тебя есть 10 заданий, одно задание в день, чтобы определиться, чем сейчас важнее всего заняться для твоего развития.\r\n\r\nЦель марафона — сделать твои цели более ясными. Это, согласно исследованиями, на 30% повышает вероятность того, что ты расскажешь мне позже об успешном их достижении.\r\n\r\nЖми на кнопку ниже, чтобы зайти в игру! Это бесплатно :)",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Играть!",
    "trigger": "",
    "linked_to": "startmarathon1",
    "linked_to_id": "798"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/Зарешай-марафон.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "824",
  "textid": "marathonquestionariebgn1",
  "name": "стартовая анкета марафона",
  "message": "Сейчас буду тебя расспрашивать. \r\n\r\nОтветы на следующие несколько вопросов помогут тебе увереннее пройти первую часть пути. Каждая минута рефлексии над ответами вернется к тебе сторицей!\r\n\r\nА еще, если хочешь общаться с другими участниками, добавляйся в чат, там будут другие начинашки (первый этап, связанный с постановкой цели), с ними можно будет поговорить. Кроме того, там будут кураторы :) t.me\/zareshaistart\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Спрашивай!",
    "trigger": "",
    "linked_to": "marathonquestionariebgn2",
    "linked_to_id": "825"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/2019-10-27 22.11.38.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "825",
  "textid": "marathonquestionariebgn2",
  "name": "стартовая анкета марафона 2",
  "message": "1\/5 Каких результатов ты ждешь от игры для себя?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "826",
  "textid": "marathonquestionariebgn3",
  "name": "стартовая анкета марафона 3",
  "message": "2\/5 Окей. А как выбираешь, что делать, а что нет?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "827",
  "textid": "marathonquestionariebgn4",
  "name": "стартовая анкета марафона 4",
  "message": "3\/5 А вообще какие основные сферы жизни ты выделяешь в своей жизни?\r\nПришли мне 5-7 штук\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "829",
  "textid": "marathonquestionariebgn5",
  "name": "стартовая анкета марафона 5",
  "message": "4\/5\r\n\r\nКруто! \r\nА что считаешь своей самой важной сильной стороной?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "830",
  "textid": "marathonquestionariebgn6",
  "name": "стартовая анкета марафона 6",
  "message": "5\/5 И для финала, как считаешь, насколько явная формулировка цели влияет на общей успех дела?\r\nОтветь цифрой 0-9, 0 — вообще не влияет, 9 — очень сильно влияет. ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "839",
  "textid": "marathonquestionariebgn7",
  "name": "Завершение стартового опроса марафон",
  "message": "Отлично. Теперь можно переходить к заданиям. Следующее задание будет приходить тебе спустя сутки после выполнения предыдущего (или сутки молчания): это рекомендуемый темп прохождения.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Давай ",
    "trigger": "",
    "linked_to": "marathontask0",
    "linked_to_id": "873"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/кнопка.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "847",
  "textid": "midgamequest1",
  "name": "Анкета в середине, вопрос1",
  "message": "Подошла середина игры, давай сверим часики! Это поможет сделать игру полезнее за оставшееся время.\r\n\r\nКак общее впечатление от процесса?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "848",
  "textid": "midgamequest2",
  "name": "Анкета в середине, вопрос2",
  "message": "Оцени полезность игры для тебя на текущий момент, от 1 до 10, 1 — совершенно бесполезна, 10 — невероятно полезна.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "850",
  "textid": "midgamequest3",
  "name": "Анкета в середине, вопрос3",
  "message": "Оцени понятность игры для тебя от 1 до 10. 1 — я вообще не понимаю, что делать и что происходит, 10 — все понятно.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "851",
  "textid": "midgamequest4",
  "name": "Анкета в середине, вопрос4",
  "message": "Что больше всего понравилось?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "852",
  "textid": "midgamequest5",
  "name": "Анкета в середине, вопрос5",
  "message": "Что меньше всего понравилось?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "853",
  "textid": "midgamequest6",
  "name": "Анкета в середине, вопрос6",
  "message": "Что, как тебе кажется, можно сейчас улучшить, чтобы сделать твой опыт участия в игре более классным?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "854",
  "textid": "midgamequest7",
  "name": "Анкета в середине, вопрос7",
  "message": "Можно, если нам понравится, публиковать твой отзыв?\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "855",
  "textid": "midgamequest8",
  "name": "Анкета в середине, вопрос8",
  "message": "Свободный микрофон — напиши любое обращение к экспертам. ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "856",
  "textid": "midgamequest9",
  "name": "Анкета в середине, вопрос9",
  "message": "Хочешь продолжить играть в следующем месяце?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "Да"
    }],
    "text": "Да!",
    "trigger": "",
    "linked_to": "midgamequest10",
    "linked_to_id": "857"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "Нет"
    }],
    "text": "Нет",
    "trigger": "",
    "linked_to": "midgamequest11",
    "linked_to_id": "866"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "857",
  "textid": "midgamequest10",
  "name": "Анкета в середине, вопрос10",
  "message": "Вот ссылка на билетик https:\/\/zaresh.ai\/snovazareshat\r\n\r\nОстальное передам экспертам. Продолжай играть :) ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "866",
  "textid": "midgamequest11",
  "name": "Анкета в середине, вопрос11",
  "message": "Принято! Продолжай играть :) ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "873",
  "textid": "marathontask0",
  "name": "Задание марафон 0",
  "message": "Наверняка ты помнишь школьные задачки про велосипедиста, который ехал из пункта А в пункт Б. Первый шаг к реализации задуманного – определение точки начала пути и его окончания. \r\n\r\nНачало (пункт А) – это то, что сейчас тебя не устраивает, что хотелось бы изменить. Окончание (пункт Б) – цель, результат, к которому ты хочешь прийти. \r\n\r\nЯ помогу тебе определить оба этих пункта и сделать это осознанно и по всем правилам эффективного решения проблем. Итак, вперед!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Вперед!",
    "trigger": "",
    "linked_to": "marathontask1",
    "linked_to_id": "874"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/велосипед.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "874",
  "textid": "marathontask1",
  "name": "Задание марафон 1",
  "message": "Люди часто думают, что точно знают, что нужно менять здесь и сейчас, чтобы стать счастливее\/эффективнее\/довольнее. При этом выбирают для изменения то, что больше «болит» (причиняет дискомфорт) или то, что легче всего поменять (для чего есть навык). Знакомо?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Еще как!",
    "trigger": "",
    "linked_to": "marathontask11",
    "linked_to_id": "875"
  }, {
    "on_click": [],
    "text": "Да нет..",
    "trigger": "",
    "linked_to": "marathontask12",
    "linked_to_id": "876"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "875",
  "textid": "marathontask11",
  "name": "Задание марафона 11",
  "message": "Ты не одинок_а! И та, и другая стратегия не слишком эффективны. Мы подойдем к определению проблемной области комплексно — со стороны рациональной оценки и развития чувствительности к твоим потребностям. Готов_а к заданию?\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да!",
    "trigger": "",
    "linked_to": "marathontask111",
    "linked_to_id": "877"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "876",
  "textid": "marathontask12",
  "name": "Задание марафон 12",
  "message": "Отлично, если так!\r\nИ та, и другая стратегия – не слишком успешна.\r\nМы подойдем к определению проблемной области комплексно – со стороны рациональной оценки и развития чувствительности к твоим потребностям. Готов_а к заданию?\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да!",
    "trigger": "",
    "linked_to": "marathontask111",
    "linked_to_id": "877"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "877",
  "textid": "marathontask111",
  "name": "Задание марафона 111",
  "message": "Первое #задание – про рациональную оценку. Вот схема «Колеса баланса» - оцени по \"спицам\" в колесе степень удовлетворенности своей жизнью по шкале от 0 до 10. Если поймешь, что какие-то важные для тебя сферы жизни тут не отражены, нарисуй свое \"колесо\" и пришли мне картинку.\r\n\r\nЗавтра будет следующее. ",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/колесо баланса.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "912",
  "textid": "marathontask2",
  "name": "Марафон задание 2",
  "message": "Привет! Сегодня второе задание\r\nИ мы продолжаем разбираться с тем, что <b>по-настоящему важно <\/b> в жизни и что <b>хотелось бы изменить.<\/b>\r\n \r\nИсследования показали, что люди лучше достигают целей по изменениям, если они (цели) связаны с их (людей) <b>истинными потребностями.<\/b> Один из способов понять, что для тебя является действительно значимым – поработать со <b>списком ценностей.<\/b> ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Получить задание",
    "trigger": "",
    "linked_to": "marathontask21",
    "linked_to_id": "915"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/колеса.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "915",
  "textid": "marathontask21",
  "name": "Марафон задание 21",
  "message": "Вот #задание\r\n\r\nЗдесь https:\/\/graph.org\/Metodika-Cennostnye-orientacii-Miltona-Rokicha-10-26 ты найдешь инструкцию, что делать. Пришли получившиеся списки мне, чтобы они сохранились в чате. Они тебе понадобятся на следующих этапах.\r\n\r\nСледующее задание будет завтра!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "918",
  "textid": "marathontask3",
  "name": "Задание марафон 3",
  "message": "А вот и третье задание \r\n\r\nНа него может потребоваться время, но до конца цепочки ты точно успеешь его выполнить. Важно понимать, какими ресурсами ты обладаешь, чтобы мочь сформулировать конкретную и реалистичную цель, а в дальнейшем составить хороший план действий.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Что для этого надо сделать?",
    "trigger": "",
    "linked_to": "marathontask31",
    "linked_to_id": "919"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/литсо.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "919",
  "textid": "marathontask31",
  "name": "Задание марафон 31",
  "message": "#задание\r\n\r\nСпроси нескольких (не меньше 3) друзей и\/или коллег, какие сильные стороны они в тебе видят, что считают твоими отличительными чертами, которые они ценят и, возможно, хотели бы обладать такими же. Запиши все, что услышишь, и пришли мне.\r\n\r\nСледующее задание завтра.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "920",
  "textid": "marathontask4",
  "name": "Марафон задание 4",
  "message": "А вот четвертый день.\r\nЕсли ты выполнил_а предыдущие задания, то у тебя уже есть материал для анализа ситуации. Однако иногда наши ощущения и эмоции способны гораздо лучше, чем сознательные убеждения, рассказать о том, что для нас действительно важно изменить. Полезно иметь информацию из обоих источников :)",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, но как научиться ее получать?",
    "trigger": "",
    "linked_to": "marathontask41",
    "linked_to_id": "921"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/Прислушайся к ощущениям.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "921",
  "textid": "marathontask41",
  "name": "Задание Марафон 41",
  "message": "Твое следующее #задание — на развитие внимательности к собственным ощущениям. По моему напоминанию в течение дня отправляй мне ответы на вопросы: \r\n1. В какой ты ситуации?\r\n2. Что ты чувствуешь?\r\n3. Насколько это важно для тебя по шкале от 1 до 10?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "922",
  "textid": "marathontask421",
  "name": "Марафон задание 4 напоминалка1",
  "message": "Продолжаем развивать внимательность к собственным ощущениям.\r\n\r\n1. В какой ты ситуации?\r\n2. Что ты чувствуешь?\r\n3. Насколько это важно для тебя по шкале от 1 до 10?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "923",
  "textid": "marathontask5",
  "name": "марафон задание 5",
  "message": "Поздравляю, ты подошел_а к середине пути! А мы продолжим развивать внимательность к своим потребностям — ведь, как ты помнишь, вероятность достичь цели сильно увеличивается, если цель связана с твоими истинными потребностями.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Ок, что надо сделать?",
    "trigger": "",
    "linked_to": "marathontask51",
    "linked_to_id": "924"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/внимательность к ощущениям.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "924",
  "textid": "marathontask51",
  "name": "Задание Марафон51",
  "message": "#задание\r\nПосмотри на свои ответы от предыдущих заданий (вчерашнего и задания про ранжирование ценностей).",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Сделано, дальше",
    "trigger": "",
    "linked_to": "marathontask52",
    "linked_to_id": "925"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "925",
  "textid": "marathontask52",
  "name": "Задание марафон 52",
  "message": "#задание\r\nСопоставь чувства, о которых ты писал_а вчера, контекст (ситуации) и список ценностей. Скажи, связаны ли какие-то и этих переживаний с важными ценностями, которые находятся в топе списка?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, связаны",
    "trigger": "",
    "linked_to": "marathontask531",
    "linked_to_id": "926"
  }, {
    "on_click": [],
    "text": "Нет, не связаны",
    "trigger": "",
    "linked_to": "marathontask532",
    "linked_to_id": "927"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "926",
  "textid": "marathontask531",
  "name": "Задание марафон 531",
  "message": "#задание\r\nОтлично! Напиши, с какими именно. Мы вернемся к этому ровно через 2 задания.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "927",
  "textid": "marathontask532",
  "name": "Задание марафон 532",
  "message": "Напиши, с какими тогда важными другими ценностями это может быть связано.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "929",
  "textid": "marathonResponse",
  "name": "Ответ марафонца",
  "message": "Отлично!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "940",
  "textid": "marathontask6",
  "name": "Задание марафона 6",
  "message": "Привет! Сегодня новый день игры. Ровно половина цепочки позади! Давай подведем промежуточные итоги.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Давай!",
    "trigger": "",
    "linked_to": "marathontask61",
    "linked_to_id": "942"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "942",
  "textid": "marathontask61",
  "name": "Задание Марафон 61",
  "message": "Если ты выполнял_а предыдущие задания, то у тебя есть:\r\n\r\n1. Представление о том, какими сферами жизни ты сильнее или слабее удовлетворен_а (задание 1)\r\n\r\n2. Какие ценности в жизни для тебя сейчас важнее всего, а какими можно пренебречь (задание 2)\r\n\r\n3. С какими ценностями связаны твои повседневные переживания. И проявляются ли твои главные ценности в значимых повседневных переживаниях и ситуациях (задание 5)\r\n\r\n4. Еще я надеюсь, что ты узнал_а кое-что про свои сильные стороны в глазах других людей (задание 3)\r\n\r\nЕсли чего-то нет, догоняй по предыдущим  заданиям! ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Все есть",
    "trigger": "",
    "linked_to": "marathontask62",
    "linked_to_id": "943"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "943",
  "textid": "marathontask62",
  "name": "Задание марафон 62",
  "message": "Отлично, у тебя есть все необходимое, чтобы определиться с направлением для изменений и сформулировать эффективную цель. К этому мы приступим завтра\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А что сегодня?",
    "trigger": "",
    "linked_to": "marathontask63",
    "linked_to_id": "944"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "944",
  "textid": "marathontask63",
  "name": "Задание марафон 63",
  "message": "#задание #отдых\r\n\r\nА сегодня — еще одна важная штука. Обрати внимание!\r\nЧтобы эффективно достигать целей, надо эффективно отдыхать и заботиться о себе. Поэтому ровно посередине пути из нашего пункта А в пункт Б мы сделаем привал. \r\n\r\nТвое следующее задание — придумать для ближайших пяти дней способ приятно отдохнуть, восстановить силы - даже если ты сможешь выделить всего 15 минут в день, но пусть это будет чистым временем только для себя, временем на что-то очень приятное и пополняющее силы. Напиши мне, какие способы отдыхать ты будешь использовать.",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/relax.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "949",
  "textid": "marathontask7",
  "name": "Марафон задание 7",
  "message": "Надеюсь, ты хорошо отдохнул_а и будешь делать это и дальше :) До конца цепочки осталось 4 задания, и за это время мы определим, над чем важным ты хочешь подумать и поработать в дальнейшем, а потом превратим это в хорошую цель. В конце тебя ждет бонусное #задание, чтобы прикинуть, какие ресурсы у тебя уже есть, а каких не хватает для достижения цели.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Как выделить самую актуальную проблему?",
    "trigger": "",
    "linked_to": "marathontask71",
    "linked_to_id": "950"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/IMG_013.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "950",
  "textid": "marathontask71",
  "name": "Задание марафон 71",
  "message": "Выпиши из предыдущих заданий:\r\n1. Из Колеса баланса - 3 сферы с самыми низкими баллами\r\n2. Из списка терминальных ценностей - 5 топовых ценностей\r\n3. Из списка актуальных эмоций\/переживаний, которые ты оценил_а как важные (больше 7-8 баллов) - 2-3 сферы жизни, к которым эти эмоции относятся. Возможно, у тебя не было таких важных переживаний в день выполнения заданий. Это нормально. Тогда ориентируйся на материал из пунктов 1 и 2.\r\n\r\n#задание",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово!",
    "trigger": "",
    "linked_to": "marathontask72",
    "linked_to_id": "951"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "951",
  "textid": "marathontask72",
  "name": "Задание марафон 72",
  "message": "#задание\r\nПосмотри, где у тебя есть пересечения. Есть ли сферы с низкими баллами, которые одновременно попали в топ ценностей? И которые в повседневной жизни вызывают сильные переживания?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Есть",
    "trigger": "",
    "linked_to": "marathontask731",
    "linked_to_id": "959"
  }, {
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "marathontask732",
    "linked_to_id": "953"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "953",
  "textid": "marathontask732",
  "name": "Задание марафон 732",
  "message": "Такое бывает. И это значит, что, скорее всего, ты не уделяешь достаточно внимания каким-то сферам своей жизни, потому что они не кажутся тебе достаточно важными, не связаны с твоими ключевыми потребностями. \r\n\r\nЕсли ты все-таки считаешь, что стоит их \"подтянуть\", подумай: а) чем эти сферы могут быть полезны для твоих топовых терминальных ценностей? б) связаны ли эти сферы с твоими инструментальными ценностями? \r\n\r\nНапример, заниматься здоровьем самим по себе тебе может казаться неинтересным и утомительным, но если осознать, что оно выступает средством для активной деятельной жизни или получения новых впечатлений и путешествий (или чего-то другого, значимого для тебя лично), мотивация изменить образ жизни может усилиться.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Кажется, понятно. Давай дальше",
    "trigger": "",
    "linked_to": "marathontask7321",
    "linked_to_id": "954"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "954",
  "textid": "marathontask7321",
  "name": "Задание марафон 7321",
  "message": "Итак, ты можешь выбрать один из двух способов сформулировать проблему:\r\nА. Выбрать не главную по ценностям, но не удовлетворяющую тебя сферу жизни. При этом хорошо подумать, для чего тебе улучшать то, что не кажется важным. И можно ли это сделать важным)\r\nБ. Выбрать не самую \"проваленную\" сферу жизни, но при этом  попадающую в топ ценностей.\r\n\r\nВыбери и подумай, какая именно проблема из выбранной сферы беспокоит тебя на сегодняшний день больше всего. Напиши ее мне.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "959",
  "textid": "marathontask731",
  "name": "Задание марафон 731",
  "message": "#задание Эти сферы стоит сделать приоритетом. Можешь выбрать для формулировки проблемы а) сферу с самым низким баллом по удовлетворенности, либо б) с самым высоким рангом в списке ценностей.\r\n\r\nПодумай, какая именно проблема из этой сферы беспокоит тебя на сегодняшний день больше всего. Напиши ее мне.",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/IMG_013.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "968",
  "textid": "marathonBAD",
  "name": "марафон чего-то нет",
  "message": "Сейчас разберемся!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "976",
  "textid": "marathontask8",
  "name": "Задание марафона 8",
  "message": "#задание Сегодня наступает важный момент - мы будем <b>превращать проблему в цель<\/b>. Доказано, что цели, сформулированные как желание чего-то достичь, реализуются чаще, чем сформулированные как желание от чего-то избавиться. \r\n\r\nНапример - \"я хочу найти новую интересную работу\" - более эффективная цель, чем \"я хочу перестать работать на старой скучной работе\".",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "marathontask81",
    "linked_to_id": "1408"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/время действовать.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "979",
  "textid": "marathontask9",
  "name": "Задание марафона 9",
  "message": "#задание\r\nДвигаемся дальше. \r\n\r\nФормулировка цели должна быть не только <b>позитивной <\/b> (то есть описывать то, чего ты хочешь достичь, а не избежать или избавиться), а еще и очень <b>конкретной<\/b>. \r\n\r\nНадеюсь, ты уже описал_а, каких изменений в жизни ожидаешь, когда решишь свою проблему (задание 8). Настало время подумать об этих изменениях детальнее.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "marathontask901",
    "linked_to_id": "1413"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/2019-11-05 00.01.23.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "980",
  "textid": "marathontask10",
  "name": "Задание марафона 10",
  "message": "#задание Помнишь, мы просили собрать мнения знакомых о твоих сильных сторонах? \r\n\r\nСейчас эта информации, а также твое собственное представление о ресурсах пригодится, чтобы оценить твою цель по еще одному важному критерию - <b>достижимости<\/b>. \r\n\r\nа) Сначала ответь себе на вопрос — <b>включает ли моя цель изменения, которые должны произойти с другими людьми или обстоятельствами, которые от меня не зависят или почти не зависят?<\/b> Скажем, если ты хочешь, чтобы другие люди стали лучше тебя понимать или больше любить и уважать, это не будет хорошей формулировкой цели, потому что ее выполнение часто не зависит от тебя. Твоя формулировка должна включать только то, что связано лично с тобой, с твоими решениями, поведением и состоянием.\r\n\r\nб) Затем вспомни <b>все ресурсы, о которых говорили другие люди и знаешь ты сам_а и ответь на вопрос - есть ли у меня все необходимое, чтобы достичь цели?<\/b> Если да, отлично, дальше дело стоит за разработкой плана. Если нет, то чего именно не хватает? Знаешь ли ты, как ты можешь нарастить свои ресурсы и кто тебе может в этом помочь?\r\n\r\nОтправь мне ответы, чуть позже тебя ждет бонус.",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/upselfesteem-Recovered.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "983",
  "textid": "marathontask91",
  "name": "Задание марафон 91",
  "message": "Ок!\r\nКстати, завтра последний день вводной части игры — ты получишь еще одно основное и одно бонусное задание. \r\n\r\nУ тебя будет хорошо сформулированная цель — этот фактор вносит около 30% вклада в ее достижение. \r\n\r\nЧтобы дойти до цели более гарантированно, можно овладеть инструментами планирования, поддержки мотивации, сбора обратной связи и корректировки планов на основе опыта. \r\nВсе это ты можешь получить в игре \"Зарешай\"\r\n\r\nБери билет на zaresh.ai",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "986",
  "textid": "marathontask101",
  "name": "Задание марафона 101",
  "message": "Напоминаем, что это задание - последнее в цепочке (ну почти последнее - вечером будет еще бонусное :)\r\n\r\nЕсли ты планомерно двигался_лась по всем шагам, то у тебя есть все, чтобы сформулировать четкую, позитивную и достижимую цель. Если хочешь так же планомерно начать идти к ней, записывайся на игру \"Зарешай\", общайся в течение месяца с ботом и\/или опытными экспертами и делай свои планы реальностью zaresh.ai\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "987",
  "textid": "marathonbonus",
  "name": "Бонус Марафон",
  "message": "А вот и обещанное бонусное задание. \r\n\r\nПопробуй проявить творческий подход в размышлении над твоей целью и своими ресурсами.\r\n\r\nПокажи результат друзьям или запости в соцсети с хэштегом #мойсупергерой #зарешай\r\n\r\nМне тоже напиши, мне интересно!\r\n\r\n<b>Задание: придумай своего личного супер-героя <\/b>\r\n\r\nВ чём фишка супер-героев? У них есть какие-то исходные таланты, с помощью которых им легче достигать своих целей. \r\nНо некоторые еще и много работали над тем, чтобы научиться пользоваться этими талантами или приобрести другие сильные черты, которые существенно оптимизировали их деятельность.\r\n\r\nПридумай своего супер-героя. Про свои цели и сильные черты ты уже что-то знаешь. Какую супер-силу в дополнение к своим качествам ты хотел_а бы получить, чтобы достичь цели? Как бы ты назвал_а своего супергероя?\r\n\r\n<b>Примеры:<\/b>\r\n\r\nТы умеешь осиливать сложные задачи, а было бы здорово ещё и доводить их до впечатляющего результата\r\nКреативный, а вот здорово бы добавить усидчивости\r\nУмная и способная, хорошо бы определиться, куда направить такой талант\r\nУпорный работник, которому не хватает умения так же погружаться в отдых\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/IMAGE 2019-11-06 19:17:30.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "988",
  "textid": "marathonquestionaruifin1",
  "name": "финишная анкета марафона",
  "message": "Насколько полученный в игре результат совпадает с тем, чего ты ожидал_а по шкале от 0 (вообще не то) до 9 (полностью совпадает)? ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "989",
  "textid": "marathonquestionaruifin2",
  "name": "финишная анкета марафона 2",
  "message": "Что больше всего понравилось в игре?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "990",
  "textid": "marathonquestionaruifin3",
  "name": "финишная анкета марафона 3",
  "message": "Что меньше всего понравилось?\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "991",
  "textid": "marathonquestionaruifin4",
  "name": "финишная анкета марафона 4",
  "message": "Чего тебе не хватило во время игры?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "992",
  "textid": "marathonquestionaruifin5",
  "name": "финишная анкета марафона 5",
  "message": "Что было особенно полезным?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "993",
  "textid": "marathonquestionaruifin6",
  "name": "финишная анкета марафона 6",
  "message": "Ещё несколько вопросов, которые помогут тебе успешно внедрить полезные штуки в твою жизнь.\r\n\r\nВ ходе прохождения заданий ты узнал_а о двух способах понимать, что для тебя важно.\r\n1. Анализ своих ценностей.\r\n2. Обратная связь от эмоций и ощущений.\r\n\r\nРасскажи в свободной форме, какой из способов тебе даётся лучше?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "994",
  "textid": "marathonquestionaruifin7",
  "name": "финишная анкета марафона 7",
  "message": "В процессе мы обращали твоё внимание на то, какими ресурсами ты обладаешь. \r\nБыли ли у тебя в этом открытия? Есть ли у тебя мысли о том, почему это важно и как это может сделать твою жизнь лучше?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "995",
  "textid": "marathonquestionaruifin8",
  "name": "финишная анкета марафона 8",
  "message": "Работая над формулировкой цели, мы сказали о значимости того, чтобы:\r\n- Цель была измерима;\r\n- Её достижение было в зоне твоего контроля;\r\n- Была сформулирована позитивна;\r\n- Была связана с твоими ценностями.\r\nКак по-твоему, как это может помочь тебе повысить вероятность достижения цели и почему?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "996",
  "textid": "marathonfinal",
  "name": "Финал марафона",
  "message": "Вот ты и подошел_а к концу стартовой цепочки. \r\nНадеюсь, что ты вынес_ла из нее полезное знание и инструменты думания о важном - проблемных и ресурсных областях, способах фокусировки на своих эмоциях и потребностях, трансформации проблемы в цель. \r\n\r\nЭто только первые шаги к эффективному решению жизненных задач и получению желаемых результатов: жизнь слишком коротка, чтобы не учиться её жить!\r\nУдачи тебе на этом пути! А если хочешь, zaresh.ai их вместе с нами в игре :)\r\n\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1006",
  "textid": "terms",
  "name": "Условия",
  "message": "ПУБЛИЧНАЯ ОФЕРТА ОБ ОКАЗАНИИ КОНСУЛЬТАЦИОННЫХ УСЛУГ\r\n\r\nДанный документ является официальным предложением (публичной офертой) физического лица-предпринимателя Медведева Данилы Андреевича, действующего на основании свидетельства о государственной регистрации физического лица-предпринимателя №77 017104523 в дальнейшем именуемого также «Исполнитель», об оказании консультационных услуг заказчику, в дальнейшем именуемому «Клиент».\r\n\r\nСтатья 1. Предмет договора\r\n1.1. Исполнитель обязуется оказать Услуги личного коучинга в рамках программы “Зарешай” (далее — «Услуги»), а Клиент обязуется оплатить и принять заказанные услуги.\r\n1.2. Услуги оказываются Клиенту путем проведения консультаций онлайн, посредством программы Телеграм.\r\n\r\nСтатья 2. Момент заключения договора\r\n2.1. Данный договор является публичной офертой и считается заключенным на настоящих условиях с момента оплаты Услуг, определяемого в соответствии с п. 4.1 настоящего Договора.\r\n\r\nСтатья 3. Цена услуг\r\n3.1. Цена на Услуги указывается на странице https:\/\/prapion.me\/zareshai.\r\n3.2. Общая стоимость Услуг зависит от вида выбранных Клиентом Услуг при оплате.\r\n3.3. Цена на Услуги и способ оплаты может меняться. Исполнитель обязуется предупреждать Клиента, регулярно пользующегося Услугами Исполнителя, об изменении цены не менее, чем за месяц до даты изменения.\r\n\r\nСтатья 4. Оплата услуг\r\n4.1. Оплата Услуг производится одним из способов, указанных на странице: https:\/\/prapion.me\/zareshai либо через бота Зарешай в Телеграм.\r\n4.2. Обязанность Клиента по оплате стоимости Услуг считается исполненной с момента передачи платежным сервисом Исполнителю информации о завершении перевода денежных средств в оплату Услуги в соответствии с правилами данного платежного сервиса.\r\n\r\nСтатья 5. Порядок и сроки оказания услуг\r\n5.1. Услуги предоставляются Клиенту в объеме, соответствующем сумме произведенной Клиентом оплаты Услуг.\r\n5.2. Оказание Услуг осуществляется в сроки и согласно расписанию, опубликованному на сайте https:\/\/prapion.me\/zareshai, после оплаты Клиентом стоимости Услуг.\r\n5.3. Продолжительность участия в программе коучинга “Зарешай” составляет 1 месяц (30 дней).\r\n5.4. Содержание и методы работы во время проведения программы коучинга “Зарешай” определяются Исполнителем (а также может определяться Исполнителем и Клиентом совместно), исходя из потребностей Клиента и представления Исполнителя о конструктивных методах работы, а также личного профессионального опыта.\r\nПри этом Клиент имеет право получать необходимую ему информацию о методах работы в процессе проведения коучинга, в любой момент может отказаться от любого предложения Исполнителя, а также с ведома Исполнителя прекратить коучинг в любой момент времени.\r\n5.5. Обязательства сторон считаются исполненными надлежащим образом после оказания Клиенту Услуг путем проведения личного коучинга Исполнителем в рамках программы “Зарешай” в течение срока, определяемого суммой произведенной Клиентом оплаты Услуг.\r\n\r\nСтатья 6. Политика безопасности и нераспространения личных данных пользователей\r\n6.1. Исполнитель обязуется не разглашать любую информацию, сообщенную Клиентом Исполнителю во время оказания Услуг, без письменного согласия Клиента.\r\n\r\nСтатья 7. Ответственность сторон и разрешение споров\r\n7.1. Стороны несут ответственность за неисполнение или ненадлежащее исполнение настоящего Договора в порядке, предусмотренном настоящим Договором и действующим законодательством Российской Федерации.\r\n7.2. Исполнитель не несет ответственности, если ожидания Клиента о качестве Услуг оказались не оправданы.\r\n7.3. Исполнитель не несет ответственности за частичное или полное неисполнение обязательств по оказанию Услуг, если они являются следствием форс-мажорных обстоятельств.\r\n7.4. Программа коучинга “Зарешай” не предполагает психологического консультирования и не несет ответственности за психологические отклонения, возникающие или обостряющиеся в процессе коучинга.\r\n7.5. Клиент, производя оплату в соответствии с п. 4.1 настоящего Договора, подтверждает, что с условиями настоящего Договора ознакомлен и согласен.\r\n7.6. Все споры и разногласия, возникающие при исполнении сторонами обязательств по настоящему Договору, решаются путем переговоров. В случае невозможности их устранения, стороны имеют право обратиться за судебной защитой своих интересов.\r\n\r\nСтатья 8. Возврат денежных средств\r\n8.1. Клиент не вправе требовать возврата денежных средств за уже оказанные услуги (проведенные консультации).\r\n8.2. В случае отказа Исполнителя от выполнения договора Клиент вправе потребовать от Исполнителя возвращения денежных средств, оплаченных Исполнителем, за еще не проведенные консультации.\r\n8.3. Возврат платежей на электронные кошельки не производится.\r\n\r\nСтатья 9. Форс-мажорные обстоятельства\r\n9.1. Стороны освобождаются от ответственности за неисполнение или ненадлежащее исполнение обязательств по настоящему Договору на время действия непреодолимой силы. Под непреодолимой силой понимаются чрезвычайные и непреодолимые при данных условиях обстоятельства, препятствующие исполнению своих обязательств Сторонами по настоящему Договору. К ним относятся стихийные явления (землетрясения, наводнения и т. п.), обстоятельства общественной жизни (военные действия, чрезвычайные положения, забастовки, эпидемии и т. п.), запретительные меры государственных органов (запрещение перевозок, валютные ограничения, международные санкции запрета на торговлю и т. п.). В течение этого времени стороны не имеют взаимных претензий, и каждая из сторон принимает на себя свой риск последствия форс-мажорных обстоятельств.\r\n\r\nСтатья 10. Срок действия и иные условия договора\r\n10.1. Настоящий Договор вступает в силу с момента оплаты Клиентом Услуг, определяемого в соответствии с п. 4.1 настоящего Договора, и прекращается при полном исполнении обязательств сторонами.\r\n10.2. Исполнитель оставляет за собой право изменять настоящий Договор в одностороннем порядке до момента его заключения.\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1007",
  "textid": "9november19",
  "name": "9 ноября 19",
  "message": "Отлично, я тебя вижу!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я уже играл(а)",
    "trigger": "",
    "linked_to": "snova",
    "linked_to_id": "435"
  }, {
    "on_click": [],
    "text": "Я играю в первый раз",
    "trigger": "",
    "linked_to": "startquestionarie",
    "linked_to_id": "1009"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1009",
  "textid": "startquestionarie",
  "name": "Стартовая анкета",
  "message": "Привет! Давай для начала я тебя расспрошу о тебе, чтобы лучше понимать, что тебе предлагать. \r\nСколько тебе лет?\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1010",
  "textid": "startquestionarie2",
  "name": "Стартовая анкета 2",
  "message": "Как ты узнал_а об игре?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1011",
  "textid": "startquestionarie3",
  "name": "Старт анкета 3",
  "message": "Какая у тебя мотивация участия в игре?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1012",
  "textid": "startquestionarie4",
  "name": "Анкета 4",
  "message": "Какие у тебя ожидания от участия в игре?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1017",
  "textid": "startquestionarie5",
  "name": "Анкета 5",
  "message": "Тебя добавлять в команду, или хочешь играть сам_а?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1018",
  "textid": "startquestionarie6",
  "name": "Анкета 6",
  "message": "В каком городе будешь во время игры?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1019",
  "textid": "startquestionarie7",
  "name": "Анкета 7",
  "message": "Сейчас будет серия вопросов, чтобы понять, где ты сейчас :) \r\n\r\n<b>Как у тебя обстоят дела с планированием?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 0
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 0
    }],
    "text": "Не планирую ничего",
    "trigger": "",
    "linked_to": "startquestionarie8",
    "linked_to_id": "1020"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 1
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 1
    }],
    "text": "Планирую интуитивно",
    "trigger": "",
    "linked_to": "startquestionarie8",
    "linked_to_id": "1020"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 2
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 2
    }],
    "text": "Читал_а о какой-то методологии",
    "trigger": "",
    "linked_to": "startquestionarie8",
    "linked_to_id": "1020"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 3
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 3
    }],
    "text": "Пользуюсь какой-то методологией",
    "trigger": "",
    "linked_to": "startquestionarie8",
    "linked_to_id": "1020"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 4
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 4
    }],
    "text": "Регулярно составляю планы и отслеживаю выполнение",
    "trigger": "",
    "linked_to": "startquestionarie8",
    "linked_to_id": "1020"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1020",
  "textid": "startquestionarie8",
  "name": "Анкета 8",
  "message": " <b>У тебя есть поставленный процесс личного стратегирования?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 0
    }, {
      "type": "update_user_progress",
      "target": "stamina",
      "value": 0
    }, {
      "type": "update_user_progress",
      "target": "agency",
      "value": 0
    }],
    "text": "Не знаю, что это",
    "trigger": "",
    "linked_to": "startquestionarie9",
    "linked_to_id": "1021"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 1
    }, {
      "type": "update_user_progress",
      "target": "stamina",
      "value": 1
    }, {
      "type": "update_user_progress",
      "target": "agency",
      "value": 1
    }],
    "text": "Нет, в моей жизни все хаотично",
    "trigger": "",
    "linked_to": "startquestionarie9",
    "linked_to_id": "1021"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 2
    }, {
      "type": "update_user_progress",
      "target": "stamina",
      "value": 2
    }, {
      "type": "update_user_progress",
      "target": "agency",
      "value": 2
    }],
    "text": "Какой-то есть, но больше на интуиции",
    "trigger": "",
    "linked_to": "startquestionarie9",
    "linked_to_id": "1021"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 3
    }, {
      "type": "update_user_progress",
      "target": "stamina",
      "value": 3
    }, {
      "type": "update_user_progress",
      "target": "agency",
      "value": 3
    }],
    "text": "Стараюсь наладить",
    "trigger": "",
    "linked_to": "startquestionarie9",
    "linked_to_id": "1021"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 4
    }, {
      "type": "update_user_progress",
      "target": "stamina",
      "value": 4
    }, {
      "type": "update_user_progress",
      "target": "agency",
      "value": 4
    }],
    "text": "Да, есть несколько уровней стратегирования",
    "trigger": "",
    "linked_to": "startquestionarie9",
    "linked_to_id": "1021"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1021",
  "textid": "startquestionarie9",
  "name": "Анкета 9",
  "message": "<b>Оцени, насколько твои ежедневные дела вписываются в твою общую стратегию?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 0
    }],
    "text": "У меня нет стратегии",
    "trigger": "",
    "linked_to": "startquestionarie10",
    "linked_to_id": "1022"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 1
    }],
    "text": "Я делаю только то, что вписывается в мою стратегию",
    "trigger": "",
    "linked_to": "startquestionarie10",
    "linked_to_id": "1022"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 2
    }],
    "text": "50\/50",
    "trigger": "",
    "linked_to": "startquestionarie10",
    "linked_to_id": "1022"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1022",
  "textid": "startquestionarie10",
  "name": "Анкета 10",
  "message": "<b>Удавалось ли тебе обычно придерживаться планов по самообучению?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 0
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 0
    }],
    "text": "Никогда",
    "trigger": "",
    "linked_to": "startquestionarie11",
    "linked_to_id": "1023"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 1
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 1
    }],
    "text": "Бывало, но это скорее исключение",
    "trigger": "",
    "linked_to": "startquestionarie11",
    "linked_to_id": "1023"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 2
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 2
    }],
    "text": "Редко",
    "trigger": "",
    "linked_to": "startquestionarie11",
    "linked_to_id": "1023"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 3
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 3
    }],
    "text": "Обычно удавалось",
    "trigger": "",
    "linked_to": "startquestionarie11",
    "linked_to_id": "1023"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "strategy",
      "value": 4
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 4
    }],
    "text": "Всегда удается",
    "trigger": "",
    "linked_to": "startquestionarie11",
    "linked_to_id": "1023"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1023",
  "textid": "startquestionarie11",
  "name": "Анкета 11",
  "message": "<b>Сколько у тебя обычно продуктивных часов в неделю?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 0
    }],
    "text": "0-10",
    "trigger": "",
    "linked_to": "startquestionarie12",
    "linked_to_id": "1024"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 1
    }],
    "text": "10-25",
    "trigger": "",
    "linked_to": "startquestionarie12",
    "linked_to_id": "1024"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 2
    }],
    "text": "25-40",
    "trigger": "",
    "linked_to": "startquestionarie12",
    "linked_to_id": "1024"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 3
    }],
    "text": "40-50",
    "trigger": "",
    "linked_to": "startquestionarie12",
    "linked_to_id": "1024"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 4
    }],
    "text": "50-80",
    "trigger": "",
    "linked_to": "startquestionarie12",
    "linked_to_id": "1024"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1024",
  "textid": "startquestionarie12",
  "name": "Анкета 12",
  "message": "<b>Удается ли тебе организовать процесс работы над чем-то без внешней мотивации и регулярно работать по плану?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 0
    }],
    "text": "Делаю только из-под палки",
    "trigger": "",
    "linked_to": "startquestionarie13",
    "linked_to_id": "1025"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 1
    }],
    "text": "Могу, но недолго",
    "trigger": "",
    "linked_to": "startquestionarie13",
    "linked_to_id": "1025"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 2
    }],
    "text": "Если дело важное, могу",
    "trigger": "",
    "linked_to": "startquestionarie13",
    "linked_to_id": "1025"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 3
    }],
    "text": "В целом да",
    "trigger": "",
    "linked_to": "startquestionarie13",
    "linked_to_id": "1025"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 4
    }],
    "text": "Отлично самоорганизуюсь, работаю много и хорошо",
    "trigger": "",
    "linked_to": "startquestionarie13",
    "linked_to_id": "1025"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1025",
  "textid": "startquestionarie13",
  "name": "Анкета 13",
  "message": "<b>Ты умеешь ставить таймер на пять минут, чтобы решить не очень сложную проблему?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 0
    }],
    "text": "Первый раз слышу про такой способ",
    "trigger": "",
    "linked_to": "startquestionarie14",
    "linked_to_id": "1026"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 1
    }],
    "text": "Знаю, но для меня не работает",
    "trigger": "",
    "linked_to": "startquestionarie14",
    "linked_to_id": "1026"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 2
    }],
    "text": "Обычно использую",
    "trigger": "",
    "linked_to": "startquestionarie14",
    "linked_to_id": "1026"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 3
    }],
    "text": "Пользуюсь часто",
    "trigger": "",
    "linked_to": "startquestionarie14",
    "linked_to_id": "1026"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "stamina",
      "value": 4
    }],
    "text": "Да! И даже для сложных проблем",
    "trigger": "",
    "linked_to": "startquestionarie14",
    "linked_to_id": "1026"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1026",
  "textid": "startquestionarie14",
  "name": "Анкета 14",
  "message": "<b>Знакома ли тебе концепция стейкхолдерского анализа?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 0
    }],
    "text": "Не знаю даже близко",
    "trigger": "",
    "linked_to": "startquestionarie15",
    "linked_to_id": "1027"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 1
    }],
    "text": "Знаю слово ",
    "trigger": "",
    "linked_to": "startquestionarie15",
    "linked_to_id": "1027"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 2
    }],
    "text": "Думаю, знаю неплохо",
    "trigger": "",
    "linked_to": "startquestionarie15",
    "linked_to_id": "1027"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 3
    }],
    "text": "Знаю хорошо",
    "trigger": "",
    "linked_to": "startquestionarie15",
    "linked_to_id": "1027"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 4
    }],
    "text": "Да, и я умею его делать",
    "trigger": "",
    "linked_to": "startquestionarie15",
    "linked_to_id": "1027"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1027",
  "textid": "startquestionarie15",
  "name": "Анкета 15",
  "message": "<b>Насколько часто у тебя бывают конфликты с другими людьми?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 0
    }],
    "text": "Почти каждый день",
    "trigger": "",
    "linked_to": "startquestionarie16",
    "linked_to_id": "1028"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 1
    }],
    "text": "Нередко",
    "trigger": "",
    "linked_to": "startquestionarie16",
    "linked_to_id": "1028"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 2
    }],
    "text": "Редко",
    "trigger": "",
    "linked_to": "startquestionarie16",
    "linked_to_id": "1028"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 3
    }],
    "text": "Почти не бывает",
    "trigger": "",
    "linked_to": "startquestionarie16",
    "linked_to_id": "1028"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 4
    }],
    "text": "Не могу даже вспомнить последний случай",
    "trigger": "",
    "linked_to": "startquestionarie16",
    "linked_to_id": "1028"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1028",
  "textid": "startquestionarie16",
  "name": "Анкета 16",
  "message": "<b>Удаётся ли тебе систематически отстаивать свои интересы в общении? <\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 0
    }],
    "text": "Явно не удается",
    "trigger": "",
    "linked_to": "startquestionarie17",
    "linked_to_id": "1029"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 1
    }],
    "text": "Удается только с некоторыми людьми",
    "trigger": "",
    "linked_to": "startquestionarie17",
    "linked_to_id": "1029"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 2
    }],
    "text": "50\/50",
    "trigger": "",
    "linked_to": "startquestionarie17",
    "linked_to_id": "1029"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 3
    }],
    "text": "Только если нет сильного давления",
    "trigger": "",
    "linked_to": "startquestionarie17",
    "linked_to_id": "1029"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 4
    }],
    "text": "Я хорошо умею постоять за себя",
    "trigger": "",
    "linked_to": "startquestionarie17",
    "linked_to_id": "1029"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1029",
  "textid": "startquestionarie17",
  "name": "Анкета 17",
  "message": "<b>Сталкиваешься ли ты с проблемами в том, чтобы объяснить людям чего ты хочешь и почему это важно?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 0
    }],
    "text": "Постоянно",
    "trigger": "",
    "linked_to": "startquestionarie18",
    "linked_to_id": "1030"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 1
    }],
    "text": "Частенько",
    "trigger": "",
    "linked_to": "startquestionarie18",
    "linked_to_id": "1030"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 2
    }],
    "text": "Довольно редко",
    "trigger": "",
    "linked_to": "startquestionarie18",
    "linked_to_id": "1030"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 3
    }],
    "text": "Почти никогда",
    "trigger": "",
    "linked_to": "startquestionarie18",
    "linked_to_id": "1030"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "commun",
      "value": 4
    }],
    "text": "Никогда",
    "trigger": "",
    "linked_to": "startquestionarie18",
    "linked_to_id": "1030"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1030",
  "textid": "startquestionarie18",
  "name": "Анкета 18",
  "message": "<b>Как часто у тебя бывает ощущение бессмысленности своих действий?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 0
    }],
    "text": "Постоянно ощущаю",
    "trigger": "",
    "linked_to": "startquestionarie19",
    "linked_to_id": "1031"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 1
    }],
    "text": "В большинстве ситуаций",
    "trigger": "",
    "linked_to": "startquestionarie19",
    "linked_to_id": "1031"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 2
    }],
    "text": "Иногда, но само проходит",
    "trigger": "",
    "linked_to": "startquestionarie19",
    "linked_to_id": "1031"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 3
    }],
    "text": "Очень редко",
    "trigger": "",
    "linked_to": "startquestionarie19",
    "linked_to_id": "1031"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 4
    }],
    "text": "Никогда",
    "trigger": "",
    "linked_to": "startquestionarie19",
    "linked_to_id": "1031"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1031",
  "textid": "startquestionarie19",
  "name": "Анкета 19",
  "message": "<b>Есть ли у тебя представление о том, чем ты хочешь заниматься в масштабе жизни?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 0
    }],
    "text": "Никогда не думал_а об этом",
    "trigger": "",
    "linked_to": "startquestionarie20",
    "linked_to_id": "1032"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 1
    }],
    "text": "Вообще нет",
    "trigger": "",
    "linked_to": "startquestionarie20",
    "linked_to_id": "1032"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 2
    }],
    "text": "В общих чертах",
    "trigger": "",
    "linked_to": "startquestionarie20",
    "linked_to_id": "1032"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 3
    }],
    "text": "Да, но можно точнее",
    "trigger": "",
    "linked_to": "startquestionarie20",
    "linked_to_id": "1032"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 4
    }],
    "text": "Есть хорошее представление",
    "trigger": "",
    "linked_to": "startquestionarie20",
    "linked_to_id": "1032"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1032",
  "textid": "startquestionarie20",
  "name": "Анкета 20",
  "message": "<b>У тебя бывает аналитический паралич?<\/b>\r\n\r\nЭто такое состояние, когда не можешь принять решение.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 0
    }],
    "text": "Не выхожу из него",
    "trigger": "",
    "linked_to": "startquestionarie21",
    "linked_to_id": "1033"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 1
    }],
    "text": "Довольно часто",
    "trigger": "",
    "linked_to": "startquestionarie21",
    "linked_to_id": "1033"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 2
    }],
    "text": "Иногда",
    "trigger": "",
    "linked_to": "startquestionarie21",
    "linked_to_id": "1033"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 3
    }],
    "text": "Почти никогда",
    "trigger": "",
    "linked_to": "startquestionarie21",
    "linked_to_id": "1033"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "agency",
      "value": 4
    }],
    "text": "Вообще не про меня",
    "trigger": "",
    "linked_to": "startquestionarie21",
    "linked_to_id": "1033"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1033",
  "textid": "startquestionarie21",
  "name": "Анкета 21",
  "message": "<b>Знаком_а ли ты с байесианством?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 0
    }],
    "text": "Первый раз слышу",
    "trigger": "",
    "linked_to": "startquestionarie22",
    "linked_to_id": "1034"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 1
    }],
    "text": "Слышал_а что-то",
    "trigger": "",
    "linked_to": "buttonstartquestionarie22",
    "linked_to_id": null
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 2
    }],
    "text": "Представляю, что это",
    "trigger": "",
    "linked_to": "startquestionarie22",
    "linked_to_id": "1034"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 3
    }],
    "text": "Знаю о чем речь",
    "trigger": "",
    "linked_to": "startquestionarie22",
    "linked_to_id": "1034"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 4
    }],
    "text": "Применяю",
    "trigger": "",
    "linked_to": "startquestionarie22",
    "linked_to_id": "1034"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1034",
  "textid": "startquestionarie22",
  "name": "Анкета 22",
  "message": "<b>Когда встречаешь где-либо утверждения, которые для тебя важны, как часто занимаешься проверкой их на подлинность?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 0
    }],
    "text": "Хм, интересная мысль",
    "trigger": "",
    "linked_to": "startquestionarie23",
    "linked_to_id": "1035"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 1
    }],
    "text": "Иногда проверяю",
    "trigger": "",
    "linked_to": "startquestionarie23",
    "linked_to_id": "1035"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 2
    }],
    "text": "50\/50",
    "trigger": "",
    "linked_to": "startquestionarie23",
    "linked_to_id": "1035"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 3
    }],
    "text": "Чаще проверяю, чем нет",
    "trigger": "",
    "linked_to": "startquestionarie23",
    "linked_to_id": "1035"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 4
    }],
    "text": "Всегда или почти всегда",
    "trigger": "",
    "linked_to": "startquestionarie23",
    "linked_to_id": "1035"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1035",
  "textid": "startquestionarie23",
  "name": "Анкета 23",
  "message": "<b>Знакомо ли тебе понятие ожидаемой полезности?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 0
    }],
    "text": "А что это?",
    "trigger": "",
    "linked_to": "startquestionarie24",
    "linked_to_id": "1036"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 1
    }],
    "text": "Кажется, слова знакомые",
    "trigger": "",
    "linked_to": "startquestionarie24",
    "linked_to_id": "1036"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 2
    }],
    "text": "Вроде знаю что это",
    "trigger": "",
    "linked_to": "startquestionarie24",
    "linked_to_id": "1036"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 3
    }],
    "text": "Да, знакомо",
    "trigger": "",
    "linked_to": "startquestionarie24",
    "linked_to_id": "1036"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 4
    }],
    "text": "Конечно, я так решения принимаю!",
    "trigger": "",
    "linked_to": "startquestionarie24",
    "linked_to_id": "1036"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1036",
  "textid": "startquestionarie24",
  "name": "Анкета 24",
  "message": "<b>Насколько вероятно, что ты встретишь динозавра на улице?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 0
    }],
    "text": "50\/50",
    "trigger": "",
    "linked_to": "startquestionarie25",
    "linked_to_id": "1059"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 1
    }],
    "text": "Не знаю",
    "trigger": "",
    "linked_to": "startquestionarie25",
    "linked_to_id": "1059"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 2
    }],
    "text": "0",
    "trigger": "",
    "linked_to": "startquestionarie25",
    "linked_to_id": "1059"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 3
    }],
    "text": "Близко к нулю",
    "trigger": "",
    "linked_to": "startquestionarie25",
    "linked_to_id": "1059"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "estim",
      "value": 4
    }],
    "text": "<0.000000001%",
    "trigger": "",
    "linked_to": "startquestionarie25",
    "linked_to_id": "1059"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1059",
  "textid": "startquestionarie25",
  "name": "Анкета 25",
  "message": "<b>Умеешь ли ты справляться со стрессом?<\/b>",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 0
    }],
    "text": "Не умею, постоянно в стрессе",
    "trigger": "",
    "linked_to": "startquestionarie26",
    "linked_to_id": "1079"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 1
    }],
    "text": "Почти не управляю состоянием",
    "trigger": "",
    "linked_to": "startquestionarie26",
    "linked_to_id": "1079"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 2
    }],
    "text": "Иногда получается справляться",
    "trigger": "",
    "linked_to": "startquestionarie26",
    "linked_to_id": "1079"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 3
    }],
    "text": "Умею, но можно лучше",
    "trigger": "",
    "linked_to": "startquestionarie26",
    "linked_to_id": "1079"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 4
    }],
    "text": "Отлично справляюсь",
    "trigger": "",
    "linked_to": "startquestionarie26",
    "linked_to_id": "1079"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1079",
  "textid": "startquestionarie26",
  "name": "Анкета 26",
  "message": "Доставляет ли тебе дискомфорт ситуация, когда кто-то о тебе плохо думает?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 0
    }],
    "text": "Выбивает из колеи",
    "trigger": "",
    "linked_to": "startquestionarie32",
    "linked_to_id": "1550"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 1
    }],
    "text": "Переживаю!",
    "trigger": "",
    "linked_to": "startquestionarie32",
    "linked_to_id": "1550"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 2
    }],
    "text": "Иногда спокойно отношусь, иногда нет",
    "trigger": "",
    "linked_to": "startquestionarie32",
    "linked_to_id": "1550"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 3
    }],
    "text": "Почти всегда мне норм",
    "trigger": "",
    "linked_to": "startquestionarie32",
    "linked_to_id": "1550"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 4
    }],
    "text": "Не влияет на мой комфорт",
    "trigger": "",
    "linked_to": "startquestionarie32",
    "linked_to_id": "1550"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1080",
  "textid": "startquestionarie27",
  "name": "Анкета 27",
  "message": "Понятно! Я чуть больше знаю о тебе :)\r\n\r\nВ игре будет предоставлена информация о том, как можно анализировать и решать разные проблемы. Эти рекомендации не являются обязательным руководством к действию – решение, как ими распорядиться принимает сам игрок, и он же несет ответственность за риски и любые последствия от принятых решений. \r\nИгра не является психологическим, психотерапевтическим, коучинговым, медицинским или иным видом профессиональной помощи. Если ведущие игры поймут, что решение задачи игрока требует получение профессиональной помощи, то они сообщат об этом игроку.  \r\nЦель игры – достижение положительных результатов в решении актуальных для игроков задач, однако этот процесс зависит от множества факторов, в том числе не связанных с игрой, поэтому гарантий достижения результата к моменту окончания игры нет. \r\n\r\nДанные о содержании и ходе выполнения заданий защищены конфиденциальностью и доступны только ведущим. Без согласия игроков они не могут разглашаться, за исключением случаев, когда создаются ситуации угрозы физической безопасности игроков или лиц, с ними связанных.\r\n\r\nНапиши \"понятно\", если дочитал_а до этого места :) ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1081",
  "textid": "startquestionarie28",
  "name": "Анкета 28",
  "message": "Это значит, что в течение 4 недель, пока идёт игра, игроки буду выполнять все задания, которые выдадут организаторы, используя их инструкции (если они не противоречат закону и здравому смыслу). \r\nНа время игры игроки не будут подвергать сомнениям и критике действия организаторов, дожидаясь финального результата. Любые замечания и предложения, которые появятся у игроков они смогут высказать во время сбора обратной связи.\r\n\r\nНапиши \"окей\", если дочитал_а до этого места!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1082",
  "textid": "startquestionarie29",
  "name": "Анкета 29",
  "message": "Если ты играешь в первый раз и игра не соответствует твоим ожиданиям, то мы готовы вернуть деньги за билет в любой момент. \r\n\r\nЧтобы подобного не случилось с другими людьми, мы попросим тебя подробно рассказать про ожидания и реальный игровой опыт. Если по независящим от тебя обстоятельствам у тебя не получилось принять участие в игре, то попроси перенести твой билет в следующие запуски. При этом организаторы оставляют за собой право отказать в возмещении или возврате стоимости билета без объяснения причин. Но постараются так не делать:) По всем таким вопросам пиши Антону Зельдину – @anton_zeldin в телеграмме.\r\n\r\nВ остальных случаях, стоимость билета не возвращается после заполнения данной анкеты.\r\n\r\nНапиши \"хорошо\", если дочитал_а до этого места.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1088",
  "textid": "startquestionarie30",
  "name": "Анкета 30",
  "message": "Спасибо за ответы! Теперь можно переходить к первому заданию :)",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Поехали!",
    "trigger": "",
    "linked_to": "problem",
    "linked_to_id": "105"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1108",
  "textid": "SexSurveyStartTest",
  "name": "Тестовый секс-опрос. Начало",
  "message": "Уважаемый участник! Предлагаем проверить ваши знания о медицинских, юридических и физиологических аспектах секса и сексуальности. Вы сразу сможете увидеть результаты по каждому вопросу и оценить свою осведомленность. Этот опрос анонимный, поэтому просим отвечать так, как вы думаете. Ваши анкетные данные защищены конфиденциальностью и не попадут в открытый доступ.\r\nВам будут предложены 6 блоков вопросов с вариантами ответов. Ваша задача выбрать тот, с которым вы согласны. Общее время, которое вам потребуется - 10 минут.\r\nБлагодарим за участие!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Начнём!",
    "trigger": "",
    "linked_to": "SexSurveyQ1",
    "linked_to_id": "1109"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1109",
  "textid": "SexSurveyQ1",
  "name": "Тестовый секс-опрос. Вопрос №1",
  "message": "1. Гендерная идентичность",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "мужская"
    }],
    "text": "мужская",
    "trigger": "",
    "linked_to": "SexSurveyQ2",
    "linked_to_id": "1111"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "женская"
    }],
    "text": "женская",
    "trigger": "",
    "linked_to": "SexSurveyQ2",
    "linked_to_id": "1111"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "небинарная"
    }],
    "text": "небинарная",
    "trigger": "",
    "linked_to": "SexSurveyQ2",
    "linked_to_id": "1111"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1111",
  "textid": "SexSurveyQ2",
  "name": "Тестовый секс-опрос. Вопрос №2",
  "message": "Укажите свой возраст (полных лет): ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1112",
  "textid": "SexSurveyQ3",
  "name": "Тестовый секс-опрос. Вопрос №3",
  "message": "Укажите уровень полученного вами образования:",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "основное_общее_9_классов"
    }],
    "text": "9 классов",
    "trigger": "",
    "linked_to": "SexSurveyQ4",
    "linked_to_id": "1113"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "среднее_полное_общее_11_классов"
    }],
    "text": "11 классов",
    "trigger": "",
    "linked_to": "SexSurveyQ4",
    "linked_to_id": "1113"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "среднее_профессиональное_ПТУ_колледж_техникум"
    }],
    "text": "среднее профессиональное",
    "trigger": "",
    "linked_to": "SexSurveyQ4",
    "linked_to_id": "1113"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "студент"
    }],
    "text": "студент",
    "trigger": "",
    "linked_to": "SexSurveyQ4",
    "linked_to_id": "1113"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "высшее"
    }],
    "text": "высшее",
    "trigger": "",
    "linked_to": "SexSurveyQ4",
    "linked_to_id": "1113"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "ученая_степень"
    }],
    "text": "ученая степень",
    "trigger": "",
    "linked_to": "SexSurveyQ4",
    "linked_to_id": "1113"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1113",
  "textid": "SexSurveyQ4",
  "name": "Тестовый секс-опрос. Вопрос №4",
  "message": "В каких отношениях вы состоите на текущий момент?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "не состою в отношениях"
    }],
    "text": "не состою в отношениях",
    "trigger": "",
    "linked_to": "SexSurveyQ5",
    "linked_to_id": "1114"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "в моногамных (один партнер_ка)"
    }],
    "text": "в моногамных (один партнер_ка)",
    "trigger": "",
    "linked_to": "SexSurveyQ5",
    "linked_to_id": "1114"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "в открыто немоногамных"
    }],
    "text": "в открыто немоногамных ",
    "trigger": "",
    "linked_to": "SexSurveyQ5",
    "linked_to_id": "1114"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "другое"
    }],
    "text": "другое",
    "trigger": "",
    "linked_to": "SexSurveyQ4s1",
    "linked_to_id": "1148"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1114",
  "textid": "SexSurveyQ5",
  "name": "Тестовый секс-опрос. Вопрос №5",
  "message": "Укажите свой родительский статус:",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "не имею детей"
    }],
    "text": "не имею детей",
    "trigger": "",
    "linked_to": "SexSurveyQ6",
    "linked_to_id": "1115"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "воспитываю детей"
    }],
    "text": "воспитываю детей",
    "trigger": "",
    "linked_to": "SexSurveyQ5s1",
    "linked_to_id": "1150"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "имею взрослых детей"
    }],
    "text": "имею взрослых детей",
    "trigger": "",
    "linked_to": "SexSurveyQ6",
    "linked_to_id": "1115"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1115",
  "textid": "SexSurveyQ6",
  "name": "Тестовый секс-опрос. Вопрос №6",
  "message": "Вероисповедание",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1117",
  "textid": "SexSurveyQ7",
  "name": "Тестовый секс-опрос. Вопрос №7",
  "message": "Наличие братьев-сестер, очередность рождения",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1118",
  "textid": "SexSurveyQ8",
  "name": "Тестовый секс-опрос. Вопрос №8",
  "message": "Где вы проживали до 18 лет (провели бОльший отрезок жизни от рождения до 18 лет)?\r\nСтрана\r\nНаселенный пункт (город\/деревня\/другое)\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1119",
  "textid": "SexSurveyQ9",
  "name": "Тестовый секс-опрос. Вопрос №9",
  "message": "Где вы проживаете сейчас?\r\nСтрана\r\nНаселенный пункт (город\/деревня\/другое)\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1120",
  "textid": "SexSurveyQ10",
  "name": "Тестовый секс-опрос. Вопрос №10",
  "message": "Где вы воспитывались бОльший отрезок жизни от рождения до 18 лет",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "в полной семье"
    }],
    "text": "в полной семье",
    "trigger": "",
    "linked_to": "SexSurveyQ11",
    "linked_to_id": "1121"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "в неполной семье"
    }],
    "text": "в неполной семье",
    "trigger": "",
    "linked_to": "SexSurveyQ11",
    "linked_to_id": "1121"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "в государственном детском доме\/интернате"
    }],
    "text": "в государственном детском доме\/интернате",
    "trigger": "",
    "linked_to": "SexSurveyQ11",
    "linked_to_id": "1121"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "в частном\/семейном детском доме"
    }],
    "text": "в частном\/семейном детском доме",
    "trigger": "",
    "linked_to": "SexSurveyQ11",
    "linked_to_id": "1121"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1121",
  "textid": "SexSurveyQ11",
  "name": "Тестовый секс-опрос. Вопрос №11",
  "message": "Национальная идентичность",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1122",
  "textid": "SexSurveyQ12",
  "name": "Тестовый секс-опрос. Вопрос №12",
  "message": "Область профессиональной деятельности\r\n\r\nтут будут варианты ответов, но их слишком много сейчас и поэтому их нет)",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": " survey",
    "trigger": "",
    "linked_to": "SexSurveyStartTest",
    "linked_to_id": "1108"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1148",
  "textid": "SexSurveyQ4s1",
  "name": "Тестовый секс-опрос. Вопрос №4, другое",
  "message": "Напишите, что именно:",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1150",
  "textid": "SexSurveyQ5s1",
  "name": "Тестовый секс-опрос. Вопрос №5, воспитываю детей",
  "message": "Укажите, количество детей",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1152",
  "textid": "SexSurveyQ5s2",
  "name": "Тестовый секс-опрос. Вопрос №5, имею взрослых детей",
  "message": "Укажите возраст детей через ;",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1167",
  "textid": "aftermar1",
  "name": "После марафона 1",
  "message": "Привет, зарешатель! \r\n\r\nУ тебя есть пройденный первый шаг, ты поставил_а цели и определил_ась с тем, какие сферы твоей жизни нуждаются в <i>зарешании<\/i>.\r\n\r\nНо мы не нашли тебя в игре!\r\nПоотмечай, пожалуйста, почему? \r\n\r\nСейчас я тебя немножко поспрашиваю, будет всего несколько вопросов и займет буквально минуту. Ок?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Ок!",
    "trigger": "",
    "linked_to": "aftermar2",
    "linked_to_id": "1175"
  }, {
    "on_click": [],
    "text": "Нет, не ок",
    "trigger": "",
    "linked_to": "aftermar19",
    "linked_to_id": "1202"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1169",
  "textid": "aftermar3",
  "name": "После марафона 3",
  "message": "Зарешай показался тебе полезным (в целом)?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да",
    "trigger": "",
    "linked_to": "aftermar5",
    "linked_to_id": "1171"
  }, {
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "aftermar6",
    "linked_to_id": "1172"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1170",
  "textid": "aftermar4",
  "name": "После марафона 4",
  "message": "Тебе рассказать побольше?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, расскажите!",
    "trigger": "",
    "linked_to": "aftermar9",
    "linked_to_id": "1192"
  }, {
    "on_click": [],
    "text": "Нет, неинтересно",
    "trigger": "",
    "linked_to": "aftermar10",
    "linked_to_id": "1193"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1171",
  "textid": "aftermar5",
  "name": "После марафона 5",
  "message": "Расскажи, почему не играешь?\r\n\r\n1. Слишком дорого\r\n2. По итогам марафона нет проблем, требующих решения\r\n3. У меня нет времени\r\n4. Я поучаствую, но позже\r\n5. Мне надо сначала обработать для себя марафон\r\n6. Другое",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дорого",
    "trigger": "",
    "linked_to": "aftermar7",
    "linked_to_id": "1173"
  }, {
    "on_click": [],
    "text": "Нет проблемы",
    "trigger": "",
    "linked_to": "aftermar11",
    "linked_to_id": "1194"
  }, {
    "on_click": [],
    "text": "Нет времени",
    "trigger": "",
    "linked_to": "aftermar16",
    "linked_to_id": "1199"
  }, {
    "on_click": [],
    "text": "Поучаствую позже",
    "trigger": "",
    "linked_to": "aftermar17",
    "linked_to_id": "1200"
  }, {
    "on_click": [],
    "text": "Надо осмыслить",
    "trigger": "",
    "linked_to": "aftermar18",
    "linked_to_id": "1201"
  }, {
    "on_click": [],
    "text": "Другое",
    "trigger": "",
    "linked_to": "aftermar20",
    "linked_to_id": "1203"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1172",
  "textid": "aftermar6",
  "name": "После марафона 6",
  "message": "Можешь указать, почему?\r\n\r\n1. По итогам марафона проблем нет\r\n2. Марафон не оправдал ожиданий\r\n3. Марафон не зацепил эмоционально\r\n4. Кажется, бот недопилен\r\n5. Я не верю в онлайн-консультации",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Нет проблемы",
    "trigger": "",
    "linked_to": "aftermar11",
    "linked_to_id": "1194"
  }, {
    "on_click": [],
    "text": "Ожидания разошлись",
    "trigger": "",
    "linked_to": "aftermar12",
    "linked_to_id": "1195"
  }, {
    "on_click": [],
    "text": "Марафон не зацепил",
    "trigger": "",
    "linked_to": "aftermar13",
    "linked_to_id": "1196"
  }, {
    "on_click": [],
    "text": "Кажется, бот недопилен",
    "trigger": "",
    "linked_to": "aftermar14",
    "linked_to_id": "1197"
  }, {
    "on_click": [],
    "text": "Я не верю в онлайн-консультации",
    "trigger": "",
    "linked_to": "aftermar15",
    "linked_to_id": "1198"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1173",
  "textid": "aftermar7",
  "name": "После марафона 7",
  "message": "А сколько было бы комфортно?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1174",
  "textid": "aftermar8",
  "name": "После марафона 8",
  "message": "Спасибо за ответ! Если хочешь поучаствовать как стипендиат_ка, жми на кнопку.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Хочу быть золушкой",
    "trigger": "",
    "linked_to": "cinderella",
    "linked_to_id": "702"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1175",
  "textid": "aftermar2",
  "name": "После марафона 2",
  "message": "Ты понял_а, что такое Зарешай?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, игра чтобы решить проблемы",
    "trigger": "",
    "linked_to": "aftermar3",
    "linked_to_id": "1169"
  }, {
    "on_click": [],
    "text": "Нет, что вообще?",
    "trigger": "",
    "linked_to": "aftermar4",
    "linked_to_id": "1170"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1192",
  "textid": "aftermar9",
  "name": "После марафона 9",
  "message": "Однажды коуч по рациональности, исследователь психотерапии и проектный менеджер (зашли в бар - зачеркнуто) собрались вместе и стали мечтать о том, как можно улучшить мир. Что было бы здорово, если бы каждый человек мог в любой момент воспользоваться их компетенциями в своей обычной жизни с обычными делами и задачами. Из этой мечты и появился бот Зарешай.\r\n\r\nБилеты тут:\r\nzaresh.ai",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1193",
  "textid": "aftermar10",
  "name": "После марафона 10",
  "message": "Понятно, жаль. Если что, пиши :) ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1194",
  "textid": "aftermar11",
  "name": "После марафона 11",
  "message": "Круто! Спасибо за ответ.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1195",
  "textid": "aftermar12",
  "name": "После марафона 12",
  "message": "Жаль это слышать. Расскажи, что ожидал_а и что пошло не так?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1196",
  "textid": "aftermar13",
  "name": "После марафона 13",
  "message": "Расскажи, что могло бы зацепить?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1197",
  "textid": "aftermar14",
  "name": "После марафона 14",
  "message": "И правда, запуск был тестовый! Мы нашли довольно много ошибок и починили их. \r\n\r\nА какие ошибки увидел_а ты?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1198",
  "textid": "aftermar15",
  "name": "После марафона 15",
  "message": "Понятно, спасибо. Если будет минутка, разверни, почему?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1199",
  "textid": "aftermar16",
  "name": "После марафона 16",
  "message": "Спасибо за ответ! Мы стараемся сделать так, чтобы игра занимала у тебя не более получаса в день в среднем. Если надумаешь — заходи :)",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1200",
  "textid": "aftermar17",
  "name": "После марафона 17",
  "message": "Отлично, будем тебя ждать! Билеты на zaresh.ai",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1201",
  "textid": "aftermar18",
  "name": "После марафона 18",
  "message": "Понимаю! Пиши, как надумаешь реализовывать планы и если потребуется помощь?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1202",
  "textid": "aftermar19",
  "name": "После марафона 19",
  "message": "*передразнивает* неть не окь",
  "turned_on": true,
  "buttons": [],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/дени.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1203",
  "textid": "aftermar20",
  "name": "После марафона 20",
  "message": "Расскажи, что?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1272",
  "textid": "7december19",
  "name": "Старт 7 декабря 19",
  "message": "Отлично, я тебя вижу!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я играю в первый раз",
    "trigger": "",
    "linked_to": "startquestionarie",
    "linked_to_id": "1009"
  }, {
    "on_click": [],
    "text": "Я уже играл_а",
    "trigger": "",
    "linked_to": "snova",
    "linked_to_id": "435"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1273",
  "textid": "newyearwelcome",
  "name": "Привет новый год",
  "message": "Привет! Я Зарешай-бот, я провожу марафон \"Завершай 2k19\". \r\n\r\nПервым делом, давайте выясним, как этот марафон может вам помочь. Для этого предлагаю ответить на пять простых вопросов теста удовлетворенности жизнью Динера. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Начать тест",
    "trigger": "",
    "linked_to": "diener1",
    "linked_to_id": "1274"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1274",
  "textid": "diener1",
  "name": "Динер1",
  "message": "Насколько вы согласны (1-7) с утверждением: \r\n\r\n<b>Почти во всем моя жизнь соответствует моему идеалу<\/b>",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1275",
  "textid": "diener2",
  "name": "Динер2",
  "message": "Насколько вы согласны (1-7) с утверждением: \r\n\r\n<b>Условия моей жизни превосходные<\/b>",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1276",
  "textid": "diener3",
  "name": "",
  "message": "Насколько вы согласны (1-7) с утверждением: \r\n\r\n<b>Я удовлетворен_а своей жизнью<\/b>",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1277",
  "textid": "diener4",
  "name": "",
  "message": "Насколько вы согласны (1-7) с утверждением: \r\n\r\n<b>В главном я пока достигал_а всего, чего хотел_а в жизни<\/b>",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1278",
  "textid": "diener5",
  "name": "",
  "message": "Насколько вы согласны (1-7) с утверждением: \r\n\r\n<b>Если бы я мог_ла прожить свою жизнь еще раз, я бы почти ничего в ней не изменил_а<\/b>\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1279",
  "textid": "diener6",
  "name": "",
  "message": "Посчитайте баллы. Сколько?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "7-13",
    "trigger": "",
    "linked_to": "dienerend1",
    "linked_to_id": "1280"
  }, {
    "on_click": [],
    "text": "14-20",
    "trigger": "",
    "linked_to": "dienerend2",
    "linked_to_id": "1281"
  }, {
    "on_click": [],
    "text": "21-28",
    "trigger": "",
    "linked_to": "dienerend3",
    "linked_to_id": "1282"
  }, {
    "on_click": [],
    "text": "29-35",
    "trigger": "",
    "linked_to": "dienerend4",
    "linked_to_id": "1283"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1280",
  "textid": "dienerend1",
  "name": "",
  "message": "Этот балл отражает низкий уровень удовлетворенностью жизнью. Чаще всего это связано с серьезными и затяжными жизненными проблемами (например, со здоровьем - своим или близких людей, длительными и трудными ситуациями на работе или в отношениях). Возможно, справиться с такими ситуациями будет проще, обратившись за помощью других людей, в том числе специалистов. Марафон \"Завершай 2k19\" поможет высветить ключевые трудности и определить \"мишени\", с которых стоит начать менять свою жизнь к лучшему. Присоединяйтесь!\r\n\r\nДля этого напишите свое имя и \"хочу присоединиться\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "newyearlanding",
    "linked_to_id": "1302"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1281",
  "textid": "dienerend2",
  "name": "",
  "message": "Ваш уровень удовлетворенности жизнью немного ниже среднего или приближается к среднему. Обычно в таких случаях люди могут испытывать неудовлетворенность в отдельных областях жизни, а другими бывают вполне довольны, либо в текущей жизни есть какая-то конкретная проблема, которая не дает покоя. К счастью, практически для любой проблемы есть решение, если правильно проанализировать ее истоки и то, как она \"работает\". Можно сделать это самому, а можно обратиться за помощью к другим людям, в том числе к специалистам. Специально под эту задачу мы сделали марафон \"Завершай 2k19\". Во время марафона в вашем распоряжении будут лучшие, научно-обоснованные инструменты, чтобы провести такой анализ и наметить пути решения. Присоединяйтесь!\r\n\r\nДля этого напишите свое имя и \"хочу присоединиться\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "newyearlanding",
    "linked_to_id": "1302"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1282",
  "textid": "dienerend3",
  "name": "",
  "message": "У вас средний или чуть выше среднего уровень удовлетворенностью жизнью. Человек с такими баллами в целом доволен своей жизнью. При этом могут существовать такие сферы, в которых хочется значительных улучшений. Качественный анализ того, как вы строите свою жизнь в течение года, поможет сфокусироваться на главном и совершить такой прорыв. Присоединяйтесь к марафону! \r\n\r\nНапишите свое имя и \"хочу присоединиться\".",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "newyearlanding",
    "linked_to_id": "1302"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1283",
  "textid": "dienerend4",
  "name": "",
  "message": "У вас высокий уровень удовлетворенности жизнью. Впрочем, возможно, у вас есть какие-то сферы жизни, в которых хотелось бы большего и лучшего. Это абсолютно нормально и может вдохновлять к развитию и совершенствованию. Подведите итоги с ботом-экспертом и выявите самые эффективные стратегии, которые можно взять с собой в наступающий год. Присоединяйтесь к марафону!\r\n\r\nНапишите свое имя и \"хочу присоединиться\".\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "newyearlanding",
    "linked_to_id": "1302"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1302",
  "textid": "newyearlanding",
  "name": "Ссылка на новогодний марафон",
  "message": "Билеты тут: \r\nhttps:\/\/zaresh.ai\/zavershai",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1309",
  "textid": "synchro_day",
  "name": "Ежедневный митинг",
  "message": "Расскажи, что делал вчера?\r\nЧто планируешь делать сегодня?\r\nКакие есть затруднения?\r\nКто из команды нужен для совместных задач?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1408",
  "textid": "marathontask81",
  "name": "Задание марафон 81",
  "message": "Посмотри на проблему, которую ты сформулировал_а вчера и ответь на вопрос: <b>\"Какого результатата я хотел_а бы достичь, когда решу свою проблему?\".<\/b> Пусть твой ответ начинается со слов \"Я хочу...\" или \"Хорошим результатом я буду считать...\".\r\n\r\nЗатем <b>опиши<\/b>, к каким изменениям в твоей личности и в жизни в целом приведет достигнутая цель. Насколько значимыми будут эти изменения для тебя?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1413",
  "textid": "marathontask901",
  "name": "Задание марафон 901",
  "message": "<b>Ответь на вопрос:<\/b>\r\nКак я пойму, что достиг_ла желаемого результата? Постарайся описать конкретные признаки изменений, которые произойдут, когда ты получишь то, что хочешь. Это могут быть изменения в твоем <b>поведении, эмоциональном состоянии, каких-то конкретных переменах в социальном статусе<\/b>, стиле общения и пр.\r\n\r\nБудет здорово, если эти признаки сможешь непосредственно наблюдать не только ты, но и твое окружение. Иными словами, если бы ты передал_а это описание сторонним людям, они могли бы отметить изменения извне, даже не зная, что твоя цель достигнута. \r\n\r\nОпиши <b>образ результата<\/b> в небольшом эссе, подбери картинки, представь, пришли мне.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "",
    "trigger": "response",
    "linked_to": "marathontask91",
    "linked_to_id": "983"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1417",
  "textid": "marathonquestionaruifin12",
  "name": "финишная анкета марафона",
  "message": "Что было совсем иначе, что совпало? Какие у тебя мысли\/ощущения по этому поводу?\r\n",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1433",
  "textid": "marathonquestionaruifin9",
  "name": "Финишная анкета марафона 9",
  "message": "Свободный микрофон\r\nНапиши всё, что приходит в голову! Можно про цепочку, можно про зарешай,, про жизнь или любую случайную мысль",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1465",
  "textid": "finalquest1",
  "name": "",
  "message": "Какая у тебя была цель в этом запуске?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1466",
  "textid": "finalquest2",
  "name": "",
  "message": "Так. А что удалось сделать по факту? Какой твой лучший результат за игру?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1467",
  "textid": "finalquest3",
  "name": "",
  "message": "Это значит сколько процентов от цели выполнено?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1468",
  "textid": "finalquest4",
  "name": "",
  "message": "Оок. Перечисли 2-3 пункта, которые помогали тебе двигаться в сторону цели",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1469",
  "textid": "finalquest5",
  "name": "",
  "message": "Понятно. А теперь еще 2-3 пункта, которые, наоборот, мешали тебе двигаться в сторону цели",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1470",
  "textid": "finalquest6",
  "name": "",
  "message": "С учетом этого что собираешься сделать иначе в следующий раз? И почему, на твой взгляд, это изменит ситуацию к лучшему?\r\n\r\nВообще, конечно, ответить \"ничего\" тоже будет нормально, поэтому не стесняйся :) ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1471",
  "textid": "finalquest7",
  "name": "",
  "message": "Понятно. Узнал_а ли ты что-то новое о себе или мире за время игры? Что именно?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1482",
  "textid": "finalquest8",
  "name": "",
  "message": "Расскажи мне теперь, а я передам людям, как общее впечатление от игры?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1483",
  "textid": "finalquest9",
  "name": "",
  "message": "Оцени полезность игры для тебя? 1-10",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1484",
  "textid": "finalquest10",
  "name": "",
  "message": "Оцени приятность игры для тебя? 1-10",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1485",
  "textid": "finalquest11",
  "name": "",
  "message": "Что больше всего понравилось за время игры?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1486",
  "textid": "finalquest12",
  "name": "",
  "message": "Что меньше всего понравилось за время игры?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1487",
  "textid": "finalquest13",
  "name": "",
  "message": "Чего тебе не хватило во время игры?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1488",
  "textid": "finalquest14",
  "name": "",
  "message": "Что было особенно полезным?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1489",
  "textid": "finalquest15",
  "name": "",
  "message": "Была ли польза от других участников?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1490",
  "textid": "finalquest16",
  "name": "",
  "message": "Что можно сделать, чтобы полнее использовать ресурс группового взаимодействия? Если есть соображения на этот счет, поделись!",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1491",
  "textid": "finalquest17",
  "name": "",
  "message": "Как бы ты описал_а третьим людям, что такое \"Зарешай\"?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1492",
  "textid": "finalquest18",
  "name": "",
  "message": "Насколько оправдались твои ожидания от игры? 1-10",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1493",
  "textid": "finalquest19",
  "name": "",
  "message": "Насколько вероятно порекомендуешь игру друзьям? 1-10",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1494",
  "textid": "finalquest20",
  "name": "",
  "message": "Как по твоему, насколько полезность игры соответствует ее цене?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "takemoney",
      "value": 1
    }],
    "text": "Полезность выше цены",
    "trigger": "",
    "linked_to": "finalquest21",
    "linked_to_id": "1495"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "takemoney",
      "value": 0
    }],
    "text": "Полезность равна цене",
    "trigger": "",
    "linked_to": "finalquest21",
    "linked_to_id": "1495"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "takemoney",
      "value": -1
    }],
    "text": "Полезность ниже цены",
    "trigger": "",
    "linked_to": "finalquest21",
    "linked_to_id": "1495"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1495",
  "textid": "finalquest21",
  "name": "",
  "message": "Хочешь принять участие в следующем запуске?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Возможно",
    "trigger": "",
    "linked_to": "finalquest22",
    "linked_to_id": "1496"
  }, {
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "finalquest221",
    "linked_to_id": "1522"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1496",
  "textid": "finalquest22",
  "name": "",
  "message": "Заходи на https:\/\/zaresh.ai\/snovazareshat\r\n\r\nА еще расскажи мне что угодно, что не вошло в вопросы, которые я тебе задал, если хочешь. ",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1497",
  "textid": "finalquest23",
  "name": "",
  "message": "Можно опубликовать твой отзыв со ссылкой на тебя? Или лучше без ссылки? Или вообще не публиковать?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1498",
  "textid": "finalquest24",
  "name": "",
  "message": "И, как и обещал, спрошу: насколько ты хочешь помогать миру теперь? 1-10",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1499",
  "textid": "finalquest25",
  "name": "",
  "message": "Заходи в чат выпускников https:\/\/t.me\/joinchat\/CHlYyUfAlk5rN86uqPBiAg3",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1522",
  "textid": "finalquest221",
  "name": "",
  "message": "Расскажешь, почему? Просто ответь на это сообщение.",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1523",
  "textid": "startquestionarie31",
  "name": "Анкета 31",
  "message": "Ну и напоследок внезапный вопрос. Насколько ты считаешь важной для себя ценностью помощь миру? 1-10",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1550",
  "textid": "startquestionarie32",
  "name": "Анкета 32",
  "message": "Как ты смотришь на себя в моменты, когда результат твоих действий оказывается ниже твоих ожиданий?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 0
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 0
    }],
    "text": "Сильно себя ругаю",
    "trigger": "",
    "linked_to": "startquestionarie33",
    "linked_to_id": "1553"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 1
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 1
    }],
    "text": "Немного расстраиваюсь",
    "trigger": "",
    "linked_to": "startquestionarie33",
    "linked_to_id": "1553"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 2
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 2
    }],
    "text": "Ничего, с кем не бывает!",
    "trigger": "",
    "linked_to": "startquestionarie33",
    "linked_to_id": "1553"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 3
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 3
    }],
    "text": "Не унываю по возможности",
    "trigger": "",
    "linked_to": "startquestionarie33",
    "linked_to_id": "1553"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 4
    }, {
      "type": "update_user_progress",
      "target": "selfed",
      "value": 4
    }],
    "text": "Поддерживаю себя",
    "trigger": "",
    "linked_to": "startquestionarie33",
    "linked_to_id": "1553"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1553",
  "textid": "startquestionarie33",
  "name": "Анкета 33",
  "message": "Насколько ты понимаешь свои потребности?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 0
    }],
    "text": "Не особо понимаю эту штуку с потребностями",
    "trigger": "",
    "linked_to": "startquestionarie34",
    "linked_to_id": "1554"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 1
    }],
    "text": "Мне сложно определить их",
    "trigger": "",
    "linked_to": "startquestionarie34",
    "linked_to_id": "1554"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 2
    }],
    "text": "Вроде ощущаю, но выразить не могу",
    "trigger": "",
    "linked_to": "startquestionarie34",
    "linked_to_id": "1554"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 3
    }],
    "text": "Почти всегда могу выразить",
    "trigger": "",
    "linked_to": "startquestionarie34",
    "linked_to_id": "1554"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "aware",
      "value": 4
    }],
    "text": "Отлично понимаю и легко выражаю словами",
    "trigger": "",
    "linked_to": "startquestionarie34",
    "linked_to_id": "1554"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1554",
  "textid": "startquestionarie34",
  "name": "Анкета 34",
  "message": "Что ты делаешь, когда что-то не получается?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "update_user_progress",
      "target": "selfed",
      "value": 0
    }],
    "text": "Расстраиваюсь и бросаю",
    "trigger": "",
    "linked_to": "startquestionarie27",
    "linked_to_id": "1080"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "selfed",
      "value": 1
    }],
    "text": "Расстраиваюсь, но продолжаю",
    "trigger": "",
    "linked_to": "startquestionarie27",
    "linked_to_id": "1080"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "selfed",
      "value": 2
    }],
    "text": "Переживаю, ищу мотивацию",
    "trigger": "",
    "linked_to": "startquestionarie27",
    "linked_to_id": "1080"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "selfed",
      "value": 3
    }],
    "text": "Не обращаю внимания, фигачу",
    "trigger": "",
    "linked_to": "startquestionarie27",
    "linked_to_id": "1080"
  }, {
    "on_click": [{
      "type": "update_user_progress",
      "target": "selfed",
      "value": 4
    }],
    "text": "Учусь на ошибках и иду дальше!",
    "trigger": "",
    "linked_to": "startquestionarie27",
    "linked_to_id": "1080"
  }, {
    "on_click": [],
    "text": "2",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1569",
  "textid": "shabitstart",
  "name": "Спривычка",
  "message": "1\/10\r\n\r\nИтак, сейчас мы будем работать с установкой привычки.\r\n\r\nСуть этой работы в том, чтобы внедрить в жизнь полезную активность, которая позволит тебе лучше заботиться о себе и своём благополучии.\r\n\r\nТебе будут присылаться задания. Некоторые из них простые и потребуют немного времени. Для других потребуется больше времени и сил.\r\n\r\nЧтобы переходить от задания к заданию, нужно будет нажимать кнопку \"дальше\". Настоятельно рекомендуем переходить к следующему заданию только после того, как предыдущее выполненно. \r\n\r\n[для версии с поддержкой] \/Если задание не до конца понятно и возникают вопросы - напиши об этом!\/\r\n\r\nПлан действий такой:\r\n1. Мы выберем, какую именно действие будем вводить в привычку.\r\n2. Убедимся, что это важно и полезно для тебя.\r\n3. Ты определишь в какой момент ты будешь включаться в желаемую деятельность.\r\n4. Определим форму поддержки на период установки привычки, которая тебе подойдёт.\r\n5. Приступим к практике!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit1",
    "linked_to_id": "1571"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1570",
  "textid": "shabit2",
  "name": "Спривычка",
  "message": "3\/10 \r\n\r\nНапиши, какое действие ты хочешь ввести в привычку. #привычка\r\n\r\n<b>Подсказки: <\/b>\r\nДействие должно быть минимальным и легко исполнимым. Маленькое действие вызывает у мозга меньше сопротивления, а там уже и втянешься. \r\nЕсли речь идёт о большом деле - выбери первый и простой шаг дела. Например, \"надеть кроссовки\" для привычки бегать по утрам, а не \"одеваться, выходить на улицу и полчаса бегать\".\r\n\r\n\r\nЕсли речь идёт о чём-то небольшом, например, \"полить цветы\", то вполне подойдёт простое описание \"наполнить лейку водой и полить цветы в гостинной и спальне\".\r\n\r\n\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit3",
    "linked_to_id": "1572"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1571",
  "textid": "shabit1",
  "name": "Спривычка1",
  "message": "2\/10 \r\nПервое и самое главное: если вместо установки привычки того же результата можно добиться автоматизацией\/делегированием\/изменением среды - то подумай, не будет ли это проще.\r\n\r\nНо если ты здесь и хочешь формировать привычку, то жми \"дальше\" :) \r\n\r\n#привычка #теория",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit2",
    "linked_to_id": "1570"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1572",
  "textid": "shabit3",
  "name": "Спривычка3",
  "message": "4\/10\r\n\r\nТеперь давай убедимся, что это важно.\r\nПредставим себе, что у нас есть два мира, нынешний (1) и тот, в котором привычка установлена (2) и посмотрим, чем они отличаются\r\n1. Каким твоим потребностям отвечает второй мир, в котором привычка уже есть? Не спеши, попробуй составить наиболее полный список и выделить в нём важное. Если ты испытываешь сложности с определением потребностей, прочитай вот эту статью: [ссылка на материал про потребности]\r\n2. Какую измеримую пользу приносит установленная привычка в мире-2?\r\n3. Расскажи, почему это важно в бОльшем масштабе. Если представить себе, что у тебя была длинная и благополучная жизнь и ты находишься в глубокой и мудрой  старости и оглядываешься на 2019-й, в котором у тебя есть привычка - в чём ценность этого?  ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit4",
    "linked_to_id": "1573"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1573",
  "textid": "shabit4",
  "name": "Спривычка4",
  "message": "5\/10\r\n\r\nИтак, у тебя есть ответы на все три вопроса.\r\nТы знаешь, какие твои потребности будут удовлетворены, знаешь в чём измеримая польза, а так же какой смысл это имеет в масштабе жизни в целом.\r\n\r\nНасколько важным видится тебе установка привычки сейчас по шкале от 1 до 10?\r\n\r\n1-5 -> вопрос, стоит ли оно того и если стоит - поехали дальше.\r\n6-10 -> отлично, поехали дальше.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit5",
    "linked_to_id": "1574"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1574",
  "textid": "shabit5",
  "name": "Спривычка5",
  "message": "6\/10\r\n\r\nОтлично. Мы поговорили о том, почему это важно.\r\nСледующий шаг, убедиться в том, что у желаемой привычки нет значимых издержек. \r\nЕсли станется так, что есть - не переживай, мы подумаем о том, что с этим сделать!\r\n\r\nИтак, поехали. Сперва давай попробуем оценить риски аналитически.\r\n1. Заполни квадрат Декарта. Нужно ответить на четыре вопроса\r\n- Что будет, если эта привычка будет установлена\r\n- Чего не будет, если эта привычка будет установлена\r\n- Что будет, если эта привычка не будет установлена\r\n- Чего не будет, если эта привычка не будет установлена\r\nНе спеши! Продуманные ответы помогут тебе а) Лучше понять, с чем тебе придётся столкнуться и избежать неожиданных последствий и увидеть источники возможного сопротивления; б) Ещё раз увидеть все плюсы и получить больше внутренней мотивации.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit6",
    "linked_to_id": "1575"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1575",
  "textid": "shabit6",
  "name": "Спривычка6",
  "message": "7\/10\r\n\r\nИдём дальше. \r\nТеперь попробуем посмотреть на ситуацию обращаясь за помощью к ощущениям и чувствам.\r\n2. Представь себе, что прошёл месяц и ты успешно внедрил_а привычку. \r\nПредставь себе во всех деталях день, в котором эта привычка уже установлена. Попробуй погрузиться в какой-то конкретный момент этого дня и ответить на следующие вопросы:\r\n- Насколько мне уютно в этом? И если есть неуют - то о чём это?\r\n- Есть ли у меня какие-то приятные ощущения от нового состояния?\r\n\r\nЕсли всё хорошо - поехали дальше!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit7",
    "linked_to_id": "1576"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1576",
  "textid": "shabit7",
  "name": "Спривычка7",
  "message": "8\/10\r\n\r\nМы на финишной прямой и готовы начинать практику!\r\n\r\nДавай придумаем, какой может быть триггер. Триггер — это такое событие, которое точно произойдет независимо от тебя, и которое будет сигналом, что пора выполнять действие. Например \"услышать звонок будильника\" (его можно установить заранее на нужное время и выбрать уникальный сигнал для твоей привычки), или \"встать с кровати\". Иногда бывает хорошо сделать триггер типа \"увидеть стакан воды у постели\", но только если ты вообще никогда не забываешь его туда ставить. \r\nИтак, выбирай понятный и точно срабатывающий триггер. \r\n#привычка",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit8",
    "linked_to_id": "1577"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1577",
  "textid": "shabit8",
  "name": "Спривычка8",
  "message": "9\/10\r\n\r\nУ тебя есть связка из триггера и действия, хорошо бы еще изобрести подкрепление за следование привычке. Это может быть что угодно маленькое и приятное тебе. Еще это может быть отложенное подкрепление, например, за хорошее исполнение схемы триггер-действие несколько раз. \r\n\r\nЕсли не получается придумать подкрепление, то спроси в чате!\r\n\r\n#привычка",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit9",
    "linked_to_id": "1578"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1578",
  "textid": "shabit9",
  "name": "Спривычка9",
  "message": "9,5\/10\r\n\r\nИтак, есть полный набор, триггер-действие-подкрепление. \r\n\r\nИногда целевая ситуация повторяется слишком редко, и нужно предпринять специальные усилия, чтобы сделать ее привычной и более частой. Для этого можно спроектировать и сделать десяток повторений одного триггера, чтобы сделать реакцией на него действие, чтобы привычка закрепилась быстрее. \r\nНапример, если ты хочешь вставать по будильнику, то ты можешь поставить будильник на каждые пять минут, раздеться, залезть в постель, а когда будильник зазвонит — сразу встать. Это облегчит следование схеме в условиях, когда тебе будет мешать сонливость — мозг уже привыкнет. \r\n\r\nОсталось практиковать! #задание #привычка\r\nКак только попробуешь дважды, напиши, что получилось, а что нет. ",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "shabit10",
    "linked_to_id": "1579"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1579",
  "textid": "shabit10",
  "name": "Спривычка10",
  "message": "10\/10 \r\n \r\nМы прошли подготовку. остался один шаг перед практикой!\r\nДавай подумаем о дополнительной поддержке.\r\n\r\nТы можешь выбрать два варианта для работы с привычкой.\r\n1) Работать самостоятельно, поискать способы самоподдержки и пользоваться моей поддержкой.\r\n2) То же, но ещё с поддержкой другого участника!\r\n\r\n1) Я хочу общаться с ботом\r\n2) Я готов общаться с ботом и с напарником",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Хочу сам сусам",
    "trigger": "",
    "linked_to": "shabitself",
    "linked_to_id": "1580"
  }, {
    "on_click": [],
    "text": "Социализация и отвага",
    "trigger": "",
    "linked_to": "shabitpart",
    "linked_to_id": "1581"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1580",
  "textid": "shabitself",
  "name": "Спривычка10а",
  "message": "Самоподдержка\r\n\r\n1. Письмо самоподдержки\r\n2. Отчёты боту и поддержка от бота",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "готово!",
    "trigger": "",
    "linked_to": "shabitfin",
    "linked_to_id": "1582"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1581",
  "textid": "shabitpart",
  "name": "Спривычка10б",
  "message": "Напарник\r\n\r\n1. Письмо самоподдержки\r\n2. Написать в чат, что ищешь напарника а) определяете время, когда тебя спрашивают про привычку и успехи\r\nб) Рассказываешь какую привычку устанавливаешь и почему это важно\r\nв) какая-нибудь ещё активность",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1582",
  "textid": "shabitfin",
  "name": "Спривычка11",
  "message": "Отлично!\r\nТеперь ближайшую неделю мы концентрируемся на привычке.\r\nПиши каждый вечер о том, как успехи.\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я напишу!",
    "trigger": "",
    "linked_to": "shabitbye",
    "linked_to_id": "1596"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1596",
  "textid": "shabitbye",
  "name": "Cпривычкапока",
  "message": "*бот горд за тебя\"",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1630",
  "textid": "LineFeedback",
  "name": "Оценка полезности цепочки",
  "message": "Расскажи, насколько полезным показалось тебе последнее задание? \r\n\r\nОцени по шкале от 1 до 7, где:\r\n\r\n1 - бесполезно (не имеет отношения к моей проблеме)\r\n7 - очень полезно",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "1"
    }],
    "text": "1",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "2"
    }],
    "text": "2",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "3"
    }],
    "text": "3",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "4"
    }],
    "text": "4",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "5"
    }],
    "text": "5",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "6"
    }],
    "text": "6",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [{
      "type": "capture_survey_answers",
      "target": "1c_al0XsCEauMbUgWuspVaEnMlw7Ho9lYND2ZvXTUQ0Q",
      "value": "7"
    }],
    "text": "7",
    "trigger": "",
    "linked_to": "LineFeedbackThanks",
    "linked_to_id": "1631"
  }, {
    "on_click": [],
    "text": "7",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1631",
  "textid": "LineFeedbackThanks",
  "name": "Благодарность за оценку",
  "message": "Спасибо! \r\nЯ обязательно учту твой ответ. И постараюсь быть более полезным!\r\n\r\nЕсли хочешь что-то ещё сообщить организаторам, просто напиши текстом ниже",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1635",
  "textid": "ContextTest",
  "name": "Игра Контекст",
  "message": "Привет! Я – бот, который поможет тебе поиграть в Контекст. \r\n\r\nЭто игра, которая позволяет интересно пообщаться с другим человеком без неловкости.\r\n\r\nИгра для двух человек. Время: 10-15 мин.\r\n\r\nНажимай \"Дальше\" и я расскажу как именно играть.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "ContextTestDescription",
    "linked_to_id": "1636"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1636",
  "textid": "ContextTestDescription",
  "name": "Игра Контекст. Описание",
  "message": "Найдите себе пару. Поговорите друг с другом в течение 10 минут о чём угодно, но к каждой своей фразе добавляйте «Я говорю это, потому что я…» — и описывайте, что вы сейчас переживаете, чего бы вам хотелось, какую задачу вы сейчас перед собой ставите и почему. \r\n\r\nНажми \"Дальше\", чтобы посмотреть пример",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "ContextTestExample",
    "linked_to_id": "1697"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1640",
  "textid": "Checkgtd",
  "name": "Вход GTD",
  "message": "Эта цепочка поможет тебе обработать содержимое контейнера INBOX.\r\n\r\nВперёд?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Поехали",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Хочу больше узнать про GTD",
    "trigger": "",
    "linked_to": "gtdF1",
    "linked_to_id": "1650"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1641",
  "textid": "checkgtd1",
  "name": "GTD Вопрос_1",
  "message": "Напиши, какую единицу из INBOX ты хочешь обработать и жми дальше.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "checkgtd2",
    "linked_to_id": "1643"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "Checkgtd",
    "linked_to_id": "1640"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1643",
  "textid": "checkgtd2",
  "name": "GTD вопрос_2",
  "message": "Требует ли это каких-то действий от тебя?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да",
    "trigger": "",
    "linked_to": "checkgtd4",
    "linked_to_id": "1653"
  }, {
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "gtdcheck3",
    "linked_to_id": "1644"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1644",
  "textid": "gtdcheck3",
  "name": "GTD_Не требует действий",
  "message": "1. Это не нужно вообще -> выбрасываем!\r\n2. Я хочу сделать что-нибудь с этим как-нибудь потом -> перенесём в контейнер \"Когда-нибудь\"\r\n3. Это не требует действий сейчас, но содержит полезную информацию -> перенесём в контейнер \"Полезная информация\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "1 Удаляем",
    "trigger": "",
    "linked_to": "checkgtd31",
    "linked_to_id": "1645"
  }, {
    "on_click": [],
    "text": "2 Когда-нибудь",
    "trigger": "",
    "linked_to": "checkgtd32",
    "linked_to_id": "1647"
  }, {
    "on_click": [],
    "text": "3 Полезная информация",
    "trigger": "",
    "linked_to": "checkgtd33",
    "linked_to_id": "1648"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd2",
    "linked_to_id": "1643"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1645",
  "textid": "checkgtd31",
  "name": "GTD удаляем",
  "message": "Отлично! Удаляй эту задачу, одной единицей меньше в INBOX так держать!",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Хочу разобрать ещё дело",
    "trigger": "",
    "linked_to": "Checkgtd",
    "linked_to_id": "1640"
  }, {
    "on_click": [],
    "text": "Пока с INBOX хватит!",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "gtdcheck3",
    "linked_to_id": "1644"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1646",
  "textid": "gtdfin",
  "name": "GTD Финал",
  "message": "Отлично! \r\nПомни, всякий раз, когда захочешь разобрать INBOX - пиши мне \/Сheckgtd",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1647",
  "textid": "checkgtd32",
  "name": "В когда-нибудь",
  "message": "Отлично, отложи это в контейнер \"Когда-нибудь\"!\r\nПомни - время от времени на этот контейнер нужно посматривать и проводить ревизию содержимого.\r\n\r\nПосмотрим ещё штуки из INBOX?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да!",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Пока хватит, бот",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "gtdcheck3",
    "linked_to_id": "1644"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1648",
  "textid": "checkgtd33",
  "name": "GTD Полезная информация",
  "message": "Отлично! Перемести это в контейнер \"Полезная информация\". Вполне возможно это пригодится. Не забывай туда заглядывать иногда.\r\n\r\nПосмотрим ещё штуки из INBOX?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "А давай",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Пока хватит ",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "gtdcheck3",
    "linked_to_id": "1644"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1650",
  "textid": "gtdF1",
  "name": "Справка GTD",
  "message": "\"Тут можно будет почитать про GTD\"",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "Checkgtd",
    "linked_to_id": "1640"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1653",
  "textid": "checkgtd4",
  "name": "Требует",
  "message": "Напиши конкретное действие, которое нужно сделать.\r\nНапример\r\n\"Позвонить тому-то\"\r\n\"Поставить чайник\"\r\n\"Накидать список решений такой-то задачи\"\r\nи жми дальше",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "checkgtd5",
    "linked_to_id": "1654"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd2",
    "linked_to_id": "1643"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1654",
  "textid": "checkgtd5",
  "name": "GTD вопрос 3",
  "message": "Скажи, можно ли это сделать менее чем за 2 минуты?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "checkgtd51",
    "linked_to_id": "1656"
  }, {
    "on_click": [],
    "text": "Да",
    "trigger": "",
    "linked_to": "gtdfast",
    "linked_to_id": "1655"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd4",
    "linked_to_id": "1653"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1655",
  "textid": "gtdfast",
  "name": "GTD do this",
  "message": "Сделай это прямо сейчас!\r\n\r\nКогда закончишь - подумай, хочешь ли ты разобрать ещё что-то из INBOX?\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Я сделал и хочу!",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Пока хватит!",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd5",
    "linked_to_id": "1654"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1656",
  "textid": "checkgtd51",
  "name": "GTD вопрос 4",
  "message": "Окей. Скажи, можно ли это перепоручить?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Нет",
    "trigger": "",
    "linked_to": "Checkgtdcase",
    "linked_to_id": "1658"
  }, {
    "on_click": [],
    "text": "Да",
    "trigger": "",
    "linked_to": "checkoutsorce",
    "linked_to_id": "1657"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd5",
    "linked_to_id": "1654"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1657",
  "textid": "checkoutsorce",
  "name": "GTD Перепоручаем",
  "message": "Отлично! \r\nВыбери кому и когда - и перепоручи!\r\nПомни, у тебя будет ещё одна задача после этого: ожидать выполнения действия. Поставь себе напоминание об этом!\r\n\r\nХочешь ли ты ещё что-то разобраться из INBOX?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, хочу!",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Пока хватит, бот",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd51",
    "linked_to_id": "1656"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1658",
  "textid": "Checkgtdcase",
  "name": "GTD есть конкретное дело",
  "message": "Отлично.\r\n\r\nДело срочное и нужно сделать его так скоро, как сможешь или же можно внести это в календарь\/поставить напоминание на конкретное время? \r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Можно отложить",
    "trigger": "",
    "linked_to": "gtdcalendar",
    "linked_to_id": "1659"
  }, {
    "on_click": [],
    "text": "Дело срочное",
    "trigger": "",
    "linked_to": "gtdatt",
    "linked_to_id": "1660"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "checkgtd51",
    "linked_to_id": "1656"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1659",
  "textid": "gtdcalendar",
  "name": "GTD календарь",
  "message": "Отлично. Внеси это в календарь или поставь напоминание прямо на сегодня (если планируешь заняться этим сегодня :)\r\n\r\nСкажи, хочешь ли ты ещё поразбирать INBOX?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Разбираем дальше",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Пока хватит, бот",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "Checkgtdcase",
    "linked_to_id": "1658"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1660",
  "textid": "gtdatt",
  "name": "GTD срочное дело",
  "message": "Хорошо, займись этим сразу, как появиться возможность!\r\n\r\nЕщё есть что-то, что хочешь разобрать из INBOX?",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Да, давай разберём",
    "trigger": "",
    "linked_to": "checkgtd1",
    "linked_to_id": "1641"
  }, {
    "on_click": [],
    "text": "Пока хватит",
    "trigger": "",
    "linked_to": "gtdfin",
    "linked_to_id": "1646"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "Checkgtdcase",
    "linked_to_id": "1658"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1695",
  "textid": "remindergumbo",
  "name": "Ремайндер для Гумбо",
  "message": "Привет! Какие несрочные дела ты сделал вчера и какие планируешь сделать сегодня?",
  "turned_on": true,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1697",
  "textid": "ContextTestExample",
  "name": "Игра Контекст. Пример",
  "message": "A: «Привет! Как ты? Я говорю это, потому что хочу как-то начать разговор и не знаю, что еще сказать».\r\n \r\nБ: «Привет! У меня всё ок. Я говорю это, потому что это стандартный вежливый ответ, и я не уверен, что тебе действительно интересно узнать, как у меня дела».\r\n \r\nA: «Я действительно хочу узнать, что сейчас происходит в твоей жизни и как ты себя чувствуешь. Я говорю это, потому что мне правда интересно.\r\n \r\nБ: «Я чувствую себя немного подавленно. Спасибо, что спросила! Я говорю это, потому что теперь чувствую себя достаточно безопасно и знаю, что тебе действительно интересно услышать ответ».\r\n \r\nA: «Да, я тоже сейчас немного расстроена. Я говорю это, потому что хочу тебя поддержать и потому что я и правда чувствую себя не очень».\r\n\r\nБ: «Кстати, ты слышала, что собираются снимать сериал по “Хранителям”»? Я говорю это, потому что надеюсь нащупать какой-то общий интерес и почувствовать себя на одной волне с тобой»\r\n\r\nА: «Ничего себе! Не слышала. А есть что-то, что тебе хотелось бы изменить в оригинальном комиксе? Я говорю это, потому что опасаюсь, что разговор про кино станет скучным и поверхностным, и поэтому я задаю вопрос больше про тебя, чем про у кого какие любимые актёры».\r\n\r\nНажми \"Дальше\", чтобы перейти непосредственно к игре",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Дальше",
    "trigger": "",
    "linked_to": "ContextTestStepByStep01",
    "linked_to_id": "1698"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1698",
  "textid": "ContextTestStepByStep01",
  "name": "Игра Контекст. Шаг 1",
  "message": "Заведите таймер и играйте в течение 10-15 минут. \r\n\r\nПомните, что когда вы раскрываете контекст, важно говорить о себе, а не о партнере или окружающей обстановке. Придерживайтесь формулы «Я говорю это, потому что я…» Можно отвечать как на обычные фразы, так и на “потому что я”-фразы.\r\n\r\nНажми \"Готово\", когда сработает таймер\r\n",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Готово",
    "trigger": "",
    "linked_to": "ContextTestStepByStep02",
    "linked_to_id": "1702"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1702",
  "textid": "ContextTestStepByStep02",
  "name": "Игра Контекст. Шаг 2",
  "message": "Обменяйтесь впечатлениями с напарником.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Заново",
    "trigger": "",
    "linked_to": "ContextTest",
    "linked_to_id": "1635"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1714",
  "textid": "NYstart",
  "name": "",
  "message": "Привет! Ты участвуешь в марафоне для тех, кто хочет войти в наступающий год осмысленно и с чувством ясности.\r\n\r\nЖизнь иногда будто происходит не с нами: мы едва успеваем отбиваться от проблем, забываем про планы и пытаемся справиться с валом событий.\r\n\r\nА порой (например, в конце года) можно оглядываться на прошедший период и подводить итоги. \r\n\r\nПодводить итоги можно по-разному: можно радоваться успехам (если они были), грустить об упущенных возможностях и пытаться не кусать локти слишком сильно.\r\n\r\nМы предлагаем делать это методично и конструктивно. Наше подведение итогов даст тебе взгляд на год на разных уровнях — от крупного плана до повседневности, и все это - с сохранением здравости взгляда и заботливого отношения к себе.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Как это будет происходить?",
    "trigger": "",
    "linked_to": "NYstart1",
    "linked_to_id": "1715"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/start_2.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1715",
  "textid": "NYstart1",
  "name": "",
  "message": "В течение 10 дней, до 26 декабря, ты получишь 5 заданий (1 в 2 дня).\r\n\r\nДля выполнения каждого понадобится от 15 минут до часа твоего времени.\r\n\r\nЧтобы тебе было удобнее сохранять результаты заданий, мы сделали таблицу: http:\/\/bit.ly\/marathon2k19\r\n\r\nСкопируй её себе.\r\n\r\nКаждое задание будет содержать подробные инструкции и советы по заполнению соответствующих полей таблицы.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Перейти к первому заданию",
    "trigger": "",
    "linked_to": "NY11",
    "linked_to_id": "1716"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NYstart",
    "linked_to_id": "1714"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1716",
  "textid": "NY11",
  "name": "Задание №1. Общее описание",
  "message": "#Задание номер один — это взгляд с высоты птичьего полета — самые крупные события, самые значимые свершения и не-свершения, самые запомнившиеся люди. \r\n\r\nВсё самое-самое, что невозможно не заметить, оглядываясь назад. Что первым приходит в голову, когда ты думаешь о 2019 годе, что ты впишешь крупными буквами в историю своей жизни.\r\n\r\nВажно! \"Самое-самое\" не означает, что тебе должны были дать Нобелевку или медаль за покорение Эвереста. \"Самое-самое\" – отражает значимость события для тебя лично, а не для других. Если в твоей системе координат \"научиться ездить на самокате\" было заветной мечтой детства (или взрослости), принесло много радости, потребовало усилий — смело считай это \"самым-самым\".\r\n\r\nВ течение этих двух дней мы будем заполнять поля Дата, Событие, Период, Люди, Мысли и эмоции, Важные решения и Девиз.\r\n \r\nПусть тебя не пугает большое количество пунктов. Большинство из них заполняется очень легко и быстро. \r\n\r\nНачнём с полей Дата и Событие.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Хорошо, начнем!",
    "trigger": "",
    "linked_to": "NY12",
    "linked_to_id": "1717"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NYstart1",
    "linked_to_id": "1715"
  }],
  "attachments": [{
    "type": "url",
    "id": "https:\/\/audd.io\/zareshai\/images\/day_1.jpg",
    "format": "image"
  }],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1717",
  "textid": "NY12",
  "name": "Задание №1. Поля Событие и Дата",
  "message": "Для начала мы хотим разделить прошедший год на несколько периодов. \r\n\r\nДля этого, оглянись назад и впиши в табличку 3 самых значимых для тебя события года и примерные даты (или хотя бы месяц), когда они произошли.\r\n\r\nПервая дата, которая уже внесена в таблицу – это 1 января 2019 года, а последняя – 16 декабря, дата начала марафона. \r\n\r\nДальше переходим к полю Период.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Переходим",
    "trigger": "",
    "linked_to": "NY13",
    "linked_to_id": "1718"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NY11",
    "linked_to_id": "1716"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": true,
  "available_by_deeplink": true
}, {
  "id": "1718",
  "textid": "NY13",
  "name": "Задание №1. Поле Период",
  "message": "Поле Период тоже заполняется достаточно интуитивно. \r\n\r\nИспользуй значения из столбца Дата, которые ты заполнил_а на предыдущем шаге.\r\n\r\nОставшиеся поля: Люди, Мысли и эмоции, Важные решения и Девиз можно заполнять в любом порядке.",
  "turned_on": true,
  "buttons": [{
    "on_click": [],
    "text": "Люди",
    "trigger": "",
    "linked_to": "NY14",
    "linked_to_id": "1738"
  }, {
    "on_click": [],
    "text": "Мысли и эмоции",
    "trigger": "",
    "linked_to": "NY15",
    "linked_to_id": "1744"
  }, {
    "on_click": [],
    "text": "Важные решения",
    "trigger": "",
    "linked_to": "NY16",
    "linked_to_id": "1745"
  }, {
    "on_click": [],
    "text": "Девиз",
    "trigger": "",
    "linked_to": "NY17",
    "linked_to_id": "1746"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NY12",
    "linked_to_id": "1717"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1738",
  "textid": "NY14",
  "name": "Задание №1. Поле Люди",
  "message": "Значимые люди, которые были рядом (отношения с ними могли быть и плохими, и хорошими. Главное, что люди сыграли важную роль в этот период).\r\n\r\nТы можешь посмотреть свои фотографии или видеозаписи соответствующего периода, чтобы лучше вспомнить, кто был рядом.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NY13",
    "linked_to_id": "1718"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1744",
  "textid": "NY15",
  "name": "Задание №1. Поле Мысли и эмоции",
  "message": "Мысли и эмоции, которые чаще всего сопровождали тебя в этот период.\r\n\r\nИдеи для этого поля можно поискать в личном дневнике или постах у себя на странице.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NY13",
    "linked_to_id": "1718"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1745",
  "textid": "NY16",
  "name": "Задание №1. Поле Важные решения",
  "message": "Решения, которые были приняты или обдумывались. Они могут быть связаны с переживаниями, которые описаны в предыдущем пункте, но могут быть и отдельными.",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NY13",
    "linked_to_id": "1718"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1746",
  "textid": "NY17",
  "name": "Задание №1. Поле Девиз",
  "message": "Как одной короткой фразой — девизом, афоризмом — ты охарактеризовал_а бы этот период жизни?",
  "turned_on": true,
  "buttons": [{
    "on_click": [{
      "type": "add_user_group",
      "target": "NYstart"
    }],
    "text": "Назад",
    "trigger": "",
    "linked_to": "NY13",
    "linked_to_id": "1718"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "2",
  "textid": "bad",
  "name": "Когда всё плохо",
  "message": "Это сообщение выключено. Оно не будет отправляться пользователям. Кнопки, ссылающиеся на него, не будут прикрепляться к сообщениям.\r\n\r\nВыключайте сообщения, которые недоделаны. Это просто: кликаете на переключатель внизу -> открывается редактирование сообщения -> кликаете ещё раз -> сохраняете.",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "5",
  "textid": "good",
  "name": "Когда всё хорошо",
  "message": "Чтобы добавить сообщение, заполните верхнюю правую карточку с названием \"Добавить сообщение\".\r\n\r\nID сообщения - его внутренний идентификатор. Напишите его латиницей, только буквами (большими и маленькими) и цифрами, без других символов.\r\nДелайте ID понятным себе и коллегам: например, переведите на английский название карточки или её суть.\r\nУчтите: редактировать ID будет нельзя. ID сообщений отправляются в статистику, в ней можно будет смотреть на большую картинку взаимодействия пользователей с ботом, изменение активности в зависимости от получения каких-то сообщений, и так далее.\r\nЕсли настолько изменяете суть сообщения, что надо изменить ID, создайте новое сообщение. (Старое отключите. Сейчас сообщения удаляются только вручную, чтобы кто-нибудь случайно всё не поудалял).\r\n\r\nПри добавлении и редактировании кнопки после ввода символа ID будут предлагаться выпадающим списком; если ввести несуществующий ID, всё будет сохранено, но кнопка не покажется (как если бы была выключена); при сохранении будет выведено предупреждение.\r\n\r\nЧтобы удалить кнопку, включите редактирование и сотрите её название и id сообщения, на которое она ссылается.",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "82",
  "textid": "menu",
  "name": "Меню",
  "message": "Что хочешь делать дальше?",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Задать вопрос по организации",
    "trigger": "",
    "linked_to": "OrgQuestion",
    "linked_to_id": "91"
  }, {
    "on_click": [],
    "text": "Задать вопрос по содержанию",
    "trigger": "",
    "linked_to": "GameQuestion",
    "linked_to_id": "94"
  }, {
    "on_click": [],
    "text": "Меня надо подбодрить",
    "trigger": "",
    "linked_to": "SweetMotivation",
    "linked_to_id": "115"
  }, {
    "on_click": [],
    "text": "Сдать задание",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "91",
  "textid": "OrgQuestion",
  "name": "Оргвопросы",
  "message": "Напиши вопрос или жми:",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Напомни даты заданий",
    "trigger": "",
    "linked_to": "dates",
    "linked_to_id": "108"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "108",
  "textid": "dates",
  "name": "Даты заданий",
  "message": "Формулирование цели: 20-25 июля \r\nПланирование: 25 июля-3 августа   \r\n<b>Исполнение: 4-15 августа <\/b>\r\nРефлексия: 16-17 августа",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "112",
  "textid": "internal",
  "name": "Внутренние цели, план",
  "message": "Выбери из предложенных вариантов, к чему относится твоя цель:",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Научиться выстраивать личные границы",
    "trigger": "",
    "linked_to": "boundaries",
    "linked_to_id": "233"
  }, {
    "on_click": [],
    "text": "Меньше зависеть от мнения окружающих",
    "trigger": "",
    "linked_to": "selfEsteem",
    "linked_to_id": "239"
  }, {
    "on_click": [],
    "text": "Лучше понимать свои потребности",
    "trigger": "",
    "linked_to": "needs",
    "linked_to_id": "258"
  }, {
    "on_click": [],
    "text": "Совсем другое",
    "trigger": "",
    "linked_to": "GameQuestion",
    "linked_to_id": "94"
  }, {
    "on_click": [],
    "text": "Назад",
    "trigger": "",
    "linked_to": "BasePlan",
    "linked_to_id": "107"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "132",
  "textid": "task4",
  "name": "Задание 4",
  "message": "План готов, можно переходить к исполнению. Ура, человек!",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "138",
  "textid": "1june19",
  "name": "Запуск с 1-го июня 19",
  "message": "Отлично, ты играешь!",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Получить первое задание",
    "trigger": "",
    "linked_to": "task1",
    "linked_to_id": null
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "191",
  "textid": "doableNO",
  "name": "Невыполнимо",
  "message": "Попробуй пару дней оценивать каждую задачу, которую делаешь, по времени. Дальше, если ты планируешь на день задач больше, чем на 6 часов, то попробуй ставить более реалистичные цели. ",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "отчасти помогло, давайте дальше",
    "trigger": "",
    "linked_to": "conflict",
    "linked_to_id": "193"
  }, {
    "on_click": [],
    "text": "Чет непонятно, поясните",
    "trigger": "",
    "linked_to": "GameQuestion",
    "linked_to_id": "94"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "193",
  "textid": "conflict",
  "name": "Внутренний конфликт",
  "message": "Каждый раз, когда ты хочешь заняться запланированными делами, у тебя находится что-то, что ты хочешь делать больше. И вообще, может ты вообще не хочешь это делать? Если проблема в этом, нажми \"есть внутренний конфликт\"\r\n",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Есть внутренний конфликт!",
    "trigger": "",
    "linked_to": "conflictYES",
    "linked_to_id": "195"
  }, {
    "on_click": [],
    "text": "Нет, я точно хочу делать запланированное",
    "trigger": "",
    "linked_to": "trigger",
    "linked_to_id": "197"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "197",
  "textid": "trigger",
  "name": "Триггер",
  "message": "Все вроде нормально, но непонятно в какой момент начинать делать задуманное. ",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Ага, похоже на то",
    "trigger": "",
    "linked_to": "triggerYES",
    "linked_to_id": "199"
  }, {
    "on_click": [],
    "text": "Нет, есть конкретный триггер!",
    "trigger": "",
    "linked_to": "triggerNO",
    "linked_to_id": "201"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "201",
  "textid": "triggerNO",
  "name": "Есть триггер",
  "message": "Хорошо! Еще вариант:\r\n5. Ты очень ругаешь себя когда что-то не получается",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Ругаю, ага",
    "trigger": "",
    "linked_to": "failures",
    "linked_to_id": "203"
  }, {
    "on_click": [],
    "text": "Нет, не это",
    "trigger": "",
    "linked_to": "depression",
    "linked_to_id": "205"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "203",
  "textid": "failures",
  "name": "Неудачи",
  "message": "Расскажи о последних неудачах, своих ощущениях по этому поводу и своих действиях. ",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Сдать задание",
    "trigger": "",
    "linked_to": "homework",
    "linked_to_id": "102"
  }, {
    "on_click": [],
    "text": "Давайте дальше",
    "trigger": "",
    "linked_to": "depression",
    "linked_to_id": "205"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "214",
  "textid": "22june19",
  "name": "22 июня19 старт",
  "message": "Отлично, ты играешь!",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Получить первое задание",
    "trigger": "",
    "linked_to": "problem",
    "linked_to_id": "105"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "283",
  "textid": "20july19",
  "name": "Старт 20 июля 19",
  "message": "Отлично, ты играешь!",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Получить первое задание",
    "trigger": "",
    "linked_to": "problem",
    "linked_to_id": "105"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "378",
  "textid": "executing",
  "name": "Выполнение!",
  "message": "Поздравляю, ты приступаешь или вот-вот приступишь к выполнению плана. Есть несколько полезностей, которые стоит учесть при выполнении любого дела, чтобы не упороться и дойти до финиша живым и довольным человеком)\r\n\r\n1. Внутри любого плана есть главные задачи и второстепенные. Мы частенько кидаемся решать не очень главные, но понятные задачи, откладывая сложные, но важные. Чекай себя время от времени на этот предмет. \r\n2. Определи свои сильные стороны и опирайся на них. То, в чем ты пока недостаточно компетентен, попробуй делегировать или хотя бы привлекай помощь - это нормально и продвинет тебя гораздо больше, чем стремление \"через не могу\" зарешать все самостоятельно.\r\n3. Планируй выполнение сложных частей плана на пиковые периоды продуктивности. Отметь, в какие периоды времени ты наиболее эффективно работаешь.\r\n4. По возможности организуй комфортную среду для работы - чтобы никто и ничто не отвлекало тебя\r\n5. Так же тщательно, как и работу, спланируй отдых и время на восстановление. Способы выбери те, которые подходят и приятны лично тебе - прогулки, чил на газоне в парке, физическая активность, общение с приятными людьми, медитация, массаж, прослушивание музыки и пр.\r\n6. Развивай свою способность реалистично оценивать возможности. Раз в неделю оглядывайся назад и подводи итог - сколько было запланировано и сколько сделано. Через пару месяцев у тебя накопится достаточно материала, чтобы понять реальные объемы работы, которые ты успеваешь выполнить в единицу времени. Скорректируй свои планы с учетом этого, не старайся выкроить двенадцать шапочек из одной шкурки :) Не бери на себя больше, чем можешь, без риска эмоционального и физического истощения.\r\n\r\n(текст подготовлен с использованием материалов из блога @geek.psychology)",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "503",
  "textid": "retrolinks",
  "name": "Ссылки после ретро",
  "message": "Закрытый чат выпускников: https:\/\/t.me\/joinchat\/CHlYyUfAlk5rN86uqPBiAg\r\n\r\nШаблон для ретро: https:\/\/telegra.ph\/Retro-Zareshaj-08-042. \r\n\r\nВ конце тебя ждет чат выпускников, шаблон для вдумчивого ретро по содержанию твоей деятельности и способ продолжить играть по сниженной цене prapion.me\/snovazareshat \r\n\r\nЕще мы будем очень рады любой обратной связи, прямо в ответ на это сообщение. Ну и отзыву в соцсетях, если тебе понравилось и ты хочешь, чтобы твои друзья тоже сыграли. ",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "634",
  "textid": "promo3",
  "name": "Промо3",
  "message": "У нас тут есть несколько типов участия: \r\n\r\nСложные проблемы, связанные с твоим внутренним миром (требуют помощи эксперта и стоят дороже: предлагаю отталкиваться в цене от стоимости консультаций психиолога или коуча)\r\n\r\nПростые проблемы, или сложные проблемы с внешним миром (не требуют эксперта, стоят дешевле!)\r\n\r\nБлаготворительный билет по хорошей цене: покупаешь себе на два месяца и вдобавок один стипендиат получает месяц. Также это может быть билет для твоего друга\/подруги на месяц. ",
  "turned_on": false,
  "buttons": [{
    "on_click": [],
    "text": "Подробнее про билет с экспертом",
    "trigger": "",
    "linked_to": "zareshatExpert",
    "linked_to_id": "635"
  }, {
    "on_click": [],
    "text": "Подробнее про билет с ботом",
    "trigger": "",
    "linked_to": "zareshatBot",
    "linked_to_id": "636"
  }, {
    "on_click": [],
    "text": "Подробнее про благотворительный",
    "trigger": "",
    "linked_to": "zareshatVIP",
    "linked_to_id": "637"
  }, {
    "on_click": [],
    "text": "3",
    "trigger": "",
    "linked_to": "buttons_per_row",
    "linked_to_id": null
  }, {
    "on_click": [],
    "text": "Посмотреть отзывы",
    "trigger": "",
    "linked_to": "promofeedback",
    "linked_to_id": "648"
  }, {
    "on_click": [],
    "text": "Посмотреть, кто консультирует",
    "trigger": "",
    "linked_to": "promoteam",
    "linked_to_id": "649"
  }, {
    "on_click": [],
    "text": "Кнопка для недоверчивых",
    "trigger": "",
    "linked_to": "promoquestion",
    "linked_to_id": "650"
  }],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "660",
  "textid": "promo2problems",
  "name": "А какие проблемы можно решать?",
  "message": "Как правило, такие, с которыми сталкивается большинство людей - определиться, что делать в жизни, куда двигаться дальше, начать или завершить важное дело, лучше понять свои потребности и ориентиры, научиться выстраивать отношения с людьми, стать более сфокусированным и организованным",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "667",
  "textid": "promo2start",
  "name": "ghjg",
  "message": "gjg",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "935",
  "textid": "marathontask5321",
  "name": "Задание Марафон 5321",
  "message": "Конечно, один день для анализа актуальных переживаний — это мало. Поэтому постарайся вспомнить, когда последний раз ты переживал_а эмоции, связанные с твоими ключевыми ценностями? В каких ситуациях это происходило? Напиши про них мне",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "952",
  "textid": "marathontas731",
  "name": "Задание марафон 731",
  "message": "Эти сферы стоит сделать приоритетом. Можешь выбрать для формулировки проблемы а) сферу с самым низким баллом по удовлетворенности, либо б) с самым высоким рангом в списке ценностей.\r\nПодумай, какая именно проблема из этой сферы беспокоит тебя на сегодняшний день больше всего. Напиши ее мне.",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1168",
  "textid": "afermar2",
  "name": "",
  "message": "",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}, {
  "id": "1500",
  "textid": "finalquest26",
  "name": "",
  "message": "",
  "turned_on": false,
  "buttons": [],
  "attachments": [],
  "send_random_attachment": false,
  "attributes": {
    "actions": {
      "on_visit": [],
      "on_leave": []
    }
  },
  "payment": {
    "attached": false
  },
  "keyboard_format": {
    "default": true
  },
  "buttons_only": false,
  "available_by_deeplink": true
}];
},{}],"src/data.js":[function(require,module,exports) {
"use strict";

var _scheme = _interopRequireDefault(require("./scheme.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

for (var i = 0; i < _scheme.default.length; i++) {
  $('.messages').append('<div id="' + _scheme.default[i].id + '" class="' + _scheme.default[i].textid + '" title="' + _scheme.default[i].name + '">' + _scheme.default[i].message + '</div>');
}
},{"./scheme.json":"src/scheme.json"}],"node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "36757" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["node_modules/parcel-bundler/src/builtins/hmr-runtime.js","src/data.js"], null)
//# sourceMappingURL=/data.6e275e21.js.map